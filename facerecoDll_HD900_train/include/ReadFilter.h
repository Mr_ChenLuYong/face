#include <windows.h>
#include <winsvc.h>
#include <conio.h>
#include <stdio.h>

#define DLLOPTION _declspec(dllexport)

extern "C" {
	DLLOPTION BOOL rfdCheckHandle();

	DLLOPTION int rfdInit();
	DLLOPTION BOOL rfdCheck();
	DLLOPTION BOOL rfdRead(UCHAR* data, ULONG length, int readSleep);
	DLLOPTION BOOL rfdReadInfo(CHAR* name, CHAR* gender, CHAR* nation, CHAR* bornday, CHAR* address, CHAR* idcode, CHAR* orgn, CHAR* startDay, CHAR* endDay, CHAR* path, ULONG length, int readSleep);
	DLLOPTION BOOL rfdReadInfoD(CHAR* name, CHAR* gender, CHAR* nation, CHAR* bornday, CHAR* address, CHAR* idcode, CHAR* orgn, CHAR* startDay, CHAR* endDay, CHAR* path, ULONG length, int readSleep, bool debug);
	DLLOPTION BOOL rfdSuccess();
	DLLOPTION BOOL rfdSuccessD(CHAR* idinfo);
	DLLOPTION BOOL rfdFailed();
	DLLOPTION BOOL rfdRelease();

	DLLOPTION BOOL rfdGetBmp(CHAR* path);
}