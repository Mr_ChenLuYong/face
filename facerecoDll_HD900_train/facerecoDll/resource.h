//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 facerecoDll.rc 使用
//
#define IDD_FACERECODLG                 13000
#define IDC_TIP                         13000
#define IDD_RESULTDLG                   13001
#define IDC_COMBO1                      13001
#define IDC_CBOCAPTURE                  13001
#define IDB_BACKGROUND                  13002
#define IDB_IDCARD                      13003
#define IDB_GETIMAGE                    13004
#define IDB_MINIMIZE                    13005
#define IDB_SUBMIT                      13006
#define IDB_MANCHECK                    13007
#define IDB_MANUNCHECK                  13008
#define IDB_GIRLCHECK                   13009
#define IDB_GIRLUNCHECK                 13010
#define IDB_CONFIRM                     13011
#define IDB_GRAYCONFIRM                 13012
#define IDB_REFUSE                      13013
#define IDB_GRAYREFUSE                  13014
#define IDB_PASS                        13015
#define IDB_FAIL                        13016
#define IDD_HOSEDLG                     13017
#define IDB_RESULTBACK                  13018
#define IDB_BACKGROUNDSMALL             13019
#define IDB_VIDEO                       13021
#define IDB_BTNPASS                     13022
#define IDD_SPIRITDLG                   13023
#define IDB_BTNREFUSE                   13024
#define IDD_BRANDNEWDLG                 13025
#define IDB_ERRORS                      13026
#define IDB_CLOSE                       13027
#define IDD_STATEDLG                    13028
#define IDI_ICON1                       13029
#define IDD_IDLESSDLG                   13030
#define IDR_MENUNOTIFY                  13032
#define IDB_TITLEPASS                   13033
#define IDB_TITLEREFUSE                 13034
#define IDB_TITLECHECK                  13035
#define IDB_BTNBLUEKNOW                 13036
#define IDB_BTNREDKNOW                  13037
#define IDB_RESULTLOGO                  13038
#define IDB_HANDBACKGROUND              13039
#define IDB_SPIRIT                      13041
#define IDB_SPIRITSLEEP                 13042
#define IDB_SPIRITDETECT                13043
#define IDR_MENUFORPUBWIN               13044
#define IDB_BACKGROUNDSINGLE            13045
#define IDB_BRANDNEWBG                  13046
#define IDB_BANDNEWPASS                 13047
#define IDB_BRANDNEWREFUSE              13048
#define IDB_BRANDNEWIDFRONT             13049
#define IDB_BRANDNEWIDBACK              13050
#define IDB_BRANDNEWVALID               13051
#define IDB_BRANDNEWINVALID             13052
#define IDB_BRANDNEWICONPASS            13053
#define IDB_BRANDNEWICONREFUSE          13054
#define IDB_BRANDNEWLEAVE               13055
#define IDB_BRANDNEWSCAN                13056
#define IDB_BRANDNEWRUN                 13057
#define IDB_BRANDNEWSUCCESS             13058
#define IDB_BRANDNEWFAILER              13059
#define IDB_BRANDNEWX                   13060
#define IDB_BTNIDLESS                   13062
#define IDB_IDLESSBACKGROUND            13063
#define IDB_IDLESSERROR                 13064
#define IDB_IDLESSCLOSE                 13065
#define IDB_IDLESSCONFIRM               13066
#define IDB_BRANDNEWRUNIDLESS           13067
#define IDB_BRANDNEWSCANIDLESS          13068
#define IDB_BRANDNEWSUCCESSIDLESS       13069
#define IDB_BRANDNEWLEAVEIDLESS         13070
#define IDB_BITMAP1                     13071
#define IDB_BRANDNEWFAILERIDLESS        13071
#define IDM_UPDATE                      32771
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_UPDATE                       32775
#define ID_EXIT                         32776
#define ID_32777                        32777
#define ID_FULLSCREEN                   32778
#define ID_32779                        32779
#define RESET                           32780
#define ID_RESET                        32781
#define ID_                             32782
#define ID_Settle                       32783
#define ID_ABOUT                        32784
#define ID_GG_32785                     32785
#define ID_32786                        32786
#define ID_32787                        32787
#define ID_OSSETTING                    32788
#define ID_32789                        32789
#define ID_32790                        32790
#define ID_32791                        32791
#define ID_REMIND_UP                    32792
#define ID_REMIND_DOWN                  32793
#define ID_REMIND_ON_OFF                32794
#define ID_Menu                         32795
#define ID_SETTING                      32796

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        13072
#define _APS_NEXT_COMMAND_VALUE         32797
#define _APS_NEXT_CONTROL_VALUE         13002
#define _APS_NEXT_SYMED_VALUE           13031
#endif
#endif
