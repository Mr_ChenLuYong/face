#include "StdAfx.h"
#include "Public.h"


CfacerecoDlg* pFacerecoDlg;
ResultDlg resultDlg;
SpiritDlg* spiritDlg;

// 项目路径
CString appPath;
CString storagePath;
MCapture* pCapture;
SCapParam pCapParam;

long preLastTime;
CString preDetectingPath;
CString readFilterPath;

// API DLL
HINSTANCE apiDll;
APIMD5 apiMd5;
APIThreshold apiThreshold;
APIUploadInfo apiUploadInfo;
APIHotelHand apiHotelHand;
APINetBarHand apiNetBarHand;
APICheckVersion apiCheckVersion;
APIGasDoSimilarity apiGasDoSimilarity;
APIGetSimilarityVal apiGetSimilarityVal;
APIDoResultSure apiDoResultSure;


void ContructBih(int nWidth,int nHeight,BITMAPINFOHEADER& bih)
{
	bih.biSize=40; 						// header size
	bih.biWidth=nWidth;
	bih.biHeight=nHeight;
	bih.biPlanes=1;
	bih.biBitCount=24;					// RGB encoded, 24 bit
	bih.biCompression=BI_RGB;			// no compression
	bih.biSizeImage=nWidth*nHeight*3;
	bih.biXPelsPerMeter=0;
	bih.biYPelsPerMeter=0;
	bih.biClrUsed=0;
	bih.biClrImportant=0;
}

void DrawBmpBuf(BITMAPINFOHEADER& bih,BYTE* pDataBuf,HWND hShowWnd,BOOL bFitWnd, bool drawLine)
{
	RECT rc;
	::GetWindowRect( hShowWnd, &rc );
	int nWidth = rc.right - rc.left-2;
	int nHeight = rc.bottom - rc.top-2;

	BITMAPINFO bi;
	memset(&bi,0,sizeof(bi));
	memcpy( &(bi.bmiHeader), &bih, sizeof(BITMAPINFOHEADER) );
	int iWidth=bih.biWidth;
	int iHeight=bih.biHeight;

	// display bitmap
	HDC hdcStill = ::GetDC( hShowWnd );
	PAINTSTRUCT ps;
	::BeginPaint( hShowWnd, &ps );


	::SetStretchBltMode( hdcStill, COLORONCOLOR );

	if(bFitWnd)
		::StretchDIBits( hdcStill, 0, 0, nWidth, nHeight,
			0, 0, iWidth, iHeight, pDataBuf, &bi,
			DIB_RGB_COLORS,	SRCCOPY );
	else
		::StretchDIBits( hdcStill, 0, 0, iWidth, iHeight,
			0, 0, iWidth, iHeight, pDataBuf, &bi,
			DIB_RGB_COLORS,	SRCCOPY );
	if(1)
	{
		::SetBkMode( hdcStill, TRANSPARENT );

		HBRUSH hBrush=(HBRUSH)GetStockObject(NULL_BRUSH);
		HBRUSH hOldBrush=(HBRUSH)::SelectObject(hdcStill,hBrush);
		HPEN hPen=CreatePen(PS_DOT,1,RGB(0,255,255));
		HPEN hOldPen=(HPEN)::SelectObject(hdcStill,hPen);

		int w=120;
		int h=160;

		if (drawLine) 
		{
			::MoveToEx(hdcStill,0,nHeight/2,NULL);
			::LineTo(hdcStill,nWidth-1,nHeight/2);

			::MoveToEx(hdcStill,nWidth/2,0,NULL);
			::LineTo(hdcStill,nWidth/2,nHeight-1);

			::Rectangle(hdcStill,nWidth/2-w/2,nHeight/2-h/2,nWidth/2+w/2,nHeight/2+h/2);
		}


		::SelectObject(hdcStill,hOldPen);
		::SelectObject(hdcStill,hOldBrush);
		::DeleteObject(hPen);
		::DeleteObject(hBrush);
	}

	::EndPaint( hShowWnd, &ps );
	::ReleaseDC( hShowWnd, hdcStill );
}

void GetPartImage( BYTE* image, int imageWidth, int imageHeight, BYTE* part,  int left,int top, int right ,int bottom )
{
	int w = right - left;
	int h = bottom - top;

	UINT nSize=imageWidth*3;
	for( int i=0; i<h; i++ )
	{
		for( int j=0; j<w; j++ )
		{
			part[j*3+i*w*3+0] = image[(left+j)*3+(top+i)*nSize+0];
			part[j*3+i*w*3+1] = image[(left+j)*3+(top+i)*nSize+1];
			part[j*3+i*w*3+2] = image[(left+j)*3+(top+i)*nSize+2];
		}
	}
}

BOOL ResampleBmp(BITMAPINFOHEADER& bih, BYTE* inBuf,int newx,int newy,BYTE* outBuf)
{
	if (newx==0 || newy==0) return FALSE;

	if (bih.biWidth==newx && bih.biHeight==newy){
		if(outBuf)
		{
			memcpy(outBuf,inBuf,bih.biSizeImage);
		}
		return TRUE;
	}

	float xScale, yScale, fX, fY;
	xScale = (float)bih.biWidth  / (float)newx;
	yScale = (float)bih.biHeight / (float)newy;

	//DWORD对齐的扫描行的字节宽度
	int dwEffWidth1 = ((((bih.biBitCount * bih.biWidth) + 31) / 32) * 4);

	bih.biWidth=newx;
	bih.biHeight=newy;
	

	int dwEffWidth2 = ((((bih.biBitCount * bih.biWidth) + 31) / 32) * 4);

	for(long y=0; y<newy; y++)
	{
		fY = y * yScale;
		for(long x=0; x<newx; x++)
		{
			fX = x * xScale;
			//得到源位图的每个象素点的RGB 
			BYTE* iSrc  = inBuf + (int)fY*dwEffWidth1 + (int)fX*3; 
			RGBQUAD rgb;
			rgb.rgbBlue = *iSrc++;
			rgb.rgbGreen= *iSrc++;
			rgb.rgbRed  = *iSrc;
			//设置目标位图的每个象素点的RGB
			BYTE* iDst = outBuf + y*dwEffWidth2 + x*3;
			*iDst++ = rgb.rgbBlue;
			*iDst++ = rgb.rgbGreen;
			*iDst   = rgb.rgbRed;
		}
	}
	bih.biSizeImage=dwEffWidth2*bih.biHeight;
	return TRUE;
}

void DrawFaceRects(BYTE* pRgbBuf,int nBufWidth,int nBufHeight,RECT* pFaceRects,int nRectNum,COLORREF color,int nPenWidth)
{
	if(pRgbBuf==NULL||pFaceRects==NULL||nRectNum==0||nPenWidth==0) return;
	RECT rc;
	int rcWidth,rcHeight;
	int widthBytes=nBufWidth*3;
	BYTE* image=pRgbBuf;

	int i,j,k;
	for(i=0;i<nRectNum;i++)
	{
		rc=pFaceRects[i];
		rcWidth=rc.right-rc.left;
		rcHeight=rc.bottom-rc.top;

		for(j=rc.left;j<rc.right;j++)
		{
			for(int a=0;a<nPenWidth;a++)
			{
				image[j*3+(rc.top+a)*widthBytes+0]=GetBValue(color);
				image[j*3+(rc.top+a)*widthBytes+1]=GetGValue(color);
				image[j*3+(rc.top+a)*widthBytes+2]=GetRValue(color);
			}
		}
		for(j=rc.left;j<rc.right;j++)
		{
			for(int a=0;a<nPenWidth;a++)
			{
				image[j*3+(rc.bottom-a)*widthBytes+0]=GetBValue(color);
				image[j*3+(rc.bottom-a)*widthBytes+1]=GetGValue(color);
				image[j*3+(rc.bottom-a)*widthBytes+2]=GetRValue(color);
			}
		}
		for(k=rc.top;k<rc.bottom;k++)
		{
			for(int a=0;a<nPenWidth;a++)
			{
				image[(rc.left+a)*3+k*widthBytes+0]=GetBValue(color);
				image[(rc.left+a)*3+k*widthBytes+1]=GetGValue(color);
				image[(rc.left+a)*3+k*widthBytes+2]=GetRValue(color);
			}
		}
		for(k=rc.top;k<rc.bottom;k++)
		{
			for(int a=0;a<nPenWidth;a++)
			{
				image[(rc.right-a)*3+k*widthBytes+0]=GetBValue(color);
				image[(rc.right-a)*3+k*widthBytes+1]=GetGValue(color);
				image[(rc.right-a)*3+k*widthBytes+2]=GetRValue(color);
			}
		}
	}
}

BOOL MirrorDIB(LPSTR lpDIBBits, LONG lWidth, LONG lHeight, BOOL bDirection,int nImageBits)
{	 
	 // 指向源图像的指针
	 LPSTR	lpSrc; 
	 // 指向要复制区域的指针
	 LPSTR	lpDst;	 
	 // 指向复制图像的指针
	 LPSTR	lpBits;
	 HLOCAL	hBits;	 
	 // 循环变量
	 LONG	i;
	 LONG	j;
	 int nBits;//每像素占的位数
	 // 图像每行的字节数
	 LONG lLineBytes;
	 // 计算图像每行的字节数
	 lLineBytes = WIDTHBYTES(lWidth *nImageBits);
	 // 暂时分配内存，以保存一行图像
	 hBits = LocalAlloc(LHND, lLineBytes);
	 if (hBits == NULL)
	 {
		 // 分配内存失败
		 return FALSE;
	 }	 
	 // 锁定内存
	 lpBits = (char * )LocalLock(hBits);
	 int nStep=nImageBits/8;
	 long lCenter=lWidth/2*nStep;
	 // 判断镜像方式
	 if (bDirection)
	 {
		 // 水平镜像
		 // 针对图像每行进行操作
		 for(i = 0; i < lHeight; i++)
		 {
			 // 针对每行图像左半部分进行操作
			 for(j = 0; j < lCenter; j+=nStep)
			 {
				 for(nBits=0;nBits<nStep;nBits++)
				 {
					 lpSrc = (char *)lpDIBBits + lLineBytes * i +lCenter- j+nBits;
					 lpDst = (char *)lpDIBBits + lLineBytes * i +lCenter+ j+nBits;
					 *lpBits = *lpDst;
					 *lpDst = *lpSrc;
					 *lpSrc = *lpBits;
				 }
				 
			 }
			 
		 }
	 }
	 else
	 {
		 // 垂直镜像
		 // 针对上半图像进行操作
		 for(i = 0; i < lHeight / 2; i++)
		 {		 
			 // 指向倒数第i行象素起点的指针
			 lpSrc = (char *)lpDIBBits + lLineBytes * i;	 
			 // 指向第i行象素起点的指针
			 lpDst = (char *)lpDIBBits + lLineBytes * (lHeight - i - 1);		 
			 // 备份一行，宽度为lWidth
			 memcpy(lpBits, lpDst, lLineBytes);
			 // 将倒数第i行象素复制到第i行
			 memcpy(lpDst, lpSrc, lLineBytes);
			 // 将第i行象素复制到倒数第i行
			 memcpy(lpSrc, lpBits, lLineBytes);
			 
		 }
	 }	 
	 // 释放内存
	 LocalUnlock(hBits);
	 LocalFree(hBits);
	 // 返回
	 return TRUE;
}

BOOL GetCBitmap(BITMAPINFOHEADER* pBih, BYTE* pDataBuf,CBitmap &bmp)
{
	BITMAPINFO bi;
	memset( &bi, 0, sizeof(bi) );
	memcpy( &(bi.bmiHeader), pBih, sizeof(BITMAPINFOHEADER) );

	CClientDC dc(NULL);
	HBITMAP hBmp =::CreateDIBitmap(dc.m_hDC,		
				pBih,	
				CBM_INIT,	
				pDataBuf,	
				&bi,	
				DIB_RGB_COLORS);	
	bmp.Attach( hBmp );
	return TRUE;
}

void SplitCString(CString str, CStringArray& Arr, char ch)
{
    int nFindposi  = str.Find(ch);
    if( nFindposi <0 ) return;
    while( nFindposi > 0)
    {
        Arr.Add(str.Left(nFindposi) );
        str = str.Right( str.GetLength() - nFindposi -1);
        str.TrimLeft(ch);

        nFindposi  = str.Find(ch);
    }

    if( !str.IsEmpty() )
        Arr.Add(str);
}

BOOL SaveBufToBmpFile( BYTE* buf, LPBITMAPINFOHEADER lpBih, LPCSTR strFileName )
{

	DWORD BeWrite=0;
	HANDLE hFile;
	hFile=CreateFile(strFileName,GENERIC_WRITE,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_ARCHIVE,NULL);
	if(hFile==INVALID_HANDLE_VALUE)	
	{

		return FALSE;
	}	
	
	BITMAPFILEHEADER bfh;
	memset( &bfh, 0, sizeof( bfh ) );
	bfh.bfSize = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+lpBih->biSizeImage;
	bfh.bfType = 0x4d42;
	bfh.bfOffBits = sizeof( BITMAPINFOHEADER ) + sizeof( BITMAPFILEHEADER );

	WriteFile(hFile,&bfh,sizeof(bfh),&BeWrite,NULL);
	WriteFile(hFile,lpBih,sizeof(BITMAPINFOHEADER),&BeWrite,NULL);
	WriteFile(hFile,buf,lpBih->biSizeImage,&BeWrite,NULL);

	CloseHandle(hFile);

	return TRUE;
}

void SaveCBitmapToBmpFile(CBitmap* bmp, LPCSTR strFileName) {
	if (!bmp->m_hObject) return;

	CImage imgTemp;
	imgTemp.Attach(bmp->operator HBITMAP());
	imgTemp.Save(strFileName);
	imgTemp.Detach();
	imgTemp.Destroy();

}

HBITMAP CopyBitmap(HBITMAP hSourceHbitmap) {
	CDC sourceDC;
	CDC destDC;
	sourceDC.CreateCompatibleDC(NULL);
	destDC.CreateCompatibleDC(NULL);
	//The bitmap information.
	BITMAP bm = {0};
	//Get the bitmap information.
	::GetObject(hSourceHbitmap, sizeof(bm), &bm);
	// Create a bitmap to hold the result
	HBITMAP hbmResult = ::CreateCompatibleBitmap(CClientDC(NULL), bm.bmWidth, bm.bmHeight);

	HBITMAP hbmOldSource = (HBITMAP)::SelectObject( sourceDC.m_hDC, hSourceHbitmap);
	HBITMAP hbmOldDest = (HBITMAP)::SelectObject( destDC.m_hDC, hbmResult );
	destDC.BitBlt(0,0,bm.bmWidth, bm.bmHeight, &sourceDC, 0, 0, SRCCOPY );

	// Restore DCs
	::SelectObject( sourceDC.m_hDC, hbmOldSource );
	::SelectObject( destDC.m_hDC, hbmOldDest );
	::DeleteObject(sourceDC.m_hDC);
	::DeleteObject(destDC.m_hDC);

	return hbmResult;
}

CString BYTEToHex(const BYTE* ary)
{
	CString hex;
	int len = 2560;

	string*  hexstr=new string();
    for (int i=0;i<len;i++)
    {
        char hex1;
        char hex2;
        int value=ary[i];
        int v1=value/16;
        int v2=value % 16;

        if (v1>=0&&v1<=9)
            hex1=(char)(48+v1);
        else
            hex1=(char)(55+v1);

        if (v2>=0&&v2<=9)
            hex2=(char)(48+v2);
        else
            hex2=(char)(55+v2);

         *hexstr=*hexstr+hex1+hex2;
    }

	hex = hexstr->c_str();
	delete hexstr;

	return hex;
}

CString Md5file(const char* filename)
{
	std::ifstream t(filename, ios::binary);  
	int length;
	char* buffer;
	t.seekg(0, std::ios::end);
	length = t.tellg();
	t.seekg(0, std::ios::beg);
	buffer = new char[length];
	t.read(buffer, length);
	t.close();

	char output[33];
	apiMd5((unsigned char *)buffer, length, output);

	delete []buffer;

	return CString(output);
}

bool MultiToUTF8(const std::string& multiText, std::string& utf8Text)
{
	// 把输入转换为Unicode
	int size = ::MultiByteToWideChar(CP_ACP,
                         0,
                         multiText.c_str(),
                        -1,
                        NULL,
                        0);
        if (0 == size)
        {
            return false;
        }

        wchar_t* wszBuffer = new wchar_t[size + 1];
        ::ZeroMemory(wszBuffer, (size + 1) * sizeof(wchar_t));

        if (0 == ::MultiByteToWideChar(CP_ACP,
                        0,
                        multiText.c_str(),
                        -1,
                        wszBuffer,
                        size + 1))
        {
            delete[] wszBuffer;
            return false;
        }
        
        // Unicode->UTF8的转换
        size = ::WideCharToMultiByte(CP_UTF8,
                         0,
                        wszBuffer,
                        -1,
                        NULL,
                        0,
                        NULL,
                        NULL);
        if (0 == size)
        {
            delete[] wszBuffer;
            return false;
        }

        char* szBuffer = new char[size + 1];
        ::ZeroMemory(szBuffer, (size + 1) * sizeof(char));

        if (0 == ::WideCharToMultiByte(CP_UTF8,
                         0,
                        wszBuffer,
                        -1,
                        szBuffer,
                        size + 1,
                        NULL,
                        NULL))
        {
            delete[] wszBuffer;
            delete[] szBuffer;
            return false;
        }

        utf8Text = szBuffer;
        delete[] wszBuffer;
        delete[] szBuffer;
        return true;
}

void LoadBmpFromFile(LPCTSTR strFileName, CBitmap& bmp)
{
	Mat im;
	im = imread(strFileName);
	int nWidth=im.cols;
	int nHeight = im.rows;
	long lLength = nWidth * nHeight * 3;
	BYTE* pCamBuf = new BYTE[lLength];
	memcpy(pCamBuf,im.data,nWidth*nHeight*3);

	BITMAPINFOHEADER bih;
	ContructBih(nWidth,nHeight,bih);
	LONG lLineBytes;
	lLineBytes = WIDTHBYTES(nWidth *24);
	BYTE* pTemp=new BYTE[lLineBytes*nHeight];
	for(int i=0;i<nHeight;i++)
	{
		for(int j=0;j<nWidth;j++)
		{
			pTemp[i*lLineBytes+j*3+0]=pCamBuf[(nHeight-1-i)*nWidth*3+j*3+0];
			pTemp[i*lLineBytes+j*3+1]=pCamBuf[(nHeight-1-i)*nWidth*3+j*3+1];
			pTemp[i*lLineBytes+j*3+2]=pCamBuf[(nHeight-1-i)*nWidth*3+j*3+2];
		}
	}

	bmp.DeleteObject();
	GetCBitmap(&bih, pTemp, bmp);
	delete []pTemp;
	delete []pCamBuf;
}

int DownloadFile(const CString strUrl,const CString strSavePath) {
    //检查传入的两个参数
    if (strUrl.IsEmpty())
        return -5;
    if (strSavePath.IsEmpty())
        return -6;
 
    unsigned short nPort;        //用于保存目标HTTP服务端口
    CString strServer, strObject;    //strServer用于保存服务器地址，strObject用于保存文件对象名称
    DWORD dwServiceType,dwRet;        //dwServiceType用于保存服务类型，dwRet用于保存提交GET请求返回的状态号
 
    //解析URL，获取信息
    if(!AfxParseURL(strUrl, dwServiceType, strServer, strObject, nPort))
    {
        //解析失败，该Url不正确
        return -1;
    }
    //创建网络连接对象，HTTP连接对象指针和用于该连接的HttpFile文件对象指针，注意delete
    CInternetSession intsess;
    CHttpFile *pHtFile = NULL;
    //建立网络连接
	CHttpConnection *pHtCon = intsess.GetHttpConnection(strServer,nPort);
    if(pHtCon == NULL)
    {
        //建立网络连接失败
        intsess.Close();
        return -2;
    }
    //发起GET请求
    pHtFile = pHtCon->OpenRequest(CHttpConnection::HTTP_VERB_GET,strObject);
    if(pHtFile == NULL)
    {
        //发起GET请求失败
        intsess.Close();
        delete pHtCon;pHtCon = NULL;
        return -3;
    }
    //提交请求
    pHtFile->SendRequest();
    //获取服务器返回的状态号
    pHtFile->QueryInfoStatusCode(dwRet);
    if (dwRet != HTTP_STATUS_OK)
    {
        //服务器不接受请求
        intsess.Close();
        delete pHtCon;pHtCon = NULL;
        delete pHtFile;pHtFile = NULL;
        return -4;
    }
    //获取文件大小
    UINT nFileLen = (UINT)pHtFile->GetLength();
    DWORD dwRead = 1;        //用于标识读了多少，为1是为了进入循环
    //创建缓冲区
    CHAR *szBuffer = new CHAR[nFileLen+1];
    TRY 
    {
        //创建文件
        CFile PicFile(strSavePath,CFile::modeCreate|CFile::modeWrite|CFile::shareExclusive);
        while(dwRead>0)
        {  
            //清空缓冲区
            memset(szBuffer,0,(size_t)(nFileLen+1));
            //读取到缓冲区
            dwRead = pHtFile->Read(szBuffer,nFileLen); 
            //写入到文件
            PicFile.Write(szBuffer,dwRead);
        }
        //关闭文件
        PicFile.Close();
        //释放内存
        delete []szBuffer;
        delete pHtFile;
        delete pHtCon;
        //关闭网络连接
        intsess.Close();
    }
    CATCH(CFileException,e)
    {
        //释放内存
        delete []szBuffer;
        delete pHtFile;
        delete pHtCon;
        //关闭网络连接
        intsess.Close();
        return -7;            //读写文件异常
    }
    END_CATCH
        return 0;
}


void TrimAry(char* ary, int len) {
	int i = 0;
	for (i = 0; i < len; i++) {
		if (ary[i] == ' ') {
			ary[i] = '\0';
			return;
		}
	}
	ary[i] = '\0';
}

BOOL BuildSenderSocket(SOCKET& sdSocket, int sdPort, SOCKADDR_IN& sockaddr_in) {
	WSADATA wsaDataSender;
	BOOL fBroadcast = TRUE;
	if(WSAStartup(MAKEWORD( 1, 1 ), &wsaDataSender )!=0)
		return FALSE;
	sdSocket=socket(PF_INET,SOCK_DGRAM,0);
	setsockopt(sdSocket,SOL_SOCKET,SO_REUSEADDR, (CHAR *)&fBroadcast, sizeof (BOOL) );
	sockaddr_in.sin_family = AF_INET;
	sockaddr_in.sin_addr.s_addr = inet_addr("127.0.0.1");
	sockaddr_in.sin_port = htons(sdPort);

	return TRUE;
}


BOOL BuildReceiverSocket(SOCKET& rcSocket, int rcPort, int timeout) {
	WSADATA wsaData;
	SOCKADDR_IN sin;
	if(WSAStartup(MAKEWORD( 1, 1 ), &wsaData )!=0)
		return FALSE;
	rcSocket=socket(AF_INET, SOCK_DGRAM,0);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(rcPort);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	
	if (timeout != 0) {
		setsockopt(rcSocket,SOL_SOCKET,SO_RCVTIMEO, (char *)&timeout,sizeof(struct timeval));
	}
	if(bind( rcSocket, (SOCKADDR FAR *)&sin, sizeof(sin))!=0)
		return FALSE;

	return TRUE;
}

BOOL FileExists(char* fileName) {
	BOOL exists = FALSE;
	fstream file;
	file.open(fileName, ios::in);
	if (file) {
		exists = TRUE;
	}
	file.close();
	return exists;
}