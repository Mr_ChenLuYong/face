#include "SRTest.h"

#include <ctime>


SRTest::SRTest()
{
	startTime_ = clock();
}


SRTest::~SRTest()
{
}

long SRTest::getElapsed(void) const
{
	return startTime_ - clock();
}
