
#include "commonlog.h"						 
#pragma comment(lib, "../lib/log4cpp.lib")


#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

#include <io.h>
CommonLog SystemLog;

CommonLog::CommonLog()
{
	
}


CommonLog::~CommonLog() {
	log4cpp::Category::shutdown();
}
 


log4cpp::Category& CommonLog::getRoot(const char* const node) {
	// 手动初始化
	try {
		static int isinit = 0;
		if (isinit == 0) {
			isinit++;
			// 默认读取本地配置文件设置日志样式
			if (_access("./log.conf", 0) != -1) {
				log4cpp::PropertyConfigurator::configure("log.conf");
			}
		}
	}
	catch (log4cpp::ConfigureFailure& f) {
		std::cout << "Configure Problem " << f.what() << std::endl;
	}
	catch (std::exception ex) {
		std::cout << "Configure Problem " << ex.what() << std::endl;
	}
	catch (...) {
		std::cout << __FUNCTION__ << "what error? " << std::endl;
	}
	if (node == nullptr) {
		return log4cpp::Category::getRoot();
	}
	return log4cpp::Category::getRoot().getInstance(node);
}

bool CommonLog::sign(int v) {
	switch (v)
	{
	case CommonLog::LOGSIGN::FATAL:
		outFunction_ = &log4cpp::Category::fatal;
		break;
	case CommonLog::LOGSIGN::ALERT:
		outFunction_ = &log4cpp::Category::alert;
		break;
	case CommonLog::LOGSIGN::CRIT:
		outFunction_ = &log4cpp::Category::crit;
		break;
	case CommonLog::LOGSIGN::ERROR:
		outFunction_ = &log4cpp::Category::error;
		break;
	case CommonLog::LOGSIGN::WARN:
		outFunction_ = &log4cpp::Category::warn;
		break;
	case CommonLog::LOGSIGN::NOTICE:
		outFunction_ = &log4cpp::Category::notice;
		break;
	case CommonLog::LOGSIGN::DEBUG:
		outFunction_ = &log4cpp::Category::debug;
		break;
	case CommonLog::LOGSIGN::NOTSET:
	case CommonLog::LOGSIGN::INFO:
		outFunction_ = &log4cpp::Category::info;
		break;
	default:
		break;
	}
	return true;

}