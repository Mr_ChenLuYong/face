/**
* @file  : commonlog.h
* @brief : 全局日志类
* @author: 陈鲁勇
* @date  : 2017年6月3日
*/

#ifndef __SK_MODULE_COMMONLOG_H__
#define __SK_MODULE_COMMONLOG_H__

#include <iostream>
#include <functional>
#include <sstream>
#include <mutex>
#include <log4cpp/Category.hh>

class CommonLog {

public:
	CommonLog();
	~CommonLog();
	
	enum LOGSIGN {
		EMERG   = 0,
		FATAL   = 0,
		ALERT   = 100,
		CRIT    = 200,
		ERROR   = 300,
		WARN    = 400,
		NOTICE  = 500,
		INFO    = 600,
		DEBUG   = 700,
		NOTSET  = 800
	};
	
public:

	log4cpp::Category& getRoot(const char* const node = nullptr);


	// 线程不安全
	template <typename INPUT>
	CommonLog& operator<<(INPUT v) {
		static std::mutex m;
		std::unique_lock<std::mutex> loker(m);
		if (typeid(LOGSIGN) == typeid(v)) {
			sign((int)v);
		}
		else {
			log4cpp::Category& root = log4cpp::Category::getRoot();
			std::stringstream io;
			io << v;
			root.(*outFunction_)(io.str());
		}
		return *this;
	}
protected:
	bool sign(int v);

private:
	std::string     logInfo_;
	void (log4cpp::Category::*outFunction_)(const std::string&);
};

extern CommonLog SystemLog;

#endif // __SK_MODULE_COMMONLOG_H__