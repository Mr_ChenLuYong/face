#ifndef _SHANREN_TEST_H_
#define _SHANREN_TEST_H_

class SRTest
{
public:
	SRTest();
	~SRTest();

public:
	long getElapsed(void) const;


private:

	long startTime_;

};

#endif // _SHANREN_TEST_H_