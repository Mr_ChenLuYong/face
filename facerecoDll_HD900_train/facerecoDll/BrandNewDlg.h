#pragma once

#include "Resource.h"

// BrandNewDlg 对话框

class BrandNewDlg : public CDialog
{
	DECLARE_DYNAMIC(BrandNewDlg)

public:
	BrandNewDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~BrandNewDlg();

// 对话框数据
	enum { IDD = IDD_BRANDNEWDLG };

	NOTIFYICONDATA notifyIcon;
	afx_msg LRESULT OnNotifyIcon(WPARAM wParam,LPARAM IParam);

protected:

	HICON m_hIcon;
	HHOOK m_hHook;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	// 拖动窗口
	afx_msg LRESULT OnNcHitTest(CPoint point);
	// 消息处理
	BOOL PreTranslateMessage(MSG* pMsg);


// 窗口控件
private:

	// 右键菜单
	CMenu menuTray;
};
