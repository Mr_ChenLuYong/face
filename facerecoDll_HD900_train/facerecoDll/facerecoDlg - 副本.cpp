// facerecoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "resource.h"
#include "hoseDlg.h"
#include "facerecoDlg.h"
#include "ReadFilter.h"
#include "DMonitor.h"


// 锁
CRITICAL_SECTION g_CriticalSection;
class MyLock
{
public:
	MyLock(){EnterCriticalSection(&g_CriticalSection);};
	~MyLock(){LeaveCriticalSection(&g_CriticalSection);};
};

LRESULT CALLBACK KeybdProc(int nCode, WPARAM wParam, LPARAM lParam) {

	if (nCode >= 0) {

		switch (wParam) {
			case WM_KEYDOWN:
				{
					PKBDLLHOOKSTRUCT pVirKey = (PKBDLLHOOKSTRUCT)lParam;
					CString msg;
					if (pVirKey->vkCode == VK_F4) {
						pFacerecoDlg->StartDetect();
						return 1;
					}
					break;
				}
		}
	}

	//将消息向下一个钩子传递
     return CallNextHookEx(NULL, nCode, wParam, lParam);
}


// facerecoDlg 对话框

CfacerecoDlg::CfacerecoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CfacerecoDlg::IDD, pParent)
{
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	// 窗口置顶
	//SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
}

void CfacerecoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

#define WM_NC (WM_USER+1001)

BEGIN_MESSAGE_MAP(CfacerecoDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_NC,&CfacerecoDlg::OnNotifyIcon)
	//}}AFX_MSG_MAP

	ON_WM_NCHITTEST()
	ON_STN_CLICKED(1003, &CfacerecoDlg::OnStnClickedGetImage)
	ON_STN_CLICKED(1013, &CfacerecoDlg::OnStnClickedSubmit)
	ON_STN_CLICKED(1004, &CfacerecoDlg::OnStnClickedMinimize)
	ON_STN_CLICKED(1019, &CfacerecoDlg::OnStnClickedMan)
	ON_STN_CLICKED(1020, &CfacerecoDlg::OnStnClickedGirl)
	ON_STN_CLICKED(1025, &CfacerecoDlg::OnStnClickedConfirm)
	ON_STN_CLICKED(1026, &CfacerecoDlg::OnStnClickedRefuse)
	ON_STN_CLICKED(1027, &CfacerecoDlg::OnStnClickedVersion)
	ON_STN_CLICKED(1029, &CfacerecoDlg::OnStnClickedClose)

END_MESSAGE_MAP()

// facerecoDlg 消息处理程序

BOOL CfacerecoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetActiveWindow();
	// 窗口置顶
	this->SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
	

	SetWindowText("人像识别程序");

	// TODO: 在此添加额外的初始化代码
	HANDLE hMutex=::CreateMutex(NULL,TRUE,"facereco.dll");
	if (hMutex == NULL) {
		CloseHandle(hMutex);
		Exit();
		return TRUE;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		CloseHandle(hMutex);
		Exit();
		return TRUE;
	}

	// 获取项目所在路径
	char pModuleName[256];
	::GetModuleFileName(NULL,pModuleName,256);
	CString strTemp;
	strTemp=pModuleName;
	int pos=strTemp.ReverseFind('\\');
	appPath=strTemp.Left(pos);
	
	// 创建暂存图片目录
	::CreateDirectory(appPath+"\\storage",NULL);

	config = appPath + "\\config.ini";
	readFilterPath = appPath + "\\ReadFilter.sys";
	
	storagePath = appPath + "\\storage";
	fullFile = storagePath+"\\full.jpg";
	avatarFile = storagePath+"\\avatar.jpg";
	idFile = storagePath+"\\photo.jpg";
	handFaceFile = storagePath + "\\handface.jpg";
	storeConfig = storagePath + "\\config.ini";


	// 加载配置
	LoadConfig();
	
	int handWindowWidth = 0;
	int handWindowHeight = 0;
	if (initType == 2 || initType == 4) {
		handWindowWidth = 200;
	} else if (initType == 1 || initType == 3) {
		handWindowWidth = 324;
	} else if (initType == 5) {
		handWindowWidth = 516;
		handWindowHeight = 80;
	}

	// 设置窗口大小
	MoveWindow(100,100,942 - handWindowWidth,577 - handWindowHeight);
	

	notifyIcon.uID = IDI_ICON1;
	notifyIcon.cbSize=sizeof(NOTIFYICONDATA);
    notifyIcon.hIcon=AfxGetApp()->LoadIcon(IDI_ICON1);
    notifyIcon.hWnd=m_hWnd;
    lstrcpy(notifyIcon.szTip,_T("Facereco"));
    notifyIcon.uCallbackMessage=WM_NC;
    notifyIcon.uFlags=NIF_ICON | NIF_MESSAGE | NIF_TIP;
    Shell_NotifyIcon(NIM_ADD,&notifyIcon);

	
	apiDll = LoadLibrary("facerecoAPI.dll");
	if (apiDll == NULL) {
		Message("缺少组件facerecoAPI.dll");
		Exit();
		return FALSE;
	}

	// 获取接口 md5 方法
	apiMd5 = (APIMD5)GetProcAddress(apiDll, "md5");
	// 获取接口 threshold 方法
	apiThreshold = (APIThreshold)GetProcAddress(apiDll, "threshold");
	// 获取接口 hotelHand 方法
	apiHotelHand = (APIHotelHand)GetProcAddress(apiDll, "hotelHand");
	// 获取接口 netbarHand 方法
	apiNetBarHand = (APINetBarHand)GetProcAddress(apiDll, "netBarHand");
	// 获取接口 uploadInfo 方法
	apiUploadInfo = (APIUploadInfo)GetProcAddress(apiDll, "uploadInfo");
	// 获取接口 checkVersion 方法
	apiCheckVersion = (APICheckVersion)GetProcAddress(apiDll, "checkVersion");
	// 获取接口 gasDoSimilarity 方法
	apiGasDoSimilarity = (APIGasDoSimilarity)GetProcAddress(apiDll, "gasDoSimilarity");
	// 获取接口 getSimilarityVal 方法
	apiGetSimilarityVal = (APIGetSimilarityVal)GetProcAddress(apiDll, "getSimilarityVal");
	// 获取接口 doResultSure 方法
	apiDoResultSure = (APIDoResultSure)GetProcAddress(apiDll, "doResultSure");

	spiritDlg = new SpiritDlg();
	spiritDlg->setDialog(this, initType);
	spiritDlg->Create(IDD_SPIRITDLG, this);
	spiritDlg->ShowWindow(true);


	// 创建控件
	// 视频显示框
	picVideo.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(540 - handWindowWidth,137,918 - handWindowWidth,440),this, 1000);
	picVideo.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_VIDEO)));
	picVideo.ShowWindow(true);


	if (initType != 5) {
		// 最小化按钮
		btnMinimize.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(843 - handWindowWidth, 0, 890 - handWindowWidth, 28),this, 1004);
		btnMinimize.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MINIMIZE)));
		btnMinimize.ShowWindow(true);
	}


	if (initType != 1 && initType != 5) {
		// 关闭按钮
		btnClose.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(893 - handWindowWidth, 0, 940 - handWindowWidth, 28),this, 1029);
		btnClose.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_CLOSE)));
		btnClose.ShowWindow(true);
	}

	if (initType == 2 || initType == 4) {

		// 手动录入人像框
		picIDByHand.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(120, 140, 222, 266),this, 1002);
		picIDByHand.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD)));
		picIDByHand.ShowWindow(true);

		// 手动录入获取人像
		btnGetImage.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(105, 284, 236, 326),this, 1003);
		btnGetImage.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GETIMAGE)));
		btnGetImage.ShowWindow(true);

		// 姓名标签
		lblName.Create(_T("姓名*"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(30, 346, 80, 366), this, 1005);
		lblName.ShowWindow(true);

		// 身份证标签
		lblIDCode.Create(_T("身份证号*"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(30, 386, 100, 406), this, 1006);
		lblIDCode.ShowWindow(true);

		// 地址标签
		lblAddress.Create(_T("地址"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(30, 426, 80, 446), this, 1007);
		lblAddress.ShowWindow(true);

		// 生日标签
		lblBirthday.Create(_T("出生日期"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(30, 466, 100, 486), this, 1008);
		lblBirthday.ShowWindow(true);

		// 性别标签
		lblGender.Create(_T("性别"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(30, 506, 80, 526), this, 1009);
		lblGender.ShowWindow(true);

		// 年标签
		lblYear.Create(_T("年"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(180, 466, 200, 486), this, 1010);
		lblYear.ShowWindow(true);

		// 月标签
		lblMonth.Create(_T("月"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(230, 466, 250, 486), this, 1011);
		lblMonth.ShowWindow(true);

		// 日标签
		lblDay.Create(_T("日"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(280, 466, 300, 486), this, 1012);
		lblDay.ShowWindow(true);

		// 提交按钮
		btnSubmit.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(251, 341, 315, 405),this, 1013);
		btnSubmit.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_SUBMIT)));
		btnSubmit.ShowWindow(true);

		// 名称文本框
		edtName.Create(WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_BORDER, CRect(105, 343, 245, 369), this, 1014);
		edtName.ShowWindow(true);

		// 身份证号文本框
		edtIDCode.Create(ES_MULTILINE|ES_AUTOHSCROLL|WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_BORDER, CRect(105, 383, 245, 409), this, 1015);
		edtIDCode.ShowWindow(true);

		// 地址文本框
		edtAddress.Create(ES_MULTILINE|ES_AUTOHSCROLL|WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_BORDER, CRect(105, 423, 310, 449), this, 1015);
		edtAddress.ShowWindow(true);

		// 年文本框
		edtYear.Create(WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_BORDER, CRect(105, 463, 175, 489), this, 1016);
		edtYear.ShowWindow(true);

		// 月文本框
		edtMonth.Create(WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_BORDER, CRect(205, 463, 225, 489), this, 1017);
		edtMonth.ShowWindow(true);

		// 日文本框
		edtDay.Create(WS_CHILD|WS_VISIBLE|WS_TABSTOP|WS_BORDER, CRect(255, 463, 275, 489), this, 1018);
		edtDay.ShowWindow(true);

		// 性别男
		btnMan.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(105, 495, 174, 539),this, 1019);
		btnMan.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MANCHECK)));
		btnMan.ShowWindow(true);

		// 性别女
		btnGirl.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(190, 495, 249, 539),this, 1020);
		btnGirl.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GIRLUNCHECK)));
		btnGirl.ShowWindow(true);
	}

	if (initType == 3 || initType == 1) {
		// 证件人像框
		picIDCard.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(378 - handWindowWidth, 140, 480 - handWindowWidth, 266),this, 1001);
		picIDCard.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD)));
		picIDCard.ShowWindow(true);
		

		// 读卡器姓名标签
		lblCardName.Create(_T("姓名"), WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(330 - handWindowWidth, 290, 528 - handWindowWidth, 310), this, 1021);
		lblCardName.ShowWindow(true);	

		// 读卡器卡号标签
		lblCardCode.Create(_T("证件号"), WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(330 - handWindowWidth, 320, 528 - handWindowWidth, 340), this, 1022);
		lblCardCode.ShowWindow(true);

		// 读卡器地址标签
		lblCardAddress.Create(_T("地址"), WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(330 - handWindowWidth, 350, 528 - handWindowWidth, 410), this, 1023);
		lblCardAddress.ShowWindow(true);

		//// 人像比对结果
		//lblCardMatch.Create(_T("相似度"), WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(330 - handWindowWidth, 410, 538 - handWindowWidth, 450), this, 1024);
		//lblCardMatch.ShowWindow(true);

		//// 人工审核通过
		//btnConfirm.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(375 - handWindowWidth, 460, 439 - handWindowWidth, 513),this, 1025);
		//btnConfirm.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAYCONFIRM)));
		//btnConfirm.ShowWindow(true);

		//// 人工审核否决
		//btnRefuse.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(435 - handWindowWidth, 460, 499 - handWindowWidth, 513),this, 1026);
		//btnRefuse.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAYREFUSE)));
		//btnRefuse.ShowWindow(true);
	}

		
	// 版本更新
	btnVersion.Create("Ver", WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(860 - handWindowWidth, 557 - handWindowHeight, 942 - handWindowWidth, 577 - handWindowHeight),this, 1027);
	btnVersion.ShowWindow(true);

	CString typeName;
	if (initType == 2)
		typeName = "宾馆版";
	else if (initType == 1 || initType == 5) 
		typeName = "网吧版";
	else if (initType == 3)
		typeName = "加油站版";
	else if (initType == 4)
		typeName = "网吧独立版";
	lblType.Create(typeName, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(860 - handWindowWidth, 537 - handWindowHeight, 942 - handWindowWidth, 557 - handWindowHeight),this, 1028);
	lblType.ShowWindow(true);

	if (initType != 5) {
		menuTray.LoadMenu(IDR_MENUNOTIFY);
	} else {
		menuTray.LoadMenu(IDR_MENUFORPUBWIN);
	}
	if (fullScreen == 0) {
		menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_UNCHECKED);
	} else {
		menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_CHECKED);
	}

	// 初始化锁
	InitializeCriticalSection(&g_CriticalSection);

	// 初始化数据
	cmdExit = false;
	genderMan = true;
	validByHand = false;
	videoDetect = false;
	doGetImage = false;
	ifGetImage = false;
	doReading = true;
	doListening = true;
	doReceiving = false;
	doComparing = false;
	doDetecting = true;
	doFilting = true;
	pauzeReading = false;
	pauzeDetecting = true;
	initFilter = false;
	initing = true;
	faceWidthConst = 102;
	faceHeightConst = 126;
	cardReaderPort = 1001;
	maxScore = 0;
	matchCount = 0;
	maxMatchCountConst = 5;
	scoreThreshold = 50;
	scoreThresholdHigh = 65;
	featureSize = EF_Size();
	idFeature = new BYTE[featureSize];
	//appkey = "4adfd670ef2cbc72b9a3aceacb73c1c8";
	//appsecret = "5fefb260863ad70bfcf142bb84c4c8f6";

	if (initType == 5) {
		m_hHook = SetWindowsHookEx(WH_KEYBOARD_LL,KeybdProc,AfxGetInstanceHandle(),0);
		if (m_hHook == NULL) {
			AfxMessageBox("快捷键设置失败");
		}
	}

	//ResultDlg resultDlg;
	//	resultDlg.Init(0, (int)maxScore,idFile, avatarFile, fullFile, fullScreen);
	//resultDlg.DoModal();
	
		BOOL bRet;
	bRet=rfdInit();
	if (bRet) {
		bRet=rfdCheck();
		if(bRet){
			CHAR tname[50];
			CHAR tgender[10];
			CHAR tnation[20];
			CHAR tbornday[10];
			CHAR taddress[80];
			CHAR tidcode[20];
			CHAR torgn[50];
			CHAR tstartday[10];
			CHAR tendday[10];
			CHAR tpath[256];
			memset(tname,0,50);
			memset(tgender,0,10);
			memset(tnation,0,20);
			memset(tbornday,0,10);
			memset(taddress,0,80);
			memset(tidcode,0,20);
			memset(torgn,0,50);
			memset(tstartday,0,10);
			memset(tendday,0,10);
			bRet=rfdReadInfo(tname,tgender,tnation,tbornday,taddress,tidcode,torgn,tstartday,tendday,"C:\\Users\\Administrator\\Documents",1288);
			if(bRet) {
			}
		}
	}
	
	AfxBeginThread(InitThreadProc, this);

	//CString id_card_img;
	//CString id_card_img_md5;
	//CString id_card_info;
	//CString place_id;
	//CString full_img;
	//CString full_img_md5;
	//char retVal[10];

	//id_card_img = "C:\\Users\\Administrator\\Desktop\\temp\\id.jpg";
	//id_card_img_md5 = Md5file(id_card_img);
	//place_id = "1";
	//id_card_info = "王-男-汉-19930331-浙江省平阳县-330184198501184115-平阳县公安局-20090122-20190122";
	//full_img = "C:\\Users\\Administrator\\Desktop\\temp\\full.jpg";
	//full_img_md5 = Md5file(full_img);
	//
	//apiGasDoSimilarity(appkey.GetBuffer(), appsecret.GetBuffer(), id_card_img.GetBuffer(), id_card_img_md5.GetBuffer(),id_card_info.GetBuffer(),
	//	place_id.GetBuffer(), full_img.GetBuffer(), full_img_md5.GetBuffer(), retVal);
	//printf("apiGasDoSimilarity result:%s\n",retVal);
	//
	//Message(retVal);
	//CString comcode(retVal);

	//apiGetSimilarityVal(appkey.GetBuffer(), appsecret.GetBuffer(),comcode.GetBuffer(), place_id.GetBuffer(), retVal);

	//Message(retVal);

	//apiDoResultSure(appkey.GetBuffer(), appsecret.GetBuffer(),comcode.GetBuffer(),place_id.GetBuffer(),"0", retVal);

	//Message(retVal);


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}


// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CfacerecoDlg::OnPaint()
{
	if (IsIconic())
	{
	}
	else
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		CRect winRect;
		GetClientRect(&winRect);
		CDC dcMem;
		dcMem.CreateCompatibleDC(&dc);
		CBitmap bmpBackground;
		if (initType == 2 || initType == 4) {
			bmpBackground.LoadBitmap(IDB_HANDBACKGROUND);
		} else if (initType == 5) {
			bmpBackground.LoadBitmap(IDB_BACKGROUNDSINGLE);
		} else {
			bmpBackground.LoadBitmap(IDB_BACKGROUNDSMALL);
		}
		BITMAP bitmap;
		bmpBackground.GetBitmap(&bitmap);
		CBitmap* pbmpOld = dcMem.SelectObject(&bmpBackground); 
		dc.StretchBlt(0,0,winRect.Width(),winRect.Height(),&dcMem,0,0,bitmap.bmWidth,bitmap.bmHeight,SRCCOPY);

		if (initing) {
			winRect.top += 80;
			
			CFont font;
			font.CreatePointFont(200,_T("微软雅黑"),NULL);
			dc.SelectObject(font);
			dc.SetTextColor(RGB(255,0,0));
			dc.SetBkMode(TRANSPARENT);
			dc.DrawText("正在初始化, 请勿进行任何操作...",&winRect, DT_CENTER);
		}

		CFont normal;
		normal.CreatePointFont(100,_T("微软雅黑"),NULL);

		if (initType == 2 || initType == 4) {
			DrawLabel(&lblName, &normal, DT_LEFT);
			DrawLabel(&lblIDCode, &normal, DT_LEFT);
			DrawLabel(&lblAddress, &normal, DT_LEFT);
			DrawLabel(&lblBirthday,&normal,  DT_LEFT);
			DrawLabel(&lblGender,&normal,  DT_LEFT);
			DrawLabel(&lblYear, &normal, DT_LEFT);
			DrawLabel(&lblMonth, &normal, DT_LEFT);
			DrawLabel(&lblDay, &normal, DT_LEFT);
		}

		DrawLabel(&lblType, &normal, DT_CENTER);

		if (initType == 3 || initType == 1) {
			DrawLabel(&lblCardName, &normal, DT_CENTER);
			DrawLabel(&lblCardCode, &normal, DT_CENTER);
			DrawLabel(&lblCardAddress, &normal, DT_CENTER|DT_WORDBREAK);
			//DrawMatch();
		}

		DrawVersion();

		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CfacerecoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

LRESULT CfacerecoDlg::OnNcHitTest(CPoint point) {
	return HTCAPTION;
}

BOOL CfacerecoDlg::PreTranslateMessage(MSG* pMsg) {
	if(pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
			case VK_RETURN:    // 屏蔽回车
				return true;
			case VK_ESCAPE:
				return true;
		}
	}
	else if (pMsg->message == WM_COMMAND)
	{
		return true;
	} else if (pMsg->message == WM_SYSCOMMAND) {
		::ShowWindow(this->m_hWnd, SW_SHOW);
		pauzeReading = FALSE;
	}
   
    return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CfacerecoDlg::OnNotifyIcon(WPARAM wParam,LPARAM IParam) {
	if (wParam == IDI_ICON1) {
		UINT uMsg = (UINT)IParam;


		switch(uMsg) {
			case WM_RBUTTONUP:
			{
				CPoint point;
				int id;
				GetCursorPos(&point);

				id = menuTray.GetSubMenu(0)->TrackPopupMenu(TPM_RETURNCMD | TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this);
				if (id == ID_UPDATE) {
					char newVersion[20];
					char urls[800];

					CString curVersion;
					curVersion.Format("v%s", version);
					int res = apiCheckVersion(appkey.GetBuffer(), appsecret.GetBuffer(), curVersion.GetBuffer(), internet_bar_id.GetBuffer(), newVersion, urls);

					if (res == -1) {
						Message("更新失败, 请确保网络已连接");
					} else if (res == 0) {
						Message("当前版本已为最新版");
					} else if (res == 1) {
						CString msg;
						msg.Format("当前版本: %s\n最新版本: %s\n是否更新?", curVersion, newVersion);
						choseDlg hDlg;
						hDlg.setTip(msg.GetBuffer());
						INT_PTR nResponse = hDlg.DoModal();
						if (nResponse == IDOK) {
							CString nVersion(newVersion);
							CString url(urls);
							
							BOOL res = UpdateVersion(nVersion, url);
							if (res) {
								Message("版本已更新, 重启后生效. 即将关闭程序, 请手动重启!!");
								if (!doComparing)
									Exit();
								else
									cmdExit = TRUE;
							}
						}
					}
				} else if (id == ID_RESET) {
					if (!doComparing) {
						Exit();
					}
				} else if (id == ID_EXIT) {
					if (!doComparing) {
						SendProtect("cmd:exit");
						Sleep(1000);
						Exit();
					}
					else
						cmdExit = TRUE;
				} else if (id == ID_FULLSCREEN) {
					if (fullScreen == 1) {
						fullScreen = 0;
						WritePrivateProfileString("config","fullscreen", "0", storeConfig);
						menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_UNCHECKED);
					} else {
						fullScreen = 1;
						WritePrivateProfileString("config","fullscreen", "1", storeConfig);
						menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_CHECKED);
					}
				} else if (id == ID_Settle) {
					if (initing) break;

					pCapture->DeviceConfig(0);
				}
				break;
			}
			case WM_LBUTTONDBLCLK:
				pauzeReading = FALSE;
				SetForegroundWindow();
				ShowWindow(SW_SHOWNORMAL);
				break;
			default:
				break;
		}
	}
	return 1;
}

UINT CfacerecoDlg::VideoThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doListening) 
	{
		if (!pDlg->pauzeReading)
			pDlg->DoListening();
		Sleep(30);
	}
	::CoUninitialize();

	return 1;
}


UINT CfacerecoDlg::SocketThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doReceiving) {
		if (pDlg->videoDetect) {
			Sleep(100);
			continue;
		}

		pDlg->DoReceiving();
		Sleep(30);
	}

	::CoUninitialize();

	return 1;
}


UINT CfacerecoDlg::ReaderThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doReading) {
		if (pDlg->videoDetect) {
			Sleep(100);
			continue;
		}

		if (!pDlg->pauzeReading)
			pDlg->DoReading();
		Sleep(30);
	}
	::CoUninitialize();

	return 1;
}

UINT CfacerecoDlg::DetectThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doDetecting) {
		if (pDlg->videoDetect) {
			Sleep(100);
			continue;
		}

		if (!pDlg->pauzeDetecting)
			pDlg->DoDetecting();
		Sleep(30); 
	}
	::CoUninitialize();

	return 1;
}


UINT CfacerecoDlg::FilterThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doFilting) {
		if (pDlg->videoDetect) {
			Sleep(100);
			continue;
		}

		pDlg->DoFilting();
		
		Sleep(30); 
	}

	::CoUninitialize();
	return 1;
}

UINT CfacerecoDlg::InitThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	pDlg->DoIniting();

	::CoUninitialize();
	return 1;

}

void CfacerecoDlg::OnStnClickedGetImage() {
	if (!doComparing) {
		picIDByHand.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD)));
		ifGetImage = FALSE;
		doGetImage = TRUE;
		
		scanFaceTime = CTime::GetCurrentTime();
	}

	//lblCardAddress.SetWindowText("浙江省温州市");
	//Invalidate(true); 
	//UpdateWindow();
}

void CfacerecoDlg::OnStnClickedSubmit() {
	if (initType == 2 || initType == 4) {
		if (!ifGetImage || !bmpStoreFaceByHand.m_hObject) {
			Message("请先获取用户头像");
			return;
		}
	}

	if (ifGetImage) {
		SaveCBitmapToBmpFile(&bmpStoreFaceByHand, handFaceFile);
	}

	edtName.GetWindowTextA(byHandName);
	edtIDCode.GetWindowTextA(byHandCode);
	edtAddress.GetWindowTextA(byHandLocation);
	edtYear.GetWindowTextA(byHandYear);
	edtMonth.GetWindowTextA(byHandMonth);
	edtDay.GetWindowTextA(byHandDay);

	if (byHandName == "") {
		Message("请先填写正确用户名");
		return;
	}

	if (byHandCode == "") {
		Message("请先填写正确身份证号");
		return;
	}


	CString sex;
	if (genderMan)
		sex = "1";
	else
		sex = "0";

	CString birth_day;
	if (byHandYear.GetLength() > 0 && byHandMonth.GetLength() > 0 && byHandDay.GetLength() > 0)
		birth_day.Format("%s-%s-%s", byHandYear, byHandMonth, byHandDay);
	CString video_img_md5;
	CString imgFile = handFaceFile;
	if (ifGetImage) {
		video_img_md5 = Md5file(imgFile.GetBuffer());
	} else {
		imgFile = "";
	}


	CString location;
	if (byHandLocation.GetLength() > 0) {
		if (initType == 2)
			location.Format("【宾馆】%s", byHandLocation);
		else if (initType == 4)
			location.Format("【网吧】%s", byHandLocation);
	}

	BOOL res = FALSE;
	if (initType == 2) {
		res = apiHotelHand(appkey.GetBuffer(), appsecret.GetBuffer(), imgFile.GetBuffer(), byHandName.GetBuffer(), byHandCode.GetBuffer(),video_img_md5.GetBuffer(), sex.GetBuffer(), "3",
			internet_bar_id.GetBuffer(), birth_day.GetBuffer(), location.GetBuffer());
	} else if (initType == 4) {
		res = apiNetBarHand(appkey.GetBuffer(), appsecret.GetBuffer(), imgFile.GetBuffer(), byHandName.GetBuffer(), byHandCode.GetBuffer(),video_img_md5.GetBuffer(), sex.GetBuffer(), "3",
			internet_bar_id.GetBuffer(), birth_day.GetBuffer(), location.GetBuffer());
	}

	if (res) {
		Message("上传成功");
		edtYear.SetWindowTextA("");
		edtMonth.SetWindowTextA("");
		edtDay.SetWindowTextA("");
		edtName.SetWindowTextA("");
		edtIDCode.SetWindowTextA("");
		edtAddress.SetWindowTextA("");
		picIDByHand.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD)));

	} else {
		Message("上传失败, 请填写正确信息");
	}
}

void CfacerecoDlg::OnStnClickedMinimize() {
	HideMain();
}

void CfacerecoDlg::OnStnClickedMan() {
	if (genderMan) return;

	btnMan.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MANCHECK)));
	btnGirl.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GIRLUNCHECK)));
	genderMan = TRUE;
}

void CfacerecoDlg::OnStnClickedGirl() {
	if (!genderMan) return;

	btnMan.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MANUNCHECK)));
	btnGirl.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GIRLCHECK)));
	genderMan = FALSE;
}

void CfacerecoDlg::OnStnClickedConfirm() {
	if (!validByHand) return;

	btnConfirm.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAYCONFIRM)));
	btnRefuse.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAYREFUSE)));

	UploadInfo(2);

	// 发送消息
	SendSocket("res:2");

	doComparing = FALSE;
	validByHand = FALSE;
}

void CfacerecoDlg::OnStnClickedRefuse() {
	if (!validByHand) return;

	btnConfirm.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAYCONFIRM)));
	btnRefuse.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAYREFUSE)));

	// 发送消息
	SendSocket("res:0");
	
	if (cmdExit) {
		Exit();
	}

	validByHand = FALSE;
	doComparing = FALSE;
}

void CfacerecoDlg::OnStnClickedVersion() {
	char newVersion[20];
	char urls[800];

	CString curVersion;
	curVersion.Format("v%s", version);
	int res = apiCheckVersion(appkey.GetBuffer(), appsecret.GetBuffer(), curVersion.GetBuffer(), internet_bar_id.GetBuffer(), newVersion, urls);

	if (res == -1) {
		Message("更新失败, 请确保网络已连接");
	} else if (res == 0) {
		Message("当前版本已为最新版");
	} else if (res == 1) {
		CString nVersion(newVersion);
		CString url(urls);
		
		BOOL res = UpdateVersion(nVersion, url);
		if (res) {
			Message("版本已更新, 重启后生效. 即将关闭程序, 请手动重启!!");
			if (!doComparing)
				Exit();
			else
				cmdExit = TRUE;
		}
	}
}

void CfacerecoDlg::OnStnClickedClose() {
	if (doComparing) {
		Message("正在识别中, 请等待识别完成");
		return;
	}
	
	SendProtect("cmd:exit");
	Sleep(1000);
	Exit();
}

void CfacerecoDlg::DoListening() {
	int nWidth = pCapParam.szCap.cx;
	int nHeight = pCapParam.szCap.cy;
	long lLength=nWidth*nHeight*3;

	BYTE* pCamBuf=new BYTE[lLength];
	BITMAPINFOHEADER bih;
	ContructBih(nWidth,nHeight,bih);
	BOOL bGet=pCapture->GetFrame(pCamBuf, lLength);


	//Mat im;
	//BOOL bGet = TRUE;
	//try {
	//	im = imread("C:\\full.jpg");
	//} catch(...) {
	//	doComparing = FALSE;
	//	return;
	//}
	//
	//int nWidth=im.cols;
	//int nHeight = im.rows;

	//long lLength = nWidth * nHeight * 3;
	//BITMAPINFOHEADER bih;
	//ContructBih(nWidth,nHeight,bih);

	//BYTE* pCamBuf = new BYTE[lLength];
	//memcpy(pCamBuf,im.data,nWidth*nHeight*3);
	//MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, FALSE,24);

	if (bGet) {
		if (videoDetect) {
			CTime temp;
			temp = CTime::GetCurrentTime();

			int timeGap = temp.GetTime() - scanFaceTime.GetTime();
			if (timeGap < 10) {
				double matchScore = 0;
				BYTE* pFeature = new BYTE[featureSize];
				CBitmap videoTemp;

				// 缓存视频图像
				videoTemp.DeleteObject();
				GetCBitmap(&bih, pCamBuf,videoTemp);

				MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, FALSE,24);
				int ret = DetectFace(pCamBuf, nWidth, nHeight, pFeature, FALSE, TRUE, TRUE);
				MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, FALSE,24);

				if (ret == 1) {
					matchCount++;
					matchScore=EF_Compare(pFeature,idFeature)*100+20.0f;
					if (matchScore > 100) matchScore = 99;

				/*	CString msg;
					msg.Format("%d", (int)matchScore);
					Message(msg.GetBuffer());*/

					if (matchScore > maxScore) {
						// 更新最大值
						maxScore = matchScore;
						// 更新缓存视频图像
						HGDIOBJ obj = bmpStoreVideo.Detach();
						DeleteObject(obj);
						bmpStoreVideo.Attach(CopyBitmap(videoTemp));
						// 更新缓存人脸图像
						obj = bmpStoreFace.Detach();
						DeleteObject(obj);
						bmpStoreFace.Attach(CopyBitmap(bmpFace));
							
						// 缓存视频人脸特征值
						videoFeatureHex = BYTEToHex(pFeature);
					}

					if (matchCount >= maxMatchCountConst) {
						// 停止监测人脸
						videoDetect = FALSE;

						// 更新窗口
						Invalidate(true); 
						UpdateWindow();

						// 保存视频文件
						SaveCBitmapToBmpFile(&bmpStoreVideo, fullFile);
						// 保存人脸文件
						SaveCBitmapToBmpFile(&bmpStoreFace, avatarFile);
						if (maxScore >= scoreThreshold) {
							if (maxScore >= scoreThresholdHigh) {
								resultDlg.Init(1, (int)maxScore,idFile, avatarFile, fullFile, fullScreen);
							} else {
								resultDlg.Init(2, (int)maxScore,idFile, avatarFile, fullFile, fullScreen);
							}
							resultDlg.DoModal();

							int response = resultDlg.Response();
							if (response == 0) {
								// 识别匹配结束
								SendProtect("status:compared");
								Sleep(200);
								doComparing = FALSE;

								SendSocket("res:0");
							} else {
								UploadInfo(response);
								// 识别匹配结束
								SendProtect("status:compared");
								Sleep(200);
								doComparing = FALSE;

								CString msg;
								msg.Format("res:%d", response);
								SendSocket(msg.GetBuffer());
							}

							HideMain();
						} else {
							resultDlg.Init(4, (int)maxScore,idFile, avatarFile, fullFile, fullScreen);
							resultDlg.DoModal();

							// 识别匹配结束
							SendProtect("status:compared");
							Sleep(200);
							videoDetect = FALSE;
							doComparing = FALSE;
							SendSocket("res:0");

							HideMain();
						}
					}
				}
				videoTemp.DeleteObject();
				delete []pFeature;
			} else {
				resultDlg.Init(0, (int)0,"", "", "", fullScreen);
				Clear();
				resultDlg.DoModal();

				// 识别匹配超时
				SendProtect("status:compared");
				Sleep(200);
				videoDetect = FALSE;
				doComparing = FALSE;
				SendSocket("res:0");

				HideMain();
			}
		} else if (doGetImage) {
			CTime temp;
			temp = CTime::GetCurrentTime();

			int timeGap = temp.GetTime() - scanFaceTime.GetTime();
			if (timeGap < 10) {
				MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, FALSE,24);
				int ret = DetectFace(pCamBuf, nWidth, nHeight, NULL, FALSE, TRUE, FALSE);
				if (ret == 1) {
					// 更新缓存人脸图像
					bmpStoreFaceByHand.Detach();
					bmpStoreFaceByHand.Attach(CopyBitmap(bmpFace));

					picIDByHand.SetBitmap(bmpStoreFaceByHand);
					doGetImage = FALSE;
					ifGetImage = TRUE;
				}
				MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, FALSE,24);
			} else {
				doGetImage = FALSE;
				ifGetImage = TRUE;
			}
		}
		DrawBmpBuf(bih,pCamBuf,picVideo.m_hWnd,TRUE,TRUE);
	}

	delete[] pCamBuf;
}

void CfacerecoDlg::DoReceiving() {
	SOCKADDR_IN saClient;
	char cRecvBuff[400];
	int nSize,nbSize;
	nSize = sizeof (SOCKADDR_IN);

	if((nbSize=recvfrom(receiverSocket,cRecvBuff,400,0,(SOCKADDR FAR *) &saClient,&nSize))==SOCKET_ERROR) {
		return;
	}
	cRecvBuff[nbSize] = '\0';
	CString msg;
	msg = cRecvBuff;

	CStringArray ary;
	SplitCString(cRecvBuff, ary, ';');
	if (ary.GetSize() > 1) {
		if (ary.ElementAt(0) == "cmd" && ary.ElementAt(1) == "exit") {
			SendProtect("cmd:exit");
			Sleep(1000);
			cmdExit = TRUE;
		}
		if (!doComparing) {
			if (ary.ElementAt(0) == "idinfo") {
				if (ary.GetSize() == 12) {
					if(CopyFile(ary.ElementAt(1),idFile, FALSE)) {
						SetFocus();
						ShowWindow(SW_SHOW);
						SetFocus();

						name=ary.ElementAt(2);
						gender=ary.ElementAt(3);
						nation=ary.ElementAt(4);
						bornday=ary.ElementAt(5);
						location=ary.ElementAt(6);
						idcode=ary.ElementAt(7);
						polistation=ary.ElementAt(8);
						startday=ary.ElementAt(9);
						enday=ary.ElementAt(10);
						internet_bar_id = ary.ElementAt(11);

						DetectIDFace(TRUE);
					}
				} else {
					SendSocket("res:0");
				}
			} else if (ary.ElementAt(0) == "cmd") {
				if (ary.ElementAt(1) == "hide") {
					Clear();
					ShowWindow(SW_HIDE);
				} else if (ary.ElementAt(1) == "show") {
					ShowWindow(SW_SHOW);
				} else if (ary.ElementAt(1) == "clear") {
					Clear();
				} else if (ary.ElementAt(1) == "exit") {
					Exit();
				}
			}
		} else {
			// 发送消息
			SendSocket("Bus");
		}
	}
}


void CfacerecoDlg::DoReading() {
	if (!doComparing) {
		DetectIDFace(FALSE);
	}
}

void CfacerecoDlg::DoDetecting() {
	if (!doComparing) {

		CTime temp;
		temp = CTime::GetCurrentTime();
		int timeGap = temp.GetTime() - detectingTime.GetTime();
		if (timeGap > 10) {
			pauzeDetecting = TRUE;
			doComparing = FALSE;
			spiritDlg->setState(1);
			
			return;
		}

		CFileFind find;
		CTime lastTime = NULL;
		CTime tempTime;
		CString fileName;
		CString filePath = "";
		BOOL bf = find.FindFile(detectPath);
		while (bf) {
			bf = find.FindNextFile();
			if(!find.IsDots()) {
				find.GetLastWriteTime(tempTime);
				if (lastTime == NULL) {
					lastTime = tempTime;
					fileName = find.GetFileName();
					filePath = find.GetFilePath();
				} else if (tempTime > lastTime) {
					lastTime = tempTime;
					fileName = find.GetFileName();
					filePath = find.GetFilePath();
				}
			}
		}


		if (filePath != "") {
			if (CopyFile(find.GetFilePath(),idFile, FALSE)) {
				idcode = fileName.Left(fileName.GetLength() - 6);
				::ShowWindow(this->m_hWnd, SW_SHOW);
				pauzeReading = FALSE;
				pauzeDetecting = TRUE;

				DetectIDFace(true);
			}
			remove(filePath);
		}
	}
}

void CfacerecoDlg::DoFilting() {
	if (!doComparing) {
		BOOL bRet;
		CHAR tname[50];
		CHAR tgender[10];
		CHAR tnation[20];
		CHAR tbornday[10];
		CHAR taddress[80];
		CHAR tidcode[20];
		CHAR torgn[50];
		CHAR tstartday[10];
		CHAR tendday[10];
		CHAR tpath[256];
		memset(tname,0,50);
		memset(tgender,0,10);
		memset(tnation,0,20);
		memset(tbornday,0,10);
		memset(taddress,0,80);
		memset(tidcode,0,20);
		memset(torgn,0,50);
		memset(tstartday,0,10);
		memset(tendday,0,10);
		bRet=rfdReadInfo(tname,tgender,tnation,tbornday,taddress,tidcode,torgn,tstartday,tendday,"C:\\Users\\Administrator\\Documents",1288);
		if (bRet) {
			Message(tname);
			Message(tgender);
			Message(tnation);
			Message(tbornday);
			Message(taddress);
			Message(tidcode);
			Message(torgn);
			Message(tstartday);
			Message(tendday);

		}
	}
}

void CfacerecoDlg::DoIniting() {
	
	// 加载驱动
	if (initType == 1) {
		BOOL bRet=loadDriver("ReadFilter", readFilterPath.GetBuffer());
		if (!bRet) {
			Message("加载驱动失败, 请确保读卡器已连接.");
			Exit();
			return;
		}

		bRet=rfdInit();
		if (!bRet) {
			Message("初始化设备失败.");
			Exit();
			return;
		}

		bRet=rfdCheck();
		if (!bRet) {
			Message("测试设备失败, 请确保设备已连接");
			Exit();
			return;
		}

		DoFilting();
	
		//AfxBeginThread(FilterThreadProc, this);

		initFilter = TRUE;
	}

	// 初始化摄像头
	pCapture = MCapture::Create(&pCapParam);
	if (pCapture == NULL) {
		Message("未识别摄像头设备, 请确保设备已连接.");
		Exit();
		return;
	}
	BOOL bOK=pCapture->InitCapture();
	if (!bOK) {
		Message("未识别摄像头设备, 请确保设备已连接.");
		Exit();
		return;
	}

	// 初始化识别程序
	THFI_Param param;
	param.nMinFaceSize=40;
	param.nRollAngle=45;
	param.bOnlyDetect=true;
	int ret = THFI_Create(2,&param);
	if (ret != 2) {
		Message("未识别摄像头设备, 请确保设备已连接.");
		Exit();
		return;
	}

	EF_Init(2);
	pCapture->Play();

	// 请求阈值
	QueryThreshold();

	BOOL buildSocket = FALSE;
	// 创建接收守护进程消息Socket
	buildSocket = BuildReceiverSocket(receiverProtect, receiverProtectPort, 5);
	if(!buildSocket)
	{
		Message("无法创建通信1.\n");
		Exit();
		return ;
	}

	// 创建发送守护进程消息Socket
	buildSocket = BuildSenderSocket(senderProtect, senderProtectPort, saUdpServProtect);
	if(!buildSocket)
	{
		Message("无法创建通信2.\n");
		Exit();
		return ;
	}

	// 打开守护进程
	WinExec(appPath + "\\faceprotect.dll", SW_SHOW);
	Sleep(8000);

	SOCKADDR_IN saClient;
	char cRecvBuff[200];
	int nSize,nbSize;
	nSize = sizeof (SOCKADDR_IN);

	SendProtect("status:inited");
	Sleep(8000);
	if((nbSize=recvfrom(receiverProtect,cRecvBuff,200,0,(SOCKADDR FAR *) &saClient,&nSize))==SOCKET_ERROR) {
		Message("无法创建通信3.\n");
		Exit();
		return;
	}


	if (initType == 1) {
		doReceiving = TRUE;

		// 设置接收信息socket
		buildSocket = BuildReceiverSocket(receiverSocket, receiverPort, 0);
		if(!buildSocket)
		{
			Message("无法创建通信4.\n");
			Exit();
			return ;
		}

		// 设置发送信息socket
		buildSocket = BuildSenderSocket(senderSocket, senderPort, saUdpServ);
		if(!buildSocket)
		{
			Message("无法创建通信.\n");
			Exit();
			return ;
		}

		AfxBeginThread(SocketThreadProc, this);
	} else if (initType == 2 || initType == 3 || initType == 4) {
		// 打开读卡器

		int res;
     	res=InitComm(cardReaderPort);
		if (res != 1) {
			Message("无法开启读卡器, 请确保设备已连接.");
			Exit();
			return ;
		}

		AfxBeginThread(ReaderThreadProc, this);
	} else if (initType == 5) {
		name="无";
		gender="无";
		nation="无";
		bornday="19710101";
		location="未获取信息";
		polistation="未获取信息";
		startday="19710101";
		enday="19710101";
		AfxBeginThread(DetectThreadProc, this);
	}

	// 启用绘制摄像头线程
	AfxBeginThread(VideoThreadProc, this);

	initing = FALSE;

	Invalidate(TRUE);
	UpdateWindow();

	pauzeReading = TRUE;
	ShowWindow(SW_HIDE);
}

void CfacerecoDlg::LoadConfig() {
	// 摄像头初始值
	pCapParam.szCap.cx=640;
	pCapParam.szCap.cy=480;
	pCapParam.eType=CAP_WDM;
	pCapParam.lIndex=0;

	// 初始化类型
	initType = GetPrivateProfileInt("identities","initype",0,config);
	// appkey
	GetPrivateProfileString("identities","appkey","0", appkey.GetBuffer(33), 33,config);

	// appsecret
	GetPrivateProfileString("identities","appsecret","0", appsecret.GetBuffer(33), 33,config);

	// 摄像头编号
	pCapParam.lIndex = GetPrivateProfileInt("camera","id",0,config);
	// 发送端口
	senderPort = GetPrivateProfileInt("socket","sport",7002,config);
	// 接收端口
	receiverPort = GetPrivateProfileInt("socket","lport",7001,config);
	// 发送保护进程端口
	senderProtectPort = GetPrivateProfileInt("socket","spport",37777,config);
	// 接收守护进程端口
	receiverProtectPort = GetPrivateProfileInt("socket","lpport",36666,config);
	// 读卡器端口
	cardReaderPort=GetPrivateProfileInt("card","port",1001,config);

	// 读取场地编号
	GetPrivateProfileString("config","placeno","0", internet_bar_id.GetBuffer(15), 15,storeConfig);
	// 版本号
	GetPrivateProfileString("config","version","1.0.0", version.GetBuffer(15), 15,storeConfig);
	// 是否全屏
	fullScreen=GetPrivateProfileInt("config","fullscreen",0,storeConfig);
	// 读取监测路径
	GetPrivateProfileString("config", "detectpath", "", detectPath.GetBuffer(300), 300, storeConfig);
	detectPath.Format("%s\\*_H.jpg", detectPath.GetBuffer());

}

void CfacerecoDlg::QueryThreshold() {
	char threshold[4];
	char thresholdHigh[4];
	apiThreshold(appkey.GetBuffer(), appsecret.GetBuffer(), threshold, thresholdHigh);

	scoreThreshold = atof(threshold);
	scoreThresholdHigh = atof(thresholdHigh);

	if (scoreThreshold == 0)
		scoreThreshold = 55;
	if (scoreThresholdHigh == 0)
		scoreThresholdHigh = 75;
}

void CfacerecoDlg::UploadInfo(int result) {
	if(!FileExists(idFile.GetBuffer())) return;
	if(!FileExists(avatarFile.GetBuffer())) return;
	if(!FileExists(fullFile.GetBuffer())) return;

	CString place;
	if (initType == 1 || initType == 4 || initType == 5) {
		place = "网吧";
	} else if (initType == 2) {
		place = "宾馆";
	} else if (initType == 3) {
		place = "加油站";
	}


	CString id_card_info;
	id_card_info.Format("%s-%s-%s-%s-【%s】%s-%s-%s-%s-%s", name, gender, nation, bornday, place, location,idcode, polistation, startday, enday);

	CString id_card_img_md5= Md5file(idFile.GetBuffer());
	CString vedio_img_md5 = Md5file(avatarFile.GetBuffer());
	CString full_img_md5= Md5file(fullFile.GetBuffer());

	CString res;
	res.Format("%d", result);
	similarity.Format("%d", (int)maxScore);

	apiUploadInfo(appkey.GetBuffer(), appsecret.GetBuffer(), idFile.GetBuffer(), avatarFile.GetBuffer(),
		fullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
		internet_bar_id.GetBuffer(), res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), initType);

	if (cmdExit) {
		Exit();
	}
}


BOOL CfacerecoDlg::UpdateVersion(CString& nVersion, CString& urls) {
	nVersion = nVersion.Mid(1, nVersion.GetLength() - 1);
	// 更新版本号
	char configFile[256];
	strcpy(configFile,appPath);
	strcat(configFile,"\\storage\\config.ini");
	WritePrivateProfileString("config","newvers", nVersion, configFile);

	// 创建新版本文件夹
	CString filePath;
	filePath.Format("%s\\%s", appPath, nVersion);
	::CreateDirectory(filePath,NULL);

	// 下载新版本组件
	CString fileName;
	CStringArray ary;
	SplitCString(urls, ary, ';');

	for (int i = 0; i < ary.GetSize(); i++) {
		CStringArray fileInfo;
		SplitCString(ary.ElementAt(i), fileInfo, '-');
		fileName.Format("%s\\%s.dll", filePath, fileInfo.ElementAt(0).GetBuffer());

		int res = DownloadFile(fileInfo.ElementAt(1), fileName);
		if (res != 0) {
			Message("版本更新失败, 请稍后再试");
			return FALSE;
		}
	}

	return TRUE;
}

void CfacerecoDlg::DrawMatch() {
	//CRect rect;
	//CString text;
	//CFont font;
	//text.Format("%d", (int)maxScore);
	//font.CreatePointFont(140,_T("微软雅黑"),NULL);
	//COLORREF white = RGB(255,255,255);
	//COLORREF red = RGB(255,0,0);

	//lblCardMatch.GetClientRect(&rect);
	//CPaintDC dcLabel(&lblCardMatch);
	//dcLabel.SelectObject(font);
	//dcLabel.SetTextColor(white);
	//dcLabel.SetBkMode(TRANSPARENT);

	//if (maxScore == 0) {
	//	dcLabel.DrawText("相似度",&rect,DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	//} else {
	//	dcLabel.DrawText("相似度",&rect,DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	//	rect.left+= 65;
	//	dcLabel.SetTextColor(red);
	//	dcLabel.DrawText(text,&rect,DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	//	rect.left+= 25;
	//	dcLabel.SetTextColor(white);
	//	dcLabel.DrawText("%",&rect,DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	//	if (maxScore >= scoreThresholdHigh) {
	//		rect.left+= 70;
	//		dcLabel.DrawText("通过",&rect,DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	//	}
	//	else {
	//		rect.left+= 30;
	//		dcLabel.DrawText("人工验证",&rect,DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	//	}
	//}
}

void CfacerecoDlg::DrawVersion() {
	CFont font;
	CRect rect;
	CString text;
	COLORREF white = RGB(255,255,255);
	text.Format("Ver %s", version);
	font.CreatePointFont(80,_T("微软雅黑"),NULL);

	btnVersion.GetClientRect(&rect);
	CPaintDC dcLabel(&btnVersion);
	dcLabel.SelectObject(font);
	dcLabel.SetTextColor(white);
	dcLabel.SetBkMode(TRANSPARENT);
	dcLabel.DrawText(text,&rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
}

void CfacerecoDlg::DrawLabel(CStatic* label, CFont* font, int prop) {
	CRect rect;
	CString text;
	COLORREF white = RGB(255,255,255);

	label->GetWindowTextA(text);
	label->GetClientRect(&rect);
	CPaintDC dcLabel(label);
	dcLabel.SelectObject(font);
	dcLabel.SetTextColor(white);
	dcLabel.SetBkMode(TRANSPARENT);
	dcLabel.DrawText(text,&rect, prop);
}

void CfacerecoDlg::DetectIDFace(BOOL fromFile) {

	Mat im;
	if (fromFile) {
		doComparing = TRUE;
		SendProtect("status:comparing");
		pauzeReading = FALSE;
	
	} else {
		if (Authenticate() != 1) return;
		doComparing = TRUE;
		SendProtect("status:comparing");
		pauzeReading = FALSE;

		doGetImage = FALSE;
		ifGetImage = FALSE;

		char tname[31];
		char tgender[3];
		char tfolk[10];
		char tbirthday[9];
		char tcode[19];
		char taddress[300];
		char tagency[100];
		char texpireStart[9];
		char texpireEnd[9];

		try {
			int res = ReadBaseInfosPhoto(tname,tgender,tfolk,tbirthday,tcode,taddress,tagency,texpireStart,texpireEnd,storagePath.GetBuffer());
			if (res != 1) {
				doComparing = FALSE;
				return;
			}
		
			TrimAry(tname, 31);
			TrimAry(tgender, 3);
			TrimAry(tfolk, 10);
			TrimAry(tbirthday, 9);
			TrimAry(tcode, 19);
			TrimAry(taddress, 300);
			TrimAry(tagency, 100);
			TrimAry(texpireStart, 9);
			TrimAry(texpireEnd, 9);

		} catch (...) {
			doComparing = FALSE;
			return;
		}

		name=tname;
		gender=tgender;
		nation=tfolk;
		bornday=tbirthday;
		location=taddress;
		idcode=tcode;
		polistation=tagency;
		startday=texpireStart;
		enday=texpireEnd;
	}


	try {
		im = imread(idFile.GetBuffer());
	} catch(...) {
		doComparing = FALSE;
		return;
	}
	
	int nWidth=im.cols;
	int nHeight = im.rows;

	long lLength = faceWidthConst * faceHeightConst * 3;
	BITMAPINFOHEADER bih;
	ContructBih(faceWidthConst,faceHeightConst,bih);

	BYTE* pCamBuf = new BYTE[lLength];
	memcpy(pCamBuf,im.data,nWidth*nHeight*3);

	int ret = DetectFace(pCamBuf, nWidth, nHeight, idFeature, FALSE, TRUE, TRUE);
	if (ret == 1) {
		LoadBmpFromFile(idFile, bmpIDAvatar);

		
		if (initType == 2 || initType == 4) {
			picIDByHand.SetBitmap(bmpIDAvatar);

			edtYear.SetWindowTextA(bornday.Left(4));
			edtMonth.SetWindowTextA(bornday.Mid(4,2));
			edtDay.SetWindowTextA(bornday.Mid(6,2));
			edtName.SetWindowTextA(name);
			edtIDCode.SetWindowTextA(idcode);
			edtAddress.SetWindowTextA(location);
			if (gender == "男") {
				btnMan.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MANCHECK)));
				btnGirl.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GIRLUNCHECK)));
				genderMan = TRUE;
			} else {
				btnMan.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MANUNCHECK)));
				btnGirl.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GIRLCHECK)));
				genderMan = FALSE;
			}
		} else if (initType == 5) {

		} else {
			picIDCard.SetBitmap(bmpIDAvatar);
			lblCardName.SetWindowTextA(name);
			lblCardCode.SetWindowTextA(idcode);
			lblCardAddress.SetWindowTextA(location);
		}

		Invalidate(true);
		UpdateWindow();

		// 缓存证件特征值
		idFeatureHex = BYTEToHex(idFeature);

		// 重置参数
		maxScore = 0;
		matchCount = 0;
		// 开始监测人脸
		videoDetect = TRUE;
		scanFaceTime = CTime::GetCurrentTime();
	} else {
		doComparing = FALSE;
		HideMain();
	}
	delete []pCamBuf;
}

int CfacerecoDlg::DetectFace(BYTE* pBuf, int width, int height, BYTE* pFeature, BOOL detectOnly, BOOL drawFaceBox, BOOL genFeature) {
	// 开始人脸检测
	THFI_FacePos facePos[1];
	facePos[0].dwReserved = (DWORD) new BYTE[512];
	int face = THFI_DetectFace(0, pBuf, 24, width, height, facePos, 1);

	FaceAngle angle = facePos->fAngle;
	if (face > 0 && abs(angle.yaw) <= 20 && abs(angle.pitch) <= 20) {
		if (genFeature) {
			memset(pFeature,0,featureSize);
			int ret = EF_Extract(0, pBuf, width, height, 3, (DWORD)&facePos[0], pFeature);
			if (!ret) {
				return -1;
			}
		} 

		if (!detectOnly) {
			// 获取人脸部分图片
			RECT rcFace=facePos[0].rcFace;
			int faceWidth = rcFace.right - rcFace.left;
			int faceHeight = rcFace.bottom - rcFace.top;
			BYTE* pFaceBuf = new BYTE[faceWidth*faceHeight*3];
			::GetPartImage(pBuf,width,height,pFaceBuf,rcFace.left,rcFace.top,rcFace.right,rcFace.bottom);

			// 垂直人脸镜像
			LONG lineBytes;
			lineBytes = WIDTHBYTES(faceWidth *24);
			BYTE* pVertical = new BYTE[lineBytes*faceHeight];
			for(int i=0;i<faceHeight;i++)
			{
				for(int j=0;j<faceWidth;j++)
				{
					pVertical[i*lineBytes+j*3+0]=pFaceBuf[(faceHeight-1-i)*faceWidth*3+j*3+0];
					pVertical[i*lineBytes+j*3+1]=pFaceBuf[(faceHeight-1-i)*faceWidth*3+j*3+1];
					pVertical[i*lineBytes+j*3+2]=pFaceBuf[(faceHeight-1-i)*faceWidth*3+j*3+2];
				}
			}
			delete []pFaceBuf;

			// 缩放至 102 x 126 大小人脸
			BITMAPINFOHEADER bih;
			::ContructBih(faceWidth,faceHeight,bih);
			BYTE* pFace=new BYTE[WIDTHBYTES(faceWidthConst *24)*faceHeightConst];
			::ResampleBmp(bih,pVertical,faceWidthConst,faceHeightConst,pFace);
			delete []pVertical;

			// 缓存人脸头像
			bmpFace.DeleteObject();
			GetCBitmap(&bih, pFace, bmpFace);

			delete []pFace;

		}

		if (drawFaceBox) {
			DrawFaceRects(pBuf,width,height,&facePos[0].rcFace,1,RGB(0,255,0),4);
		}
	
		return 1;
	}

	delete [](BYTE*)facePos[0].dwReserved;

	return 0;
}


	// 弹出消息框
void CfacerecoDlg::Message(char* msg) {
	::MessageBox(this->m_hWnd, msg, "提示", MB_ICONWARNING);
}

void CfacerecoDlg::Clear() {
	maxScore = 0;

	if (initType != 2 && initType != 4 && initType != 5) {
		picIDCard.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD)));

		lblCardName.SetWindowTextA("姓名");
		lblCardCode.SetWindowTextA("证件号");
		lblCardAddress.SetWindowTextA("地址");
		//lblCardMatch.SetWindowTextA("相似度");
	}

	Invalidate(true);
	UpdateWindow();
}

void CfacerecoDlg::SendSocket(char* msg) {
	if (initType != 1) return; 

	sendto(senderSocket, msg, lstrlen(msg), 0, (SOCKADDR *) &saUdpServ, sizeof (SOCKADDR_IN));
}


// 发送消息
void CfacerecoDlg::SendProtect(char* msg)  {
	sendto(senderProtect, msg, lstrlen(msg), 0, (SOCKADDR *) &saUdpServProtect, sizeof (SOCKADDR_IN));
}

void CfacerecoDlg::HideMain() {
	pauzeReading = TRUE;
	ShowWindow(SW_HIDE);
	if (initType == 1) {
		Clear();
	} else if (initType == 3) {
		picIDCard.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD)));
		lblCardName.SetWindowTextA("姓名");
		lblCardCode.SetWindowTextA("证件号");
		lblCardAddress.SetWindowTextA("地址");
		maxScore = 0;
	} else if (initType == 2 || initType == 4) {
		picIDByHand.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD)));
		edtYear.SetWindowTextA("");
		edtMonth.SetWindowTextA("");
		edtDay.SetWindowTextA("");
		edtName.SetWindowTextA("");
		edtIDCode.SetWindowTextA("");
		edtAddress.SetWindowTextA("");
	} else if (initType == 5) {
		spiritDlg->setState(1);
	}
}


void CfacerecoDlg::Exit() {

	doDetecting = FALSE;
	Sleep(200);

	videoDetect = FALSE;
	Sleep(200);

	doListening = FALSE;
	Sleep(200);

	doReceiving = FALSE;
	Sleep(200);

	doReading = FALSE;
	Sleep(200);

	UnhookWindowsHookEx(m_hHook);
	m_hHook = NULL;

	bmpFace.DeleteObject();
	bmpStoreFace.DeleteObject();
	bmpStoreVideo.DeleteObject();
	bmpStoreFaceByHand.DeleteObject();

	remove(fullFile);
	remove(avatarFile);
	remove(idFile);
	remove(handFaceFile);
	remove(storagePath + "\\1.jpg");
	remove(storagePath + "\\2.jpg");
	remove(storagePath + "\\photo.bmp");

	
	if(pCapture)
	{
		pCapture->Stop();
		delete pCapture;
		pCapture=NULL;
	}

	closesocket(receiverProtect);
	closesocket(senderProtect);

	//if (initType == 1 && initFilter) {
	//	rfdFailed();
	//	//rfdRelease();

	//	//unloadDriver("ReadFilter");
	//}

	if (initType == 2 || initType == 3 || initType == 4) {
		CloseComm();
	} else if (initType == 1) {
		closesocket(receiverSocket);
		closesocket(senderSocket);
	}
	WSACleanup();

	delete []idFeature;
	idFeature = NULL;

	if (apiDll != NULL) {
		FreeLibrary(apiDll);
	}

	THFI_Release();
	EF_Release();
	
	DeleteCriticalSection(&g_CriticalSection);
	Shell_NotifyIcon(NIM_DELETE,&notifyIcon);
	OnCancel();
}


void CfacerecoDlg::Active() {
	if (initType == 5)  return;

	ShowWindow(SW_SHOW);
	pauzeReading = FALSE;
}

void CfacerecoDlg::StartDetect() {
	if (initType != 5)  return;

	if (!pauzeDetecting || doComparing) return;

	CFileFind find;
	CTime lastTime = NULL;
	CTime tempTime;
	CString fileName;
	CString filePath = "";
	BOOL bf = find.FindFile(detectPath);
	while (bf) {
		bf = find.FindNextFile();
		if(!find.IsDots()) {
			remove(find.GetFilePath());
		}
	}

	pauzeDetecting = FALSE;
	detectingTime = CTime::GetCurrentTime();
	maxScore = 0;

	spiritDlg->setState(2);
}

extern "C" __declspec(dllexport) CfacerecoDlg* ShowDialog() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CfacerecoDlg* pDlg = new CfacerecoDlg;
	pFacerecoDlg = pDlg;

	return pDlg;
}