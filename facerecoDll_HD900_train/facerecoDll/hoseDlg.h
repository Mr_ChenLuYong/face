#pragma once


// choseDlg 对话框

class choseDlg : public CDialog
{
	DECLARE_DYNAMIC(choseDlg)

public:
	choseDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~choseDlg();

// 对话框数据
	enum { IDD = IDD_HOSEDLG };


protected:

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

private:
	char* theTip;

public:
	void setTip(char* tip);
};
