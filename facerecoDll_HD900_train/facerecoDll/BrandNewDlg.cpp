// BrandNewDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "facerecoDll.h"
#include "BrandNewDlg.h"


// BrandNewDlg 对话框

IMPLEMENT_DYNAMIC(BrandNewDlg, CDialog)

BrandNewDlg::BrandNewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(BrandNewDlg::IDD, pParent)
{

}

BrandNewDlg::~BrandNewDlg()
{
}

void BrandNewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(BrandNewDlg, CDialog)
END_MESSAGE_MAP()


// BrandNewDlg 消息处理程序

BOOL BrandNewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetActiveWindow();
	// 窗口置顶
	this->SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
	// 设置窗口大小
	MoveWindow(100,100,942,577);

	SetWindowText("人像识别程序");

	return true;
}


void BrandNewDlg::OnPaint()
{
	if (IsIconic())
	{
	}
	else
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		CDialog::OnPaint();
	}
}

BOOL BrandNewDlg::PreTranslateMessage(MSG* pMsg) {
	if(pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
			case VK_RETURN:    // 屏蔽回车
				return true;
			case VK_ESCAPE:
				return true;
		}
	}
	else if (pMsg->message == WM_COMMAND)
	{
		return true;
	} else if (pMsg->message == WM_SYSCOMMAND) {
		::ShowWindow(this->m_hWnd, SW_SHOW);
	}
   
    return CDialog::PreTranslateMessage(pMsg);
}

LRESULT BrandNewDlg::OnNotifyIcon(WPARAM wParam,LPARAM IParam) {
	if (wParam == IDI_ICON1) {
		UINT uMsg = (UINT)IParam;


		switch(uMsg) {
			case WM_RBUTTONUP:
			{
				CPoint point;
				int id;
				GetCursorPos(&point);

				id = menuTray.GetSubMenu(0)->TrackPopupMenu(TPM_RETURNCMD | TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this);
				if (id == ID_UPDATE) {
				
				} else if (id == ID_RESET) {
				
				} else if (id == ID_EXIT) {
				
				} else if (id == ID_FULLSCREEN) {
				
				} else if (id == ID_Settle) {
		
				}
				break;
			}
			case WM_LBUTTONDBLCLK:
				break;
			default:
				break;
		}
	}
	return 1;
}

HCURSOR BrandNewDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

LRESULT BrandNewDlg::OnNcHitTest(CPoint point) {
	return HTCAPTION;
}

extern "C" __declspec(dllexport) BrandNewDlg* ShowBrandNew() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	BrandNewDlg* pDlg = new BrandNewDlg();
	pBrandNewDlg = pDlg;

	return pDlg;
}