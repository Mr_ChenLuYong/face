#pragma once

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
using namespace cv;

#include <atlimage.h>

#include "facerecoDlg.h"
#include "BrandNewDlg.h"
#include "ResultDlg.h"
#include "SpiritDlg.h"
#include "StateDlg.h"
#include "IdlessDlg.h"



#include "ijl15\\IjlLib.h"
#include "../include/ThFacerCap.h"

#include "../include/AiFace.h"
#pragma comment (lib, "../include/AiFace.lib")

//#include "../include/THFaceImage_i.h"
//#pragma comment (lib, "../include/THFaceImage.lib")
//
//#include "../include/THFeature_i.h"
//#pragma comment(lib,"../include/THFeature.lib")

//#include "../include/sdtapi.h"
//#pragma comment (lib, "../include/Sdtapi.lib")

#define WIDTHBYTES(bits)    (((bits) + 31) / 32 * 4)

extern BrandNewDlg* pBrandNewDlg;
extern CfacerecoDlg* pFacerecoDlg;
extern ResultDlg resultDlg;
extern SpiritDlg* spiritDlg;
extern StateDlg* stateDlg;
extern IdlessDlg* idlessDlg;

extern CString appPath;
extern CString storagePath;

extern long preLastTime;
extern CString preDetectingPath;

extern CString readFilterPath;

extern CString exeWltToBmp;

extern CString logFile;

extern CString settingFile;

extern CString hosterInfoFile;

extern int tokenExpireTime;

extern int readSleep;

extern CString placeCode;


// 摄像头
extern	MCapture* pCapture;
// 摄像头基本属性
extern	SCapParam pCapParam;

// 绘制 bmp 画面
extern void DrawBmpBuf(BITMAPINFOHEADER& bih,BYTE* pDataBuf,HWND hShowWnd,bool bFitWnd,bool drawLine);
// 封装 BITMAPINFOHEADER
extern void ContructBih(int nWidth,int nHeight,BITMAPINFOHEADER& bih);
// 获取部分图片
extern void GetPartImage( BYTE* image, int imageWidth, int imageHeight, BYTE* part,  int left,int top, int right ,int bottom );
// 缩放图片
extern bool ResampleBmp(BITMAPINFOHEADER& bih, BYTE* inBuf,int newx,int newy,BYTE* outBuf);
// 绘制人像框
extern void DrawFaceRects(BYTE* pRgbBuf,int nBufWidth,int nBufHeight,RECT* pFaceRects,int nRectNum,COLORREF color,int nPenWidth);
// 图片镜像
extern bool MirrorDIB(LPSTR lpDIBBits, LONG lWidth, LONG lHeight, bool bDirection,int nImageBits);
// 缓存数据转化成 CBitmap
extern bool GetCBitmap(BITMAPINFOHEADER* pBih, BYTE* pDataBuf,CBitmap &bmp);
// 拆分字符串
extern void SplitCString(CString str, CStringArray& Arr, char ch);
// 保存缓存成bmp文件
extern bool SaveBufToBmpFile( BYTE* buf, LPBITMAPINFOHEADER lpBih, LPCSTR strFileName );
// 保存CBitmap成bmp文件
extern void SaveCBitmapToBmpFile(CBitmap* bmp, LPCSTR strFileName);
// 复制CBitmap位图
extern HBITMAP CopyBitmap(HBITMAP hSourceHbitmap);
// byte 转 16进制字符串
extern CString BYTEToHex(const BYTE* ary, int len);
// 16进制字符串转byte
extern void HexToBYTE(const char* str, BYTE* ary);
// 获取文件md5
extern CString Md5file(const char* filename);
// 多字节字符转utf8
extern bool MultiToUTF8(const std::string& multiText, std::string& utf8Text);
//utf8转多字节
extern bool UTF8ToMB(const char* pu8, char* data, int utf8Len);

// 加载图片读取数据
extern void LoadBmpFromFile(LPCTSTR strFileName, CBitmap& bmp);
// 下载文件
extern int DownloadFile(const CString strUrl,const CString strSavePath);
// 移除空格
extern void TrimAry(char* ary, int len);
// 创建发送数据Socket
extern bool BuildSenderSocket(SOCKET& sdSocket, int sdPort, SOCKADDR_IN& sockaddr_in);
// 创建接收数据Socket
extern bool BuildReceiverSocket(SOCKET& rcSocket, int rcPort, int timeout);
// 判断文件是否存在
extern bool FileExists(char* fileName);
// 多字节转 unicode
extern void MultiToUnicode(char* data, WCHAR* wdata);
// unicode 转多字节
extern void UnicodeToMulti(WCHAR* wdata, int length, char* data);

extern void UTF8ToWidchar();

// write log
extern void WriteLog(char* context);
// write log
extern void WriteLog(char* context, bool eraseTime);

extern CString UTF8Convert(CString &str, int sourceCodepage, int targetCodepage);




// API MD5 define
typedef void (*APIMD5)(unsigned char*,int, char*);
// API Threshold define
typedef void (*APIThreshold)(char*,char*,char*,char*);
// API CheckVersion defin
typedef int (*APICheckVersion)(char*, char*, char*, char*, char*, char*);
// API HotelHand define
typedef bool (*APIHotelHand)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*);
// API NetBarHand define
typedef bool (*APINetBarHand)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*);
// API UpdateInfo define
typedef void (*APIUploadInfo)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, int);
// API gasDoSimilarity define
typedef void (*APIGasDoSimilarity)(const char*, const char*, const char*, const char*, const char*, const char*, const char*, const char*, char*);
// API getSimilarityVal define
typedef void (*APIGetSimilarityVal)(const char*, const char*, const char*, const char*, char*);
// API doResultSure define
typedef void (*APIDoResultSure)(const char*, const char*, const char*, const char*, const char*, char*);
// API active upload
typedef bool (*APIActiveHeartbeat)(const char*, const char*, const char *, char *);
// API active heartbeat
typedef void (*APIActiveUpload)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, bool);
// API active check version
typedef int (*APIActiveCheckVersion)(const char*, const char*, const char *, const char *, char *);
// API active download file
typedef bool (*APIActiveDownloadFile)(const char*, const char*, const char *, char *);
// API active threshold
typedef void (*APIActiveThreshold)(const char*,const char*, const char*,char*,char*);
// API active source url
typedef int (*APIActiveSource)(const char* , const char* , const char *, char *, char*);
// API active function url
typedef bool (*APIActiveFunction)(const char*, const char*, const char *, char *, char*);



// API third hoster info
typedef bool (*APIThirdHosterInfo)(const char *, const char *);
// API third upload
typedef void (*APIThirdUpload)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char *);




typedef void (*APIActiveUploadIdless)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*,char*, char*,char*,bool);
typedef bool (*APIActiveQueryIdless)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*);


// API DLL
extern HINSTANCE apiDll;
// API MD5 function
extern APIMD5 apiMd5;
// API Threshold function
extern APIThreshold apiThreshold;
// API UploadInfo function
extern APIUploadInfo apiUploadInfo;
// API HotelHand function
extern APIHotelHand apiHotelHand;
// API NetBarHand function
extern APINetBarHand apiNetBarHand;
// API CheckVersion function
extern APICheckVersion apiCheckVersion;
// API GasDoSimilarity function
extern APIGasDoSimilarity apiGasDoSimilarity;
// API GetSimilarity function
extern APIGetSimilarityVal apiGetSimilarityVal;
// API DoResultSure function
extern APIDoResultSure apiDoResultSure;
// API ActiveUpload function
extern APIActiveUpload apiActiveUpload;
// API ActiveHeartbeat function
extern APIActiveHeartbeat apiActiveHeartbeat;
// API ActiveCheckVersion function
extern APIActiveCheckVersion apiActiveCheckVersion;
// API ActiveDownloadFile function
extern APIActiveDownloadFile apiActiveDownloadFile;
// API ActiveThreshold function
extern APIActiveThreshold apiActiveThreshold;
// API ActiveSource function
extern APIActiveSource apiActiveSource;
// API ActiveFunction function
extern APIActiveFunction apiActiveFunction;

// API ThirdHosterInfo function
extern APIThirdHosterInfo apiThirdHosterInfo;
// API ThirdUpload function
extern APIThirdUpload apiThirdUpload;



// API activeUploadIdless
extern APIActiveUploadIdless apiActiveUploadIdless;
// API activeQueryIdless
extern APIActiveQueryIdless apiActiveQueryIdless;


typedef bool (*APIRfdReadInfo)(CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , ULONG );


typedef void (*APIAboutUs)(char*,char*,char*,bool);


typedef int (*APIVisitor)(char*, char*, char*);

extern HINSTANCE aboutDll;
extern APIAboutUs apiAboutUs;

extern HINSTANCE visitorDll;
extern APIVisitor apiVisitor;

// init
extern void InitPublic();

extern void ReleasePublic();