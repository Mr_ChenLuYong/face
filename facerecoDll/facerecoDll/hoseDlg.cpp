// hoseDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "facerecoDll.h"
#include "hoseDlg.h"


// choseDlg 对话框

IMPLEMENT_DYNAMIC(choseDlg, CDialog)

choseDlg::choseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(choseDlg::IDD, pParent)
{

}

choseDlg::~choseDlg()
{
}

BOOL choseDlg::OnInitDialog() {
	SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

	GetDlgItem(IDC_TIP)->SetWindowText(theTip);
	return TRUE;
}

void choseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(choseDlg, CDialog)
	ON_BN_CLICKED(IDOK, &choseDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &choseDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// choseDlg 消息处理程序

void choseDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	OnOK();
}

void choseDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	OnCancel();
}

void choseDlg::setTip(char* tip) {
	theTip = tip;
}
