#pragma once

#ifdef DLLEXPORT

#define DLLOPTION _declspec(dllexport)    //表明标有此宏定义的函数和类是dll文件的导出函数和类，是dll文件的对外接口

#else

#define DLLOPTION _declspec(dllimport)       //表明标有此宏定义的函数和类的定义在dll文件中

#endif

#include <atltime.h>

#include "MD5.h"
#include "RyeolHttpClient.h"
using namespace Ryeol;


#include "./include-json/json.h"
#pragma comment(lib,"./include-json/json_vc71_libmt.lib")

// API URL
CString baseAPI = "http://facereco.eshanren.com/api/";
//CString baseAPI = "http://lwrxgl.longwan.gov.cn/api/";
//CString baseAPI = "http://127.0.0.1:8080/api/";

CString version = "v1_0_0";
// 获取阈值
CString getLimitAPI = baseAPI + version + "/getIdentifyLimit";
// 网吧上传
CString barUploadAPI = baseAPI + version + "/barUpload";
// 网吧人工录入
CString barManualConfirmAPI = baseAPI + version + "/barManualConfirm";
// 宾馆上传
CString hotelUploadAPI = baseAPI + version + "/hotelUpload";
// 宾馆人工录入
CString hotelManualConfirmAPI = baseAPI + version + "/hotelManualConfirm";
// 加油站上传
CString gasStationUploadAPI = baseAPI + version + "/gasStationUpload";
// 学校上传
CString schoolUploadAPI = baseAPI + version + "/schoolUpload";
// 加油站人工录入
CString gasStationManualConfirmAPI = baseAPI + version + "/gasStationManualConfirm";
// 加油站比对
CString gasDoSimilarityAPI = baseAPI + version + "/gasDoSimilarity";
// 获得比对值
CString getSimilarityValAPI = baseAPI + version + "/getSimilarityVal";
// 提交结果
CString doResultSureAPI = baseAPI + version + "/doResultSure";
// 负责人注册
CString phoneRegisterAPI = baseAPI + version + "/phoneRegister";
// 学校信息
CString schoolInfoAPI = baseAPI + version + "/getSchoolInfo";
// 上访理由
CString visitReasonAPI = baseAPI + version + "/getSchoolReason";
// 常用理由
CString usualReasonAPI = baseAPI + version + "/getUsualReason";
// 上访
CString schoolVisitAPI = baseAPI + version + "/schoolVisit";
// 离开
CString schoolLeaveAPI = baseAPI + version + "/schoolLeave";
// 全部离开
CString schoolLeaveAllAPI = baseAPI + version + "/leaveAll";
// 心跳
CString heartbeatAPI = baseAPI + version + "/heartbeat";
// 检查更新
CString checkVersionAPI = baseAPI + "version/checkVersion";

// 访问记录
CString visitViewAPI = baseAPI + version + "/visitView";

// active upload
CString activeUploadAPI = baseAPI + "upload";

// active heartbeat
CString activeHeartbeatAPI = baseAPI + "active";

// active function
CString activeFunctionAPI = baseAPI + "active";

// active threshold
CString activeThresholdAPI = baseAPI + "threshold";

// active active cleanup cache
CString activeCleanupAPI = baseAPI + "active";

// active checkVersion
CString activeCheckVersionAPI = baseAPI + "newupdate";

// active downloadFile
CString activeDownloadFileAPI = baseAPI + "active/downloadFile";

CString logoRoot = "http://rxglapi.jhengo.com/frontLogo/";
CString schoolLogoAPI = logoRoot +"front_school_logo.jpg";

bool MultiToUTF8(const std::string& multiText, std::string& utf8Text);

extern "C" {
	DLLOPTION void md5(unsigned char* buffer,int bufferlen, char* result);
	DLLOPTION void heartbeat(char* appkey, char* appsecret, char* placeId);
	DLLOPTION BOOL phoneRegister(char* appkey, char* appsecret, char* phoneNumber, char* result);
	DLLOPTION int checkVersion(char* appkey, char* appsecret, char* version, char* place_id, char* newVersion, char* urls);
	DLLOPTION void threshold(char* appkey, char* appsecret, char* threshold, char* thresholdHigh);
	DLLOPTION BOOL netBarHand(char* appkey, char* appsecret, char* video_img, char* name, char* id_card_no, char* video_img_md5, char* sex, char* result, char* internet_bar_id, char* birth_date, char* location);
	DLLOPTION BOOL hotelHand(char* appkey, char* appsecret, char* video_img, char* name, char* id_card_no, char* video_img_md5, char* sex, char* result, char* internet_bar_id, char* birth_date, char* location);
	DLLOPTION void uploadInfo(char* appkey, char* appsecret, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* internet_bar_id, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, int initType);
	DLLOPTION long uploadInfoForId(char* appkey, char* appsecret, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* internet_bar_id, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, int initType);
	DLLOPTION void gasDoSimilarity(const char* appkey, const char* appsecret, const char* id_card_img, const char* id_card_img_md5, const char* id_card_info, const char* place_id, const char* full_img, const char* full_img_md5, char* retVal);
	DLLOPTION void getSimilarityVal(const char* appkey, const char* appsecret, const char* compare_code, const char* place_id, char* retVal);
	DLLOPTION void doResultSure(const char* appkey, const char* appsecret, const char* compare_code, const char* place_id, const char* result, char* retVal);


	DLLOPTION void getSchoolInfo(const char* appkey, const char* appsecret,const char* school_id, char* schoolInfo);
	DLLOPTION bool saveSchoolInfo(const char* appkey, const char* appsecret,const char* school_id, const char* fileName);
	DLLOPTION void getVisitReason(const char* appkey, const char* appsecret,const char* school_id, char* reasons);
	DLLOPTION void getUsualReason(const char* appkey, const char* appsecret,const char* school_id, char* reasons);
	DLLOPTION void schoolLeave(const char* appkey, const char* appsecret,const char* school_id,const char* idcode);
	DLLOPTION void schoolLeaveAll(const char* appkey, const char* appsecret,const char* school_id);

	DLLOPTION void schoolVisit(const char* appkey, const char* appsecret,const char* school_id,const char* compare_id, const char* phone_num,const char* reason,const char* hoster);

	DLLOPTION void schoolVisitView(const char* appkey, const char* appsecret,const char* school_id, char* url);

	DLLOPTION void activeUpload(char* appkey, char* appsecret, char* url, char* identity, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, char* id_data, char* thresholdLower, char* thresholdHigher, char* placeCode, bool timeout);

	DLLOPTION int activeSource(const char* appkey, const char* appsecret, const char *token, char *url, char* check);

	DLLOPTION bool activeHeartbeat(const char* appkey, const char* appsecret, const char *token, char *result);

	DLLOPTION void activeThreshold(const char* appkey, const char* appsecret, const char* token, char* threshold, char* thresholdHigh);

	DLLOPTION int activeCheckVersion(const char* appkey, const char* appsecret, const char *version, const char *identity, char *newVersion);

	DLLOPTION bool activeCheckFunction(const char* appkey, const char* appsecret, const char *token, char *idless, char* check);

	DLLOPTION bool activeDownloadFile(const char* appkey, const char* appsecret, const char *version, char *urls);

	DLLOPTION bool thirdHosterInfo(const char *url, const char *filename);

	DLLOPTION bool thirdUpload(char* appkey, char* appsecret, char* url, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, char* placeCode, char* hoster_id, char* phone_num, char *reason);

	DLLOPTION void saveLogo(const char* initType, const char* fileName);

	DLLOPTION void postDQThirdPart(const char* url, const char* internet_bar_id, const char* cardId, const char* cardIdShort, const char* personName, const char* address, const char* birthday, const char* nation, const char* sex, const char* authority, const char* deadline);

	DLLOPTION void activeUploadIdless(char* appkey, char* appsecret, char* url, char* identity, char* idfile, char* facefile, char* fullfile, char* result, char* similarity, char* vedio_img_feture, char* idcode,char* thresholdLower, char* thresholdHigher,char* placeCode, bool timeout);

	DLLOPTION bool activeQueryIdless(char* appkey, char* appsecret, char* url, char* identity, char* name, char* gender, char* nation, char* idcode, char* address, char* startDate, char* endData, char* bornday, char* orgnization, char* idData, char* faceFeature, char* faceFile);

	DLLOPTION void utf8(const char* input, char* output);


	// 动车站版接口
	DLLOPTION bool checkNetwork(const char* const url);


	DLLOPTION bool verifyTrainInfo(const char* appkey, char* appsecret, const char* url, const char* placeCode, const char* verifyCode);

	DLLOPTION void setBaseApiUrl(const char* url);

}