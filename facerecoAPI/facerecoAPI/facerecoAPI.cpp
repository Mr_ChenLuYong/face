#define DLLEXPORT                    //定义了预处理器变量 DLLEXPORT
#include "facerecoAPI.h"
#include <fstream>


#include "SRNetwork.h"


// 动车站版所需要的校验参数
CString g_verifyCode;
CString g_placeCode;

bool g_networkStatus = true;
// 动车站



//void ReadConfig() {
//	char pModuleName[256];
//	::GetModuleFileName(NULL,pModuleName,256);
//	CString strTemp;
//	strTemp=pModuleName;
//	int pos=strTemp.ReverseFind('\\');
//	CString appPath=strTemp.Left(pos);
//	CString config = appPath + "\\config.ini";
//
//	version = "v1_0_0";
//	GetPrivateProfileString("config","uploadAPI","http://rxglapi.jhengo.com/api/", baseAPI.GetBuffer(64), 64,config);
//	getLimitAPI.Format("%s%s/getIdentifyLimit",baseAPI, version);
//	barUploadAPI.Format("%s%s/barUpload",baseAPI, version);
//	barManualConfirmAPI.Format("%s%s/barManualConfirm",baseAPI, version);
//	hotelUploadAPI.Format("%s%s/hotelUpload",baseAPI, version);
//	hotelManualConfirmAPI.Format("%s%s/hotelManualConfirm",baseAPI, version);
//	gasStationUploadAPI.Format("%s%s/gasStationUpload",baseAPI, version);
//	schoolUploadAPI.Format("%s%s/schoolUpload",baseAPI, version);
//	gasStationManualConfirmAPI.Format("%s%s/gasStationManualConfirm",baseAPI, version);
//	gasDoSimilarityAPI.Format("%s%s/gasDoSimilarity",baseAPI, version);
//	getSimilarityValAPI.Format("%s%s/getSimilarityVal",baseAPI, version);
//	doResultSureAPI.Format("%s%s/doResultSure",baseAPI, version);
//	phoneRegisterAPI.Format("%s%s/phoneRegister",baseAPI, version);
//	schoolInfoAPI.Format("%s%s/getSchoolInfo",baseAPI, version);
//	visitReasonAPI.Format("%s%s/getSchoolReason",baseAPI, version);
//	usualReasonAPI.Format("%s%s/getUsualReason",baseAPI, version);
//	schoolVisitAPI.Format("%s%s/schoolVisit",baseAPI, version);
//	schoolLeaveAPI.Format("%s%s/schoolLeave",baseAPI, version);
//	schoolLeaveAllAPI.Format("%s%s/leaveAll",baseAPI, version);
//	heartbeatAPI.Format("%s%s/heartbeat",baseAPI, version);
//	checkVersionAPI.Format("%s%s/checkVersion",baseAPI, version);
//	visitViewAPI.Format("%s%s/visitView",baseAPI, version);
//}

bool MultiToUTF8(const std::string& multiText, std::string& utf8Text)
{
	// 把输入转换为Unicode
	int size = ::MultiByteToWideChar(CP_ACP,
                         0,
                         multiText.c_str(),
                        -1,
                        NULL,
                        0);
        if (0 == size)
        {
            return false;
        }

        wchar_t* wszBuffer = new wchar_t[size + 1];
        ::ZeroMemory(wszBuffer, (size + 1) * sizeof(wchar_t));

        if (0 == ::MultiByteToWideChar(CP_ACP,
                        0,
                        multiText.c_str(),
                        -1,
                        wszBuffer,
                        size + 1))
        {
            delete[] wszBuffer;
            return false;
        }
        
        // Unicode->UTF8的转换
        size = ::WideCharToMultiByte(CP_UTF8,
                         0,
                        wszBuffer,
                        -1,
                        NULL,
                        0,
                        NULL,
                        NULL);
        if (0 == size)
        {
            delete[] wszBuffer;
            return false;
        }

        char* szBuffer = new char[size + 1];
        ::ZeroMemory(szBuffer, (size + 1) * sizeof(char));

        if (0 == ::WideCharToMultiByte(CP_UTF8,
                         0,
                        wszBuffer,
                        -1,
                        szBuffer,
                        size + 1,
                        NULL,
                        NULL))
        {
            delete[] wszBuffer;
            delete[] szBuffer;
            return false;
        }

        utf8Text = szBuffer;
        delete[] wszBuffer;
        delete[] szBuffer;
        return true;
}

void md5(unsigned char* buffer,int bufferlen, char* result) {
	MD5 md5;
	md5.GenerateMD5(buffer, bufferlen);
	md5.ToString(result);
}

void heartbeat(char* appkey, char* appsecret, char* placeId) {
	//ReadConfig();

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&place_id=%s&timestamp=%s%s",appkey,placeId,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("place_id"), _T(placeId));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(heartbeatAPI);
	} catch(...) {
	}
}

BOOL phoneRegister(char* appkey, char* appsecret, char* phoneNumber, char* result) {
	//ReadConfig();

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&phone_number=%s&timestamp=%s%s",appkey,phoneNumber,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("phone_number"), _T(phoneNumber));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(phoneRegisterAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		CString strUrls;
		strUrls = "";
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["success"].asBool() == true) {
				string str = root["message"].asString() + ";" + root["placeId"].asString();
				strcpy(result, str.c_str());
				return 1;
			}
		}
	} catch(...) {
		return FALSE;
	}

	return FALSE;
}

int checkVersion(char* appkey, char* appsecret, char* version, char* place_id, char* newVersion, char* urls) {
	//ReadConfig();
	if (!g_networkStatus) {
		return 1;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&client_version=%s&place_id=%s&timestamp=%s%s",appkey,version,place_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("client_version"), _T(version));
	objHttpReq.AddParam(_T("place_id"), _T(place_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(checkVersionAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		CString strUrls;
		strUrls = "";
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["success"].asBool() == true) {
				CString tmpStr;

				Json::Value data;
				data = root["data"];
				Json::Value files;
				files = data["client_download"];

				for (unsigned int i = 0 ; i < files.size(); i++) {
					strUrls += files[i].asString().c_str();
					strUrls += ";";
				}

				strcpy(urls, strUrls.GetBuffer());
				strcpy(newVersion, data["new_client_version"].asString().c_str());

				return 1;
			} else {
				return 0;
			}
		}
	} catch(...) {
		return -1;
	}

	return -1;
}

void threshold(char* appkey, char* appsecret, char* threshold, char* thresholdHigh) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}


	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&timestamp=%s%s", appkey, timestamp, appsecret);
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));


	try {
		objHttpRes = objHttpReq.RequestGet(getLimitAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["success"].asBool() == true) {
				Json::Value data;
				data = root["data"];
				strcpy(threshold, data["identify_lower_limit"].asString().c_str());
				strcpy(thresholdHigh, data["identify_upper_limit"].asString().c_str());
			}
		}
	} catch(...) {
	
	}

}

BOOL hotelHand(char* appkey, char* appsecret, char* video_img, char* name, char* id_card_no, char* video_img_md5, char* sex, char* result, char* internet_bar_id, char* birth_date, char* location) {
	/*
	appkey
	birth_date
	video_img
	id_card_no
	internet_bar_id
	location
	name
	result
	sex
	timestamp
	video_img_md5
	*/
	//ReadConfig();

	if (!g_networkStatus) {
		return 1;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CString signature;
	signature.Format("appkey=%s&birth_date=%s&id_card_no=%s&internet_bar_id=%s&location=%s&name=%s&result=%s&sex=%s&timestamp=%s&video_img_md5=%s%s",appkey,birth_date,id_card_no,internet_bar_id,location,name,result,sex,timestamp,video_img_md5,appsecret);

	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("birth_date"), _T(birth_date));
	objHttpReq.AddParam(_T("video_img"), _T(video_img), CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("id_card_no"), _T(id_card_no));
	objHttpReq.AddParam(_T("internet_bar_id"), _T(internet_bar_id));
	objHttpReq.AddParam(_T("location"), _T(location));
	objHttpReq.AddParam(_T("name"), _T(name));
	objHttpReq.AddParam(_T("result"), _T(result));
	objHttpReq.AddParam(_T("sex"), _T(sex));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("video_img_md5"), _T(video_img_md5));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(hotelManualConfirmAPI);

		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] ;
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			return root["success"].asBool();
		}
	} catch(...) {
		return FALSE;
	}
	return FALSE;
}

BOOL netBarHand(char* appkey, char* appsecret, char* video_img, char* name, char* id_card_no, char* video_img_md5, char* sex, char* result, char* internet_bar_id, char* birth_date, char* location){
	//ReadConfig();

	if (!g_networkStatus) {
		return 1;
	}


	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CString signature;
	CString vimg(video_img);
	if (vimg.GetLength() > 0) 
		signature.Format("appkey=%s&birth_date=%s&id_card_no=%s&internet_bar_id=%s&location=%s&name=%s&result=%s&sex=%s&timestamp=%s&video_img_md5=%s%s",appkey,birth_date,id_card_no,internet_bar_id,location,name,result,sex,timestamp,video_img_md5,appsecret);
	else 
		signature.Format("appkey=%s&birth_date=%s&id_card_no=%s&internet_bar_id=%s&location=%s&name=%s&result=%s&sex=%s&timestamp=%s%s",appkey,birth_date,id_card_no,internet_bar_id,location,name,result,sex,timestamp,appsecret);


	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("birth_date"), _T(birth_date));
	
	if (vimg.GetLength() > 0) 
	objHttpReq.AddParam(_T("video_img"), _T(video_img), CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("id_card_no"), _T(id_card_no));
	objHttpReq.AddParam(_T("internet_bar_id"), _T(internet_bar_id));
	objHttpReq.AddParam(_T("location"), _T(location));
	objHttpReq.AddParam(_T("name"), _T(name));
	objHttpReq.AddParam(_T("result"), _T(result));
	objHttpReq.AddParam(_T("sex"), _T(sex));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	
	if (vimg.GetLength() > 0) 
		objHttpReq.AddParam(_T("video_img_md5"), _T(video_img_md5));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(barManualConfirmAPI);

		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] ;
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			return root["success"].asBool();
		}
	} catch(...) {
		return FALSE;
	}

	return FALSE;
}



void uploadInfo(char* appkey, char* appsecret, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* internet_bar_id, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, int initType) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	uploadInfoForId(appkey, appsecret, idfile, facefile, fullfile, full_img_md5, id_card_img_md5,  id_card_info,  idcard_img_feture,  internet_bar_id,  result,  similarity,  vedio_img_feture, vedio_img_md5, initType );
}

long uploadInfoForId(char* appkey, char* appsecret, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* internet_bar_id, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, int initType) {
	//ReadConfig();

	if (!g_networkStatus) {
		return 1;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");

	CString signature;
	signature.Format("appkey=%s&full_img_md5=%s&id_card_img_md5=%s&id_card_info=%s&idcard_img_feture=%s&internet_bar_id=%s&result=%s&similarity=%s&timestamp=%s&video_img_feture=%s&video_img_md5=%s%s", appkey, full_img_md5,id_card_img_md5,id_card_info,idcard_img_feture,internet_bar_id,result,similarity,timestamp,vedio_img_feture, vedio_img_md5, appsecret);

	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("id_card_info"), _T(id_card_info));
	objHttpReq.AddParam(_T("idcard_img_feture"), _T(idcard_img_feture));
	objHttpReq.AddParam(_T("video_img_feture"), _T(vedio_img_feture));
	objHttpReq.AddParam(_T("similarity"), _T(similarity));
	objHttpReq.AddParam(_T("result"), _T(result));
	objHttpReq.AddParam(_T("internet_bar_id"), _T(internet_bar_id));
	objHttpReq.AddParam(_T("id_card_img_md5"), _T(id_card_img_md5));
	objHttpReq.AddParam(_T("video_img_md5"), _T(vedio_img_md5));
	objHttpReq.AddParam(_T("full_img_md5"), _T(full_img_md5));

	if (idfile != NULL)
		objHttpReq.AddParam(_T("id_card_img"), idfile, CHttpClient::ParamFile);
	if (facefile != NULL)
	objHttpReq.AddParam(_T("video_img"), facefile, CHttpClient::ParamFile);
	if (fullfile != NULL)
		objHttpReq.AddParam(_T("full_img"), fullfile, CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		if (initType == 1 || initType == 4 || initType == 5) {
			objHttpReq.BeginUpload(barUploadAPI);

			const DWORD cbProceed = 1024;
			do {

			} while ( !(objHttpRes = objHttpReq.Proceed(cbProceed)));

			const DWORD		cbBuff = 1024 * 10 ;
			BYTE			byBuff[cbBuff] ;
			DWORD			dwRead ;
			size_t			cbSize = 0 ;
			CString		str ;

			while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
				cbSize += dwRead ;
				byBuff[dwRead] = '\0';
				str = reinterpret_cast<LPCSTR> (byBuff) ;
			}
			//MessageBox(NULL, str.GetBuffer(), "tip", 0);

		} else if (initType == 2) {
			objHttpReq.BeginUpload(hotelUploadAPI);
			const DWORD cbProceed = 1024;
			do {

			} while ( !(objHttpRes = objHttpReq.Proceed(cbProceed)));

			const DWORD		cbBuff = 1024 * 10 ;
			BYTE			byBuff[cbBuff] ;
			DWORD			dwRead ;
			size_t			cbSize = 0 ;
			CString		str ;

			while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
				cbSize += dwRead ;
				byBuff[dwRead] = '\0';





				str = reinterpret_cast<LPCSTR> (byBuff) ;
			}
		} else if (initType == 3) {

			objHttpReq.BeginUpload(gasStationUploadAPI);
			const DWORD cbProceed = 1024;
			do {

			} while ( !(objHttpRes = objHttpReq.Proceed(cbProceed)));

			const DWORD		cbBuff = 1024 * 10 ;
			BYTE			byBuff[cbBuff] ;
			DWORD			dwRead ;
			size_t			cbSize = 0 ;
			CString		str ;

			while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
				cbSize += dwRead ;
				byBuff[dwRead] = '\0';
				str = reinterpret_cast<LPCSTR> (byBuff) ;
			}
		} else if (initType == 6 || initType == 7) {
			objHttpRes = objHttpReq.RequestUpload(schoolUploadAPI);
			const DWORD		cbBuff = 1024 * 10 ;
			BYTE			byBuff[cbBuff] ;
			DWORD			dwRead ;
			size_t			cbSize = 0 ;
			CString		str ;

			while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
				cbSize += dwRead ;
				byBuff[dwRead] = '\0';
				str = reinterpret_cast<LPCSTR> (byBuff) ;
			}

			Json::Reader reader;
			Json::Value root;
			if (reader.parse(str.GetBuffer(), root)) {
				if (root["success"].asBool() == true) {
					return root["compareId"].asInt64();
				}
			}
		}

	
	} catch(...) {
	}

	return -1;
}

void gasDoSimilarity(const char* appkey, const char* appsecret, const char* id_card_img, const char* id_card_img_md5, const char* id_card_info, const char* place_id, const char* full_img, const char* full_img_md5, char* retVal){
	/*
	appkey
	full_img
	full_img_md5
	id_card_img
	id_card_img_md5
	id_card_info
	place_id
	timestamp
	*/
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CString signature;
	signature.Format("appkey=%s&full_img_md5=%s&id_card_img_md5=%s&id_card_info=%s&place_id=%s&timestamp=%s%s",appkey,full_img_md5,id_card_img_md5,id_card_info,place_id,timestamp,appsecret);

	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("full_img"), _T(full_img), CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("full_img_md5"), _T(full_img_md5));
	objHttpReq.AddParam(_T("id_card_img"), _T(id_card_img), CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("id_card_img_md5"), _T(id_card_img_md5));
	objHttpReq.AddParam(_T("id_card_info"), _T(id_card_info));
	objHttpReq.AddParam(_T("place_id"), _T(place_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(gasDoSimilarityAPI);
	
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["success"].asString() != "0000") {
				strcpy(retVal, "0001");
			} else {
				strcpy(retVal, root["compare_code"].asString().c_str());
			}
		}
	} catch(...) {
	}
}


void getSimilarityVal(const char* appkey, const char* appsecret, const char* compare_code, const char* place_id, char* retVal) {
	/*
	appkey
	compare_code
	place_id
	timestamp
	*/
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CString signature;
	signature.Format("appkey=%s&compare_code=%s&place_id=%s&timestamp=%s%s", appkey, compare_code, place_id, timestamp, appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("compare_code"), _T(compare_code));
	objHttpReq.AddParam(_T("place_id"), _T(place_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(getSimilarityValAPI);

		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] ;
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["success"].asString() != "0000") {
				strcpy(retVal, root["success"].asString().c_str());
			} else {
				strcpy(retVal, root["similarity_val"].asString().c_str());
			}
		}
	} catch(...) {
	}
}

void doResultSure(const char* appkey, const char* appsecret, const char* compare_code, const char* place_id, const char* result, char* retVal){
	/*
	appkey
	compare_code
	place_id
	result
	timestamp
	*/
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CString signature;
	signature.Format("appkey=%s&compare_code=%s&place_id=%s&result=%s&timestamp=%s%s", appkey, compare_code, place_id, result, timestamp, appsecret);

	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("compare_code"), _T(compare_code));
	objHttpReq.AddParam(_T("place_id"), _T(place_id));
	objHttpReq.AddParam(_T("result"), _T(result));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(doResultSureAPI);

		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] ;
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			strcpy(retVal, root["success"].asString().c_str());
		}
	} catch(...) {
	}
}


void getSchoolInfo(const char* appkey, const char* appsecret,const char* school_id, char* schoolInfo) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&school_id=%s&timestamp=%s%s",appkey,school_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("school_id"), _T(school_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestPost(schoolInfoAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
		}
		strcpy(schoolInfo, str.GetBuffer());
	} catch(...) {
	}	
}


bool saveSchoolInfo(const char* appkey, const char* appsecret,const char* school_id, const char* fileName) {
	//ReadConfig();

	if (!g_networkStatus) {
		return 1;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&school_id=%s&timestamp=%s%s",appkey,school_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("school_id"), _T(school_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestPost(schoolInfoAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
		}

		ofstream depart;
		depart.open(fileName, ios::out);
		depart << str.GetBuffer() << endl;
		depart.flush();
		depart.close();
		return true;
	} catch(...) {
	}	
	return false;
}

void getVisitReason(const char* appkey, const char* appsecret,const char* school_id, char* reasons) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&school_id=%s&timestamp=%s%s",appkey,school_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("school_id"), _T(school_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestPost(visitReasonAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
		}
		strcpy(reasons, str.GetBuffer());
	} catch(...) {
	}	
}


void getUsualReason(const char* appkey, const char* appsecret,const char* school_id, char* reasons) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&school_id=%s&timestamp=%s%s",appkey,school_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("school_id"), _T(school_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestPost(usualReasonAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
		}
		strcpy(reasons, str.GetBuffer());
	} catch(...) {
	}	
}

void schoolLeave(const char* appkey, const char* appsecret,const char* school_id,const char* idcode) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&idcode=%s&school_id=%s&timestamp=%s%s",appkey, idcode,school_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("idcode"), _T(idcode));
	objHttpReq.AddParam(_T("school_id"), _T(school_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestPost(schoolLeaveAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
		}
	} catch(...) {
	}	
}


void schoolLeaveAll(const char* appkey, const char* appsecret,const char* school_id) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&school_id=%s&timestamp=%s%s",appkey,school_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("school_id"), _T(school_id));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestPost(schoolLeaveAllAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
		}
	} catch(...) {
	}	

}

void schoolVisit(const char* appkey, const char* appsecret,const char* school_id,const char* compare_id, const char* phone_num,const char* reason,const char* hoster) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&compare_id=%s&hoster=%s&phone_num=%s&reason=%s&school_id=%s&timestamp=%s%s",appkey,compare_id,hoster,phone_num,reason,school_id,timestamp,appsecret);
	
	string utf8Str;
	utf8Str = signature.GetBuffer();
	//MultiToUTF8(signature.GetBuffer(), utf8Str);


	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("school_id"), _T(school_id));
	objHttpReq.AddParam(_T("compare_id"), _T(compare_id));
	objHttpReq.AddParam(_T("reason"), _T(reason));
	objHttpReq.AddParam(_T("phone_num"), _T(phone_num));
	objHttpReq.AddParam(_T("hoster"), _T(hoster));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestPost(schoolVisitAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
		}
	} catch(...) {
	}	
}

void schoolVisitView(const char* appkey, const char* appsecret,const char* school_id, char* url) {
	//ReadConfig();

	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&school_id=%s&timestamp=%s%s",appkey,school_id,timestamp,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CString visitUrl;
	visitUrl.Format("%s?appkey=%s&school_id=%s&timestamp=%s&signature=%s", visitViewAPI, appkey, school_id, timestamp, sign);

	strcpy(url, visitUrl.GetBuffer());
}

void activeUpload(char* appkey, char* appsecret, char* url, char* identity, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, char* id_data, char* thresholdLower, char* thresholdHigher, char* placeCode, bool timeout) {
	
	if (!g_networkStatus) {
		return ;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CString isTimeout = "1";

	CString signature;
	if (timeout) {
		signature.Format("appkey=%s&full_img_md5=%s&id_card_img_md5=%s&id_card_info=%s&id_data=%s&idcard_img_feture=%s&identity=%s&isTimeoutValue=%s&place_code=%s&result=%s&similarity=%s&thresholdHigher=%s&thresholdLower=%s&timestamp=%s&verifyCode=%s%s", appkey, full_img_md5,id_card_img_md5,id_card_info, id_data,idcard_img_feture, identity,isTimeout.GetBuffer(), placeCode,result,similarity,thresholdHigher,thresholdLower,timestamp, g_verifyCode,appsecret);
	} else {
		signature.Format("appkey=%s&full_img_md5=%s&id_card_img_md5=%s&id_card_info=%s&idcard_img_feture=%s&identity=%s&metadata=%s&place_code=%s&result=%s&similarity=%s&thresholdHigher=%s&thresholdLower=%s&timestamp=%s&verifyCode=%s&video_img_feture=%s&video_img_md5=%s%s", appkey, full_img_md5,id_card_img_md5,id_card_info, idcard_img_feture,identity, id_data, placeCode,result,similarity,thresholdHigher,thresholdLower,timestamp,g_verifyCode, vedio_img_feture, vedio_img_md5, appsecret);
	}

	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("place_code"), _T(placeCode));
	objHttpReq.AddParam(_T("identity"), _T(identity));
	objHttpReq.AddParam(_T("id_card_info"), _T(id_card_info));
	objHttpReq.AddParam(_T("thresholdHigher"), _T(thresholdHigher));
	objHttpReq.AddParam(_T("thresholdLower"), _T(thresholdLower));
	objHttpReq.AddParam(_T("metadata"), _T(id_data));
	objHttpReq.AddParam(_T("idcard_img_feture"), _T(idcard_img_feture));
	objHttpReq.AddParam(_T("similarity"), _T(similarity));
	objHttpReq.AddParam(_T("result"), _T(result));
	objHttpReq.AddParam(_T("id_card_img_md5"), _T(id_card_img_md5));
	objHttpReq.AddParam(_T("full_img_md5"), _T(full_img_md5));
	// 动车站版校验码
	objHttpReq.AddParam(_T("verifyCode"), _T(g_verifyCode));
	// end 动车站

	if (timeout) {
		objHttpReq.AddParam(_T("isTimeoutValue"),isTimeout.GetBuffer());
	} else {
		objHttpReq.AddParam(_T("video_img_md5"), _T(vedio_img_md5));
		objHttpReq.AddParam(_T("video_img_feture"), _T(vedio_img_feture));
		if (facefile != NULL)
			objHttpReq.AddParam(_T("video_img"), facefile, CHttpClient::ParamFile);
	}

	if (idfile != NULL)
		objHttpReq.AddParam(_T("id_card_img"), idfile, CHttpClient::ParamFile);

	if (fullfile != NULL)
		objHttpReq.AddParam(_T("full_img"), fullfile, CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("signature"), _T(sign));

	CString temp_barUploadAPI;
	temp_barUploadAPI.Format("%s/post/%s",url,g_placeCode);
	//MessageBox(NULL, "start", temp_barUploadAPI, 0);
	try {
		//MessageBox(NULL, "start", "", 0);

		objHttpReq.BeginUpload(temp_barUploadAPI);
		const DWORD cbProceed = 1024;
		do {

		} while ( !(objHttpRes = objHttpReq.Proceed(cbProceed)));

		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] ;
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}
		//MessageBox(NULL, str.GetBuffer(), "", 0);
	} catch(...) {
	}
}

int activeSource(const char* appkey, const char* appsecret, const char *token, char *url, char* check) {
	
	if (!g_networkStatus) {
		return 1;
	}
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&timestamp=%s&token=%s%s",appkey, timestamp, token,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.SetHeader("X-Requested-Method", "source");
	objHttpReq.SetHeader("X-Requested-Param-appkey", _T(appkey));
	objHttpReq.SetHeader("X-Requested-Param-token", _T(token));
	objHttpReq.SetHeader("X-Requested-Param-timestamp", _T(timestamp));
	objHttpReq.SetHeader("X-Requested-Param-signature", _T(sign));
	//objHttpReq.AddParam(_T("appkey"), _T(appkey));
	//objHttpReq.AddParam(_T("token"), _T(token));
	//objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	//objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestGet(activeHeartbeatAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		CString strTmp;
		str = "";
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			strTmp = reinterpret_cast<LPCSTR> (byBuff) ;
			str.Format("%s%s", str, strTmp);
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["statusCode"].asInt() == 200) {

				Json::Value obj;
				obj = root["object"];
				strcpy(url, obj["url"].asString().c_str());
				strcpy(check, obj["check"].asString().c_str());
				return obj["type"].asInt();
			}
		}
	} catch(...) {
		return 0;
	}

	return 0;
}

bool activeHeartbeat(const char* appkey, const char* appsecret, const char *token, char *result) {

	if (!g_networkStatus) {
		return true;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&timestamp=%s&token=%s&verifyCode=%s%s",appkey, timestamp, token, g_verifyCode,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.SetHeader("X-Requested-Method", "heartbeat");
	objHttpReq.SetHeader("X-Requested-Param-appkey", _T(appkey));
	objHttpReq.SetHeader("X-Requested-Param-token", _T(token));
	objHttpReq.SetHeader("X-Requested-Param-timestamp", _T(timestamp));
	objHttpReq.SetHeader("X-Requested-Param-signature", _T(sign));
	// 校验码
	CString temp_activeHeartbeatAPI;
	temp_activeHeartbeatAPI.Format("%s?appkey=%s&timestamp=%s&token=%s&verifyCode=%s&signature=%s", activeHeartbeatAPI.GetBuffer(),appkey, timestamp, token, g_verifyCode, sign);
	activeHeartbeatAPI.ReleaseBuffer();
	signature.ReleaseBuffer();
	//objHttpReq.AddParam("verifyCode",g_verifyCode);
	//objHttpReq.AddParam(_T("appkey"), _T(appkey));
	//objHttpReq.AddParam(_T("token"), _T(token));
	//objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	//objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestGet(temp_activeHeartbeatAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		CString strTmp;
		str = "";
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			strTmp = reinterpret_cast<LPCSTR> (byBuff) ;
			str.Format("%s%s", str, strTmp);
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["statusCode"].asInt() == 200) {
				string str = root["message"].asString();
				strcpy(result, str.c_str());
				return 1;
			}
		}
	} catch(...) {
		return FALSE;
	}

	return FALSE;
}


void activeThreshold(const char* appkey, const char* appsecret, const char* token, char* threshold, char* thresholdHigh) {

	if (!g_networkStatus) {
		return ;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&timestamp=%s&token=%s&verifyCode=%s%s",appkey, timestamp, token, g_verifyCode, appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	/*objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.SetHeader("X-Requested-Method", "threshold");
	objHttpReq.SetHeader("X-Requested-Param-appkey", _T(appkey));
	objHttpReq.SetHeader("X-Requested-Param-token", _T(token));
	objHttpReq.SetHeader("X-Requested-Param-timestamp", _T(timestamp));
	objHttpReq.SetHeader("X-Requested-Param-signature", _T(sign));*/
	objHttpReq.AddParam(_T("verifyCode"), g_verifyCode);
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("token"), _T(token));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	CString temp_activeThresholdAPI;
	temp_activeThresholdAPI.Format("%s?verifyCode=%s",activeThresholdAPI.GetBuffer(),g_verifyCode.GetBuffer());
	activeThresholdAPI.ReleaseBuffer();
	g_verifyCode.ReleaseBuffer();


	try {
		objHttpRes = objHttpReq.RequestPost(temp_activeThresholdAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		CString strTmp;
		str = "";
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			strTmp = reinterpret_cast<LPCSTR> (byBuff) ;
			str.Format("%s%s", str, strTmp);
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["statusCode"].asInt() == 200) {
				strcpy(threshold, root["object"].asString().c_str());
				strcpy(thresholdHigh, root["object"].asString().c_str());
			}
		}
	} catch(...) {
	}
}

int activeCheckVersion(const char* appkey, const char* appsecret, const char *version, const char *identity, char *newVersion) {
	
	if (!g_networkStatus) {
		return 0;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&identity=%s&timestamp=%s&version=%s%s",appkey, identity, timestamp, version, appsecret);

	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.SetHeader("X-Requested-Method", "check");
	objHttpReq.SetHeader("X-Requested-Param-appkey", _T(appkey));
	objHttpReq.SetHeader("X-Requested-Param-identity", _T(identity));
	objHttpReq.SetHeader("X-Requested-Param-version", _T(version));
	objHttpReq.SetHeader("X-Requested-Param-timestamp", _T(timestamp));
	objHttpReq.SetHeader("X-Requested-Param-signature", _T(sign));

	try {
		objHttpRes = objHttpReq.RequestGet(activeCheckVersionAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		CString strTmp;
		str = "";
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			strTmp = reinterpret_cast<LPCSTR> (byBuff) ;
			str.Format("%s%s", str, strTmp);
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["statusCode"].asInt() == 200) {
				string ver = root["message"].asString();
				strcpy(newVersion, ver.c_str());
				return 1;
			} else {
				return 0;
			}
		}
	} catch(...) {
		return 0;
	}

	return 0;
}

bool activeCheckFunction(const char* appkey, const char* appsecret, const char *token, char *idless, char* check) {

	
	if (!g_networkStatus) {
		return true;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&timestamp=%s&token=%s%s",appkey, timestamp, token,appsecret);

	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.SetHeader("X-Requested-Method", "function");
	objHttpReq.SetHeader("X-Requested-Param-appkey", _T(appkey));
	objHttpReq.SetHeader("X-Requested-Param-token", _T(token));
	objHttpReq.SetHeader("X-Requested-Param-timestamp", _T(timestamp));
	objHttpReq.SetHeader("X-Requested-Param-signature", _T(sign));

	try {
		objHttpRes = objHttpReq.RequestGet(activeFunctionAPI);

		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		CString strTmp;
		str = "";
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			strTmp = reinterpret_cast<LPCSTR> (byBuff) ;
			str.Format("%s%s", str, strTmp);
		}

		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["statusCode"].asInt() == 200) {

				Json::Value obj;
				obj = root["object"];
				strcpy(idless, obj["idless"].asString().c_str());
				strcpy(check, obj["check"].asString().c_str());
				return true;
			}
		}
	} catch(...) {
		return false;
	}

	return false;
}

bool activeDownloadFile(const char* appkey, const char* appsecret, const char *version, char *urls) {
	
	if (!g_networkStatus) {
		return true;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&timestamp=%s&version=%s%s",appkey, timestamp, version,appsecret);
	
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("version"), _T(version));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestUpload(activeDownloadFileAPI);
		
		const DWORD cbBuff = 1024 * 10;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		CString strTmp;
		str = "";
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			strTmp = reinterpret_cast<LPCSTR> (byBuff) ;
			str.Format("%s%s", str, strTmp);
		}

		Json::Reader reader;
		Json::Value root;
		
		CString strUrls;
		strUrls = "";
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["success"].asBool() == true) {
				Json::Value files;
				files = root["files"];

				for (unsigned int i = 0 ; i < files.size(); i++) {
					strUrls += files[i].asString().c_str();
					strUrls += ";";
				}

				strcpy(urls, strUrls.GetBuffer());

				return 1;
			}
		}
	} catch(...) {
		return FALSE;
	}

	return FALSE;
}

bool thirdHosterInfo(const char *url, const char *filename) {
	
	if (!g_networkStatus) {
		return true;
	}

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	objHttpReq.SetInternet(_T("Sedion"));

	try {
		objHttpRes = objHttpReq.RequestGet(url);
		
		const DWORD cbBuff = 1024 * 20;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;

		fstream file;
		ofstream outfile(filename);
		while ( dwRead = objHttpRes->ReadContent(byBuff, cbBuff - 1) ) {
			byBuff[dwRead]='\0';
			str = reinterpret_cast<LPCSTR> (byBuff);
			if (outfile) {
				outfile << str.GetBuffer();
			}
		}
		outfile.close();

		return true;
	} catch(...) {
	
	}

	return false;
}

bool thirdUpload(char* appkey, char* appsecret, char* url, char* idfile, char* facefile, char* fullfile, char* full_img_md5, char* id_card_img_md5, char* id_card_info, char* idcard_img_feture, char* result, char* similarity, char* vedio_img_feture, char* vedio_img_md5, char* placeCode, char* hoster_id, char* phone_num, char *reason) {
	
	if (!g_networkStatus) {
		return true;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");

	CString signature;
	signature.Format("appkey=%s&full_img_md5=%s&id_card_img_md5=%s&id_card_info=%s&idcard_img_feture=%s&place_code=%s&result=%s&similarity=%s&timestamp=%s&video_img_feture=%s&video_img_md5=%s&visited_id=%s&visited_reason=%s&visited_mobile=%s%s", appkey, full_img_md5,id_card_img_md5,id_card_info,idcard_img_feture, placeCode,result,similarity,timestamp,vedio_img_feture, vedio_img_md5, hoster_id, reason, phone_num, appsecret);

	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("place_code"), _T(placeCode));
	objHttpReq.AddParam(_T("id_card_info"), _T(id_card_info));
	objHttpReq.AddParam(_T("idcard_img_feture"), _T(idcard_img_feture));
	objHttpReq.AddParam(_T("video_img_feture"), _T(vedio_img_feture));
	objHttpReq.AddParam(_T("similarity"), _T(similarity));
	objHttpReq.AddParam(_T("result"), _T(result));
	objHttpReq.AddParam(_T("id_card_img_md5"), _T(id_card_img_md5));
	objHttpReq.AddParam(_T("video_img_md5"), _T(vedio_img_md5));
	objHttpReq.AddParam(_T("full_img_md5"), _T(full_img_md5));
	objHttpReq.AddParam(_T("visited_id"), _T(hoster_id));
	objHttpReq.AddParam(_T("visited_reason"), _T(reason));
	objHttpReq.AddParam(_T("visited_mobile"), _T(phone_num));

	CString id_card_param, video_param, full_param;
	id_card_param.Format("id_card_img%s",placeCode);
	video_param.Format("video_img%s",placeCode);
	full_param.Format("full_img%s",placeCode);
	objHttpReq.AddParam(_T(id_card_param.GetBuffer()), idfile, CHttpClient::ParamFile);
	objHttpReq.AddParam(_T(video_param.GetBuffer()), facefile, CHttpClient::ParamFile);
	objHttpReq.AddParam(_T(full_param.GetBuffer()), fullfile, CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		//MessageBox(NULL, "start", "", 0);

		objHttpReq.BeginUpload(url);
		const DWORD cbProceed = 1024;
		do {

		} while ( !(objHttpRes = objHttpReq.Proceed(cbProceed)));

		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] ;
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}
		//MessageBox(NULL, str.GetBuffer(), "", 0);
	} catch(...) {
		return false;
	}
	return true;
}

void postDQThirdPart(const char* url,const char* internet_bar_id,const char* cardId,const char* cardIdShort, const char* personName, const char* address, const char* birthday, const char* nation, const char* sex, const char* authority, const char* deadline) {
	
	if (!g_networkStatus) {
		return ;
	}

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	string utf8cardId;
	MultiToUTF8(cardId, utf8cardId);

	string utf8cardIdShort;
	MultiToUTF8(cardIdShort, utf8cardIdShort);

	string utf8personName;
	MultiToUTF8(personName, utf8personName);

	string utf8address;
	MultiToUTF8(address, utf8address);
	
	string utf8birthday;
	MultiToUTF8(birthday, utf8birthday);
	
	string utf8nation;
	MultiToUTF8(nation, utf8nation);
	
	string utf8sex;
	MultiToUTF8(sex, utf8sex);
	
	string utf8authority;
	MultiToUTF8(authority, utf8authority);
	
	string utf8deadline;
	MultiToUTF8(deadline, utf8deadline);
	
	objHttpReq.AddParam(_T("cardId"), _T(utf8cardId.c_str()));
	objHttpReq.AddParam(_T("machineCode"), _T(internet_bar_id));
	objHttpReq.AddParam(_T("cardIdShort"), _T(utf8cardIdShort.c_str()));
	objHttpReq.AddParam(_T("personName"), _T(utf8personName.c_str()));
	objHttpReq.AddParam(_T("address"), _T(utf8address.c_str()));
	objHttpReq.AddParam(_T("birthday"), _T(utf8birthday.c_str()));
	objHttpReq.AddParam(_T("nation"), _T(utf8nation.c_str()));
	objHttpReq.AddParam(_T("sex"), _T(utf8sex.c_str()));
	objHttpReq.AddParam(_T("authority"), _T(utf8authority.c_str()));
	objHttpReq.AddParam(_T("deadline"), _T(utf8deadline.c_str()));

	try {
		objHttpRes = objHttpReq.RequestPost(url);

		const DWORD cbBuff = 1024;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			str = reinterpret_cast<LPCSTR> (byBuff);
		}
	} catch(httpclientexception & e) {
	}
}


void activeUploadIdless(char* appkey, char* appsecret, char* url, char* identity, char* idfile, char* facefile, char* fullfile, char* result, char* similarity, char* vedio_img_feture, char* idcode,char* thresholdLower, char* thresholdHigher,char* placeCode, bool timeout) {
	
	if (!g_networkStatus) {
		return ;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");

	CString compareType = "2";
	CString isTimeout = "1";

	CString signature;

	if (timeout) {
		signature.Format("appkey=%s&code=%s&compareType=%s&identity=%s&isTimeoutValue=%s&place_code=%s&result=%s&similarity=%s&thresholdHigher=%s&thresholdLower=%s&timestamp=%s%s", appkey,idcode, compareType.GetBuffer(), identity,isTimeout.GetBuffer(),placeCode,result,similarity,thresholdHigher,thresholdLower,timestamp,appsecret);
	} else {
		signature.Format("appkey=%s&code=%s&compareType=%s&identity=%s&place_code=%s&result=%s&similarity=%s&thresholdHigher=%s&thresholdLower=%s&timestamp=%s&video_img_feture=%s%s", appkey,idcode, compareType.GetBuffer(), identity,placeCode,result,similarity,thresholdHigher,thresholdLower,timestamp,vedio_img_feture,appsecret);
	}

	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);

	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("place_code"), _T(placeCode));
	objHttpReq.AddParam(_T("identity"), _T(identity));
	objHttpReq.AddParam(_T("code"), _T(idcode));
	objHttpReq.AddParam(_T("thresholdHigher"), _T(thresholdHigher));
	objHttpReq.AddParam(_T("thresholdLower"), _T(thresholdLower));
	objHttpReq.AddParam(_T("compareType"), _T(compareType));
	objHttpReq.AddParam(_T("similarity"), _T(similarity));
	objHttpReq.AddParam(_T("result"), _T(result));
	objHttpReq.AddParam(_T("id_card_img"), idfile, CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("full_img"), fullfile, CHttpClient::ParamFile);
	objHttpReq.AddParam(_T("signature"), _T(sign));

	if (timeout) {
		objHttpReq.AddParam(_T("isTimeoutValue"), isTimeout.GetBuffer());
	} else {
		objHttpReq.AddParam(_T("video_img_feture"), _T(vedio_img_feture));
		objHttpReq.AddParam(_T("video_img"), facefile, CHttpClient::ParamFile);
	}

	try {
		//MessageBox(NULL, "start", "", 0);

		objHttpReq.BeginUpload(url);
		const DWORD cbProceed = 1024;
		do {

		} while ( !(objHttpRes = objHttpReq.Proceed(cbProceed)));

		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] ;
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}
		//MessageBox(NULL, str.GetBuffer(), "", 0);
	} catch(...) {
	}
}

bool activeQueryIdless(char* appkey, char* appsecret, char* url, char* identity, char* name, char* gender, char* nation, char* idcode, char* address, char* startDate, char* endData, char* bornday, char* orgnization, char* idData, char* faceFeature, char* faceFile) {
	
	if (!g_networkStatus) {
		return true;
	}

	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");
	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;

	CString signature;
	signature.Format("appkey=%s&identity=%s&timestamp=%s%s", appkey, identity, timestamp, appsecret);
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);

	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("identity"), _T(identity));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));

	try {
		objHttpRes = objHttpReq.RequestGet(url);
		const DWORD cbBuff = 1024 * 20;
		BYTE byBuff[cbBuff];
		DWORD dwRead;
		size_t cbSize = 0;
		CString str;
		CString strTmp;
		str = "";
		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			strTmp = reinterpret_cast<LPCSTR> (byBuff) ;
			str.Format("%s%s", str, strTmp);
		}
		
		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["statusCode"].asInt() == 200) {
				Json::Value obj;
				obj = root["object"];
				if (!obj.isNull()) {
					if (obj["gender"].asInt() == 0) {
						strcpy(gender, "女");
					} else {
						strcpy(gender, "男");
					}

					strcpy(name, obj["name"].asString().c_str());
					strcpy(nation, obj["nation"].asString().c_str());
					strcpy(idcode, obj["code"].asString().c_str());
					strcpy(address, obj["address"].asString().c_str());
					strcpy(startDate, obj["startDate"].asString().c_str());
					strcpy(endData, obj["endDate"].asString().c_str());
					strcpy(bornday, obj["bornday"].asString().c_str());
					strcpy(orgnization, obj["orgnization"].asString().c_str());
					strcpy(idData, obj["metadata"].asString().c_str());
					strcpy(faceFeature, obj["imgFeature"].asString().c_str());
					strcpy(faceFile, obj["fileData"].asString().c_str());

					return true;
				}

			}
		}
	} catch(...) {
		
	}

	return false;
}







void utf8(const char* input, char* output) {
	string soutp;
	string sinp(input);
	MultiToUTF8(sinp, soutp);

	strcpy(output, soutp.c_str());
}



bool init(void) {
	static bool network_init = false;
	if (network_init) {
		network_init = InitIcmp();
	}


	return true;
}



bool verifyTrainInfo(const char* appkey, char* appsecret, const char* url, const char* placeCode, const char* verifyCode) {


	CHttpClient objHttpReq;
	CHttpResponse * objHttpRes = NULL;
	objHttpReq.SetUseUtf8(TRUE);

	// 时间戳
	CTime time = CTime::GetCurrentTime();
	CString timestamp = time.Format("%Y-%m-%d %H:%M:%S");

	// signature
	CString signature;
	signature.Format("appkey=%s&placeCode=%s&timestamp=%s&verifyCode=%s%s",appkey,placeCode,timestamp,verifyCode,appsecret);
	string utf8Str;
	MultiToUTF8(signature.GetBuffer(), utf8Str);
	char sign[33];
	md5((unsigned char*)utf8Str.c_str(), utf8Str.size(), sign);
	//_strupr_s(sign,sizeof(sign));

	// 请求预备
	objHttpReq.SetInternet(_T("Sedion"));
	objHttpReq.AddParam(_T("appkey"), _T(appkey));
	objHttpReq.AddParam(_T("placeCode"), _T(placeCode));
	objHttpReq.AddParam(_T("verifyCode"), _T(verifyCode));
	objHttpReq.AddParam(_T("timestamp"), _T(timestamp));
	objHttpReq.AddParam(_T("signature"), _T(sign));


	try {
		// 发送请求
		objHttpRes = objHttpReq.RequestPost(url);
		const DWORD		cbBuff = 1024 * 10 ;
		BYTE			byBuff[cbBuff] = {};
		DWORD			dwRead ;
		size_t			cbSize = 0 ;
		CString		str ;

		while ( dwRead = objHttpRes->ReadContent (byBuff, cbBuff - 1) ) {
			cbSize += dwRead ;
			byBuff[dwRead] = '\0';
			str = reinterpret_cast<LPCSTR> (byBuff) ;
		}
		
		Json::Reader reader;
		Json::Value root;
		if (reader.parse(str.GetBuffer(), root)) {
			if (root["statusCode"].asInt() != 200) {
				g_networkStatus = false;
				return false;
			}
		}
		else {
			g_networkStatus = false;
			return false;
		}
	} catch(...) {
		g_networkStatus = false;
		return false;
	}

	g_placeCode.Format("%s",placeCode);
	g_verifyCode.Format("%s",verifyCode);
	return true;
}








DLLOPTION void setBaseApiUrl(const char* url) {

	// API URL
	baseAPI.Format("%s",url);

	// active threshold
	activeThresholdAPI = baseAPI + "threshold";
	activeHeartbeatAPI = baseAPI + "active";

	
}











#define MAXBLOCKSIZE 1024
void saveLogo(const char* initType, const char* fileName) {
	HINTERNET hSession = InternetOpen("RookIE/1.0", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	if (hSession != NULL) {
		char* url;

		if (initType == "6")
			url = schoolLogoAPI.GetBuffer();

		HINTERNET handle2 = InternetOpenUrl(hSession, url, NULL, 0, INTERNET_FLAG_DONT_CACHE, 0);
		if (handle2 != NULL) {
			byte Temp[MAXBLOCKSIZE];
			ULONG Number = 1;

			FILE *stream;
			if( (stream = fopen(fileName, "wb" )) != NULL ) {
				while (Number > 0) {
					InternetReadFile(handle2, Temp, MAXBLOCKSIZE - 1, &Number);
					//fprintf(stream, (const char*)Temp);
					fwrite(Temp, sizeof (char), Number , stream);
				}
				fclose( stream );
			}
   
			InternetCloseHandle(handle2);
			handle2 = NULL;
		}
		InternetCloseHandle(hSession);
		hSession = NULL;
	}
}