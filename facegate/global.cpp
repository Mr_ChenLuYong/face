#include <QtWidgets>
#include <QAxObject>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QCryptographicHash>

#include "global.h"

QString settingFile;
QString appkey;
QString appsecret;
QString placeCode;
QString machineCode;
QNetworkAccessManager nam;

QString gloUrl = "http://facereco.eshanren.com/api/";

QString getMD5(QString input) {
	QString md5;
	QByteArray bb;
	bb = QCryptographicHash::hash(input.toUtf8(), QCryptographicHash::Md5);
	md5.append(bb.toHex());

	return md5;
}

void writeSetting(QString *fileName, QString group, QString key, QString val) {
	QSettings settings(*fileName, QSettings::IniFormat);
	settings.beginGroup(group);
	settings.setValue(key, val);
}

QJsonObject httpPost(QString strUrl, QString context) {
	QNetworkRequest request;
	QUrl url(strUrl);

	QByteArray data;
	data.append(context);
	request.setUrl(url);
	QNetworkReply* reply = nam.post(request, data);
	QEventLoop eventLoop;
	QObject::connect(&nam, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
	eventLoop.exec();	//block until finish

	QJsonObject jsonObj;
	if (reply->error() == QNetworkReply::NoError) {
		QByteArray bytes = reply->readAll();
		QJsonDocument jsonDocument = QJsonDocument::fromJson(bytes);
		if (!jsonDocument.isNull()) {
			jsonObj = jsonDocument.object();
		}
	}
	return jsonObj;
}

QJsonObject httpCheckSerial(QString serial) {
	appkey = getMD5("密键:" + serial);
	appsecret = getMD5("密钥:"+appkey + machineCode);

	QDateTime time = QDateTime::currentDateTime();
	QString timestamp = time.toString("yyyy-MM-dd hh:mm:ss");
	QString params = "appkey=" + appkey + "&check=check&machine="+machineCode + "&serial=" + serial+ "&timestamp="+ timestamp;
	QString signature = getMD5(params + appsecret);

	QString context = params + "&signature=" + signature;
	QJsonObject jsonObj = httpPost(gloUrl + "active", context);

	QJsonObject jsonModel;
	if (jsonObj.contains("statusCode")) {
		QJsonValue statusCode = jsonObj.take("statusCode");
		if (statusCode.isDouble()) {
			if (statusCode.toDouble() == 200) {
				return jsonObj.take("object").toObject();
			}
		}
	}

	return jsonModel;
}

int httpActiveSerial(QString serial) {
	appkey = getMD5("密键:" + serial);
	appsecret = getMD5("密钥:"+appkey + machineCode);

	QDateTime time = QDateTime::currentDateTime();
	QString timestamp = time.toString("yyyy-MM-dd hh:mm:ss");
	QString params = "appkey=" + appkey + "&machine="+machineCode + "&serial=" + serial+ "&timestamp="+ timestamp;
	QString signature = getMD5(params + appsecret);

	QString context = params + "&signature=" + signature;
	QJsonObject jsonObj = httpPost(gloUrl + "active", context);

	if (jsonObj.contains("statusCode")) {
		QJsonValue statusCode = jsonObj.take("statusCode");
		if (statusCode.isDouble()) {
			if (statusCode.toDouble() == 200) {
				return 1;
			}
		}
	}

	return -1;
}

bool httpActiveRegister(QString type, QString name, QString owner, QString phone, QString address) {
	QDateTime time = QDateTime::currentDateTime();
	QString timestamp = time.toString("yyyy-MM-dd hh:mm:ss");
	QString params = "address=" + address + "&appkey=" + appkey + "&name=" + name + "&owner=" + owner + "&phone=" + phone + "&timestamp=" + timestamp + "&type=" + type;
	QString signature = getMD5(params + appsecret);

	QString context = params + "&signature=" + signature;
	QJsonObject jsonObj = httpPost(gloUrl + "register", context);

	if (jsonObj.contains("statusCode")) {
		QJsonValue statusCode = jsonObj.take("statusCode");
		if (statusCode.isDouble()) {
			return (statusCode.toDouble() == 200);
		}
	}

	return false;
}