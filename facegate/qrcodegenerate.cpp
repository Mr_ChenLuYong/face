#include "qrcodegenerate.h"

#include <QDateTime>
#include <QMessageBox>
#include "global.h"

QrcodeGenerate::QrcodeGenerate(QWidget *parent) :
    QWidget(parent)
{
    qr = NULL;
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString timestamp = current_date_time.toString("yyyyMMddhhmm");

    // 二维码生成的密钥url地址
    QString url = "http://facereco.eshanren.com/api/railway?timestamp="+
            timestamp + "&machine=" + machineCode;


    // 本机密文
    QString appsecret = getMD5("动车站:" + machineCode);
    // 序列号全文
    prvSerial_ = getMD5(machineCode + timestamp + appsecret);
    // 格式化序列号
    prvSerial_ = prvSerial_.mid(5,16);
    prvSerial_ = prvSerial_.toUpper();

    generateString(url);
}

QrcodeGenerate::~QrcodeGenerate()
{
    if(qr != NULL)
    {
       QRcode_free(qr);
    }
}
bool QrcodeGenerate::saveImage(QString fileName, int size)
{
    if(size != 0 && !fileName.isEmpty())
    {
        QImage image(size, size, QImage::Format_Mono);
        QPainter painter(&image);
        QColor background(Qt::white);
        painter.setBrush(background);
        painter.setPen(Qt::NoPen);
        painter.drawRect(0, 0, size, size);
        if(qr != NULL)
        {
            draw(painter, size, size);
        }
        return image.save(fileName);
    }
    else
    {
        return false;
    }
}

void QrcodeGenerate::generateString(QString str)
{
    string = str;
    if(qr != NULL)
    {
        QRcode_free(qr);
    }
    qr = QRcode_encodeString(string.toStdString().c_str(),
                             1,
                             QR_ECLEVEL_H,
                             QR_MODE_8,
                             1);
    update();

}


void QrcodeGenerate::draw(QPainter &painter, int width, int height)
{
    QColor foreground(Qt::black);
    painter.setBrush(foreground);
    const int qr_width = qr->width > 0 ? qr->width : 1;
    double scale_x = width / qr_width;
    double scale_y = height / qr_width;
    for( int y = 0; y < qr_width; y ++)
    {
        for(int x = 0; x < qr_width; x++)
        {
            unsigned char b = qr->data[y * qr_width + x];
            if(b & 0x01)
            {
                QRectF r(x * scale_x+5, y * scale_y+5, scale_x, scale_y);
                painter.drawRects(&r, 1);
            }
        }
    }
}

void QrcodeGenerate::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QColor background(Qt::white);
    painter.setBrush(background);
    painter.setPen(Qt::NoPen);
    painter.drawRect(0, 0, width(), height());
    if(qr != NULL)
    {
        draw(painter, width(), height());
    }
}

QSize QrcodeGenerate::sizeHint()  const
{
    QSize s;
    if(qr != NULL)
    {
        int qr_width = qr->width > 0 ? qr->width : 1;
        s = QSize(qr_width * 4, qr_width * 4);
    }
    else
    {
        s = QSize(50, 50);
    }
    return s;
}

QSize QrcodeGenerate::minimumSizeHint()  const
{
    QSize s;
    if(qr != NULL)
    {
        int qr_width = qr->width > 0 ? qr->width : 1;
        s = QSize(qr_width, qr_width);
    }
    else
    {
        s = QSize(50, 50);
    }
    return s;
}
