#ifndef SRFACEGATE_H
#define SRFACEGATE_H

#include <QDialog>

#include <qrencode.h>

class QLabel;
class QPushButton;
class QLineEdit;
class QrcodeGenerate;

class SRFacegate : public QDialog
{
    Q_OBJECT

public:
    SRFacegate(QWidget *parent = 0);
    ~SRFacegate();
    QString getSerialCode(void) { return serialCode_; }

protected:

    virtual void keyPressEvent(QKeyEvent *event);

    void resizeEvent(QResizeEvent *);

private:

    void draw(QPainter &painter, int width, int height);


private slots:

    void onGotClick(void);
    void onLicenceEditChange(QString _info);




private:

    // 二维码标签
    QrcodeGenerate* labQrCode_;
    QPushButton* btnGotQrCode_;
    // 关闭按钮
    QPushButton* btnClose_;
    // 验证码控件
    QLineEdit* editLicence_;
    // 背景
    QLabel* labBackground_;

private:
    QString serialCode_;
};

#endif // SRFACEGATE_H
