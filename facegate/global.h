#ifndef GLOBAL_H
#define GLOBAL_H

#include <windows.h>

// setting.ini file name;
extern QString settingFile;
// appkey
extern QString appkey;
// appsecret
extern QString appsecret;
// place code
extern QString placeCode;
// machine code
extern QString machineCode;

QString getMD5(QString input);

extern void writeSetting(QString *fileName, QString group, QString key, QString val);

extern QJsonObject httpCheckSerial(QString serial);

extern int httpActiveSerial(QString serial);

extern bool httpActiveRegister(QString type, QString name, QString owner, QString phone, QString address);

#endif
