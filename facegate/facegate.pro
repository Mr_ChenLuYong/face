#-------------------------------------------------
#
# Project created by QtCreator 2017-09-27T15:34:20
#
#-------------------------------------------------

TEMPLATE = lib
TARGET = facegate

INCLUDEPATH += . \
    ../target/include/

#LIBS += -L"H:/qrcodeText/"
LIBS += -L"../target/lib"
LIBS += -L"../target/bin"
LIBS += -lqrcodelib

DESTDIR = ../target/bin

#QT       += core gui
QT += widgets network axcontainer


#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        srfacegate.cpp \
    global.cpp \
    commonhelper.cpp \
    oeprompt.cpp \
    qrcodegenerate.cpp

HEADERS += \
        srfacegate.h \
    global.h \
    commonhelper.h \
    oeprompt.h \
    res.h \
    qrcodegenerate.h

RESOURCES += \
    res.qrc
