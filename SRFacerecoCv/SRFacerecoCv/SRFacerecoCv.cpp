// SRFacerecoCv.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "SRFacerecoCv.h"
#include "src/CvxText.h"




int SRFacerecoCv::pasteTransparencyImage(cv::Mat& _dest, const cv::Mat& _transparency,
					    float _transparence, COLORREF _transparentColor) {

	if (_dest.cols < _transparency.cols 
		|| _dest.rows < _transparency.rows) {
			return 1;
	}

	try {
		// 获得透明背景色
		const BYTE R = GetRValue(_transparentColor);
		const BYTE G = GetGValue(_transparentColor);
		const BYTE B = GetBValue(_transparentColor);
		// 贴图片
		for (int r = 0; r < _transparency.rows; ++r) {
			for (int c = 0; c < _transparency.cols; ++c) {

				cv::Vec3b tr_mat_val = _transparency.at<cv::Vec3b>(cv::Point(c, r));
				// 过滤透明色
				if (_transparence < 1&& 
					tr_mat_val[0] == R &&
					tr_mat_val[1] == G &&
					tr_mat_val[2] == B) {
					continue;
				}
				// 逐像素重构
				cv::Vec3b& dest_mat_val = _dest.at<cv::Vec3b>(cv::Point(c, r));
				dest_mat_val[0] = (uchar)(dest_mat_val[0] * (1 - _transparence) + tr_mat_val[0] * _transparence);
				dest_mat_val[1] = (uchar)(dest_mat_val[1] * (1 - _transparence) + tr_mat_val[1] * _transparence);
				dest_mat_val[2] = (uchar)(dest_mat_val[2] * (1 - _transparence) + tr_mat_val[2] * _transparence);
			}
		}
	}
	catch (std::exception ex) {
		std::cout << ex.what() << std::endl;
		return -1;
	}

	return 0;
}

int SRFacerecoCv::pasteText(const cv::Mat& _img, const char* const _text, double _fontSize, CvPoint _pos, CvScalar _color) {
	// 做个文件存在性判断
	CvxText text("res/cvfont.ttf");
	if (_fontSize > 0) {
			
		CvScalar font_size;
		font_size.val[0] = _fontSize;      // 字体大小  
		font_size.val[1] = 0.5;   // 空白字符大小比例  
		font_size.val[2] = 0.14;   // 间隔大小比例  
		font_size.val[3] = 0;      // 旋转角度(不支持) 
		text.setFont(NULL,&font_size);
	}
	text.putText(&IplImage(_img), _text, _pos, _color);
	return 0;
}


