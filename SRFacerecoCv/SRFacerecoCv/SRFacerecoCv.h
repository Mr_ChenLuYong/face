/*
* c++ library
* depends:
*      opencv
*      freetype
*/

#ifndef __FACERECO_SRFACERECOCV_H__
#define __FACERECO_SRFACERECOCV_H__

#ifdef SRFACERECOCV_EXPORTS
#define SRFACERECOCV_API __declspec(dllexport)
#else
#define SRFACERECOCV_API __declspec(dllimport)
#endif


#include <opencv\cv.h>
#include <opencv\highgui.h>
#include <opencv2\opencv.hpp>
class SRFACERECOCV_API SRFacerecoCv {
	
public:

	static int pasteTransparencyImage(cv::Mat& _dest, const cv::Mat& _transparency,
			float _transparence = 1.0, COLORREF _transparentColor = RGB(255, 255, 255));

	static int pasteText(const cv::Mat& _img, const char* const _text, double _fontSize,
							 CvPoint _pos, CvScalar _color);

};




#endif // !__FACERECO_SRFACERECOCV_H__