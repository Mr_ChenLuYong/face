#include "srmainwidget.h"
#include <QApplication>

#include "oe/CommonHelper"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CommonHelper::setStyle(":/qss/black");
    SRMainWidget w;
    w.show();

    return a.exec();
}
