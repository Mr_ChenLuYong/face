
#include "srmainwidget.h"

#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QSettings>
#include <QDebug>
#include <QRegExpValidator>
#include "custom/oeipaddresswidget.h"
#include "oe/CommonHelper"

static SRMainWidget* g_mainWidget= Q_NULLPTR;
SRMainWidget::SRMainWidget(QWidget *parent)
    : QWidget(parent) {
    setWindowTitle(tr("系统设置"));
    QGridLayout* layout = new QGridLayout();

    QLabel* lab_face_ip = new QLabel();
    wdgtIpAddress_ = new OEIpAddressWidget();
    lab_face_ip->setText(tr("IP地址   :"));

    QLabel* lab_site_id = new QLabel();
    editPlaceId_ = new QLineEdit();
    lab_site_id->setText("场所编号 :");

    QLabel* lab_code_id = new QLabel();
    editVerifyId_ = new QLineEdit();
    lab_code_id->setText("校验码   :");

    // 添加规则
    QRegExp regExp("^[0-9a-zA-Z]{0,20}$");
    editPlaceId_->setValidator(
            new QRegExpValidator(regExp, editPlaceId_));
    editVerifyId_->setValidator(
            new QRegExpValidator(regExp, editVerifyId_));


    QPushButton* btn_confirm = new QPushButton();
    btn_confirm->setText("确认");

    QPushButton* btn_cancel = new QPushButton();
    btn_cancel->setText("取消");


    connect(btn_confirm, SIGNAL(clicked()),
            this,SLOT(onConfirm()));

    connect(btn_cancel, SIGNAL(clicked()),
            this,SLOT(hide()));

    connect(wdgtIpAddress_,SIGNAL(sigIpDone(QString)),
            this,SLOT(onIpDone(QString)));


    /// 布局
    // ip地址
    layout->addWidget(lab_face_ip,0,0,1,1);
    layout->addWidget(wdgtIpAddress_,0,1,1,5);
    // 场所编号
    layout->addWidget(lab_site_id,1,0,1,1);
    layout->addWidget(editPlaceId_,1,1,1,5);
    // 校验码
    layout->addWidget(lab_code_id,2,0,1,1);
    layout->addWidget(editVerifyId_,2,1,1,5);
    // 确认与取消
    layout->addWidget(btn_confirm,3,1,2,2);
    layout->addWidget(btn_cancel,3,3,2,2);


    setLayout(layout);
    setMinimumSize(320,140);
    setMaximumSize(640,150);
    setGeometry(QRect(pos(),minimumSize()));

    CommonHelper::moveCenter(this);
}

SRMainWidget::~SRMainWidget() {

}

void SRMainWidget::showEvent(QShowEvent *) {
    QSettings setting("config.ini", QSettings::IniFormat);
    wdgtIpAddress_->setText(setting.value("NET/ip").toString());
    editVerifyId_->setText(setting.value("NET/verify").toString());
    editPlaceId_->setText(setting.value("NET/place").toString());
}

void SRMainWidget::onIpDone(QString ip) {
#ifdef QT_DEBUG
    qDebug() << ip;
#else
    Q_UNUSED(ip)
#endif
}

void SRMainWidget::onConfirm() {
    QSettings setting("config.ini", QSettings::IniFormat);

    setting.setValue("NET/ip",wdgtIpAddress_->text());

    setting.setValue("NET/verify", editVerifyId_->text());

    setting.setValue("NET/place", editPlaceId_->text());

    hide();
}

extern "C" __declspec(dllexport) int SRShowSetting(void) {
    if (!g_mainWidget)
        g_mainWidget = new SRMainWidget();
    g_mainWidget->show();
    return 0;
}
