#ifndef SRMAINWIDGET_H
#define SRMAINWIDGET_H

#include <QWidget>
class QLineEdit;
class QNetworkAccessManager;
class OEIpAddressWidget;

class SRMainWidget : public QWidget
{
    Q_OBJECT

public:
    SRMainWidget(QWidget *parent = 0);
    ~SRMainWidget();

protected:

    virtual void showEvent(QShowEvent *);

private Q_SLOTS:

    // ip规则检查
    void onIpDone(QString ip);

    // 确认保存事件
    void onConfirm(void);


private:
    OEIpAddressWidget* wdgtIpAddress_;
    QLineEdit* editPlaceId_;
    QLineEdit* editVerifyId_;

};

#endif // SRMAINWIDGET_H
