#ifndef OEIPADDRESSWIDGET_H
#define OEIPADDRESSWIDGET_H

#include <QWidget>
#include <QList>

class QLineEdit;
class QHBoxLayout;

class OEIpAddressWidget : public QWidget
{
    Q_OBJECT
public:
    explicit OEIpAddressWidget(QWidget *parent = nullptr);

    QString text(void) const;

    void setText(QString ip);

protected:

    bool event(QEvent *event);

Q_SIGNALS:

    // 完成了ip的输入
    void sigIpDone(QString);

public Q_SLOTS:

    void onEditPressed(int idx);

public:

    QHBoxLayout *mainLayout_;

    QList<QLineEdit*> listNumber_;

};

#endif // OEIPADDRESSWIDGET_H
