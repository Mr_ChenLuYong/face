#include "oeipaddresswidget.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QRegExpValidator>
#include <QSignalMapper>
#include <QEvent>
#include <QKeyEvent>
#ifndef QT_NO_DEBUG
#include <QDebug>
#endif

OEIpAddressWidget::OEIpAddressWidget(QWidget *parent)
    : QWidget(parent) {
    mainLayout_ = new QHBoxLayout(this);
    QLabel* label1 = new QLabel(".",this);
    QLabel* label2 = new QLabel(".",this);
    QLabel* label3 = new QLabel(".",this);

    // 添加ip输入框
    QSignalMapper *mapper = new QSignalMapper();
    QRegExp regExp("25[0-5]|2[0-4]\\d|[01]?\\d\\d?");
    for (int i = 0; i < 4; ++i) {
        QLineEdit* temp_number_edit = new QLineEdit(this);
        installEventFilter(temp_number_edit);
        // 添加规则
        temp_number_edit->setValidator(
                new QRegExpValidator(regExp, temp_number_edit));
        listNumber_.push_back(temp_number_edit);
        connect(temp_number_edit,SIGNAL(returnPressed()),
                mapper,SLOT(map()));
        mapper->setMapping(temp_number_edit,i);
    }

    // edit 信号 通过 mapper 统一链接
    connect(mapper,SIGNAL(mapped(int)),
        this,SLOT(onEditPressed(int)));

    // 布局
    mainLayout_->addWidget(listNumber_.at(0));
    mainLayout_->addWidget(label1);
    mainLayout_->addWidget(listNumber_.at(1));
    mainLayout_->addWidget(label2);
    mainLayout_->addWidget(listNumber_.at(2));
    mainLayout_->addWidget(label3);
    mainLayout_->addWidget(listNumber_.at(3));

    setLayout(mainLayout_);
}

QString OEIpAddressWidget::text() const {
    QString ip = listNumber_.at(0)->text() + "."
            + listNumber_.at(1)->text() + "."
            + listNumber_.at(2)->text() + "."
            + listNumber_.at(3)->text();
    return ip;
}

void OEIpAddressWidget::setText(QString ip)
{
    QStringList list_ip = ip.split(".");
    // 正确的Ip地址
    if (list_ip.size() == 4) {
        for(auto edit : listNumber_) {
            // 保证是数字
            const QString& qstr_ip = list_ip.front();
            int int_ip = qstr_ip.toInt();
            if (int_ip < 0 || int_ip > 255)
                int_ip = 0;
            // 设置文本
            edit->setText(QString::number(int_ip));
            list_ip.pop_front();
        }
    }
    // 非法的IP地址
    else {

    }
}

bool OEIpAddressWidget::event(QEvent *event)
{
    if (event->type() == QEvent::KeyRelease) {
        QKeyEvent* key_ev = (QKeyEvent*) event;
        if (Qt::Key_Period == key_ev->key()) {
            for(int i = 0; i < listNumber_.size(); ++i)
                // 确定当前焦点窗口
                if (listNumber_.at(i) == this->focusWidget()) {
                    onEditPressed(i);
                    break;
                }
        }
    }
    return QWidget::event(event);
}


void OEIpAddressWidget::onEditPressed(int idx) {
    ++idx;
    if (idx < 0)
        idx = 0;
    else if (idx > 4)
        idx = 4;

    if (idx == listNumber_.size())
        // 可以在这里做一个简单的规则校验
        // 也可以发送信号让其他窗口处理规则
        emit sigIpDone(text());
    else {
        listNumber_.at(idx)->setFocus();
        listNumber_.at(idx)->selectAll();
    }
}
