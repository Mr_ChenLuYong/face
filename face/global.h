#ifndef GLOBAL_H
#define GLOBAL_H

// global path
extern QString gloPath;
// storage path
extern QString stoPath;
// face.dat file name
extern QString datFile;
// face.log file name
extern QString logFile;
// config.ini file name;
extern QString configFile;
// setting.ini file name;
extern QString settingFile;
// commond.bat file name;
extern QString cmdFile;
// machine code
extern QString machineCode;
// appkey
extern QString appkey;
// appsecret
extern QString appsecret;
// place code
extern QString placeCode;


extern void writeFile(QString *fileName, QString context);
extern QString readFile(QString *fileName, int startPos, int length);


extern void writeLog(QString context);
extern void writeLog(QString context, bool ereaseTime);


extern void writeSetting(QString *fileName, QString group, QString key, QString val);
extern QString readSetting(QString *fileName, QString group, QString key);


extern QString getMD5(QString input);

extern QString getWMIHWInfo();

extern QString getCurrentTime();

extern bool copyDirectoryFiles(const QString &fromDir, const QString &toDir);

#endif