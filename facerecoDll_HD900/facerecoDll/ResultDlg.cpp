// ResultDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "facerecoDll.h"
#include "ResultDlg.h"


// ResultDlg 对话框

IMPLEMENT_DYNAMIC(ResultDlg, CDialog)

ResultDlg::ResultDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ResultDlg::IDD, pParent)
{

}

ResultDlg::~ResultDlg()
{
}

void ResultDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(ResultDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_STN_CLICKED(1601, &ResultDlg::OnStnClickedConfirm)
	ON_STN_CLICKED(1602, &ResultDlg::OnStnClickedRefuse)
	ON_STN_CLICKED(1620, &ResultDlg::OnStnClickedKnow)
END_MESSAGE_MAP()


// ResultDlg 消息处理程序


BOOL ResultDlg::OnInitDialog() {

	CDialog::OnInitDialog();

	SetFocus();

	// 窗口置顶
	SetWindowPos(&CWnd::wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

	int cx = GetSystemMetrics(SM_CXSCREEN);   
	int cy = GetSystemMetrics(SM_CYSCREEN);

	// 设置窗口大小
	if (screen != 1) {
		int width = 986;
		int height = 617;
		MoveWindow((cx - width) / 2,(cy - height) / 2, width, height);

		cx = width;
		cy = height;
	} else {
		MoveWindow(0,0,cx,cy);
	}

	// 设置上边距
	int topgap = 0;
	if (cy > 617) {
		topgap = (cy - 617) / 2;
	}
	

	int titleWidth;
	int titlePicId;
	if (scoreLevel == 2) {
		titleWidth = 133;
		titlePicId = IDB_TITLECHECK;
	} else if (scoreLevel == 1) {
		titleWidth = 106;
		titlePicId = IDB_TITLEPASS;
	} else if (scoreLevel == 4) {
		titleWidth = 236;
		titlePicId = IDB_TITLEREFUSE;
	}
	
	// 标题
	int titleY = 40 + topgap;
	int titleX = (cx - titleWidth) / 2;
	picTitle.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY, CRect(titleX, titleY, titleX+titleWidth, titleY+28),this, 1604);
	picTitle.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(titlePicId)));
	picTitle.ShowWindow(true);

	// Logo
	int logoX = 20;
	int logoY = 40;
	picLogo.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY, CRect(logoX, logoY, logoX+264, logoY+36),this, 1626);
	picLogo.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_RESULTLOGO)));
	picLogo.ShowWindow(true);

	int scoreY;
	if (scoreLevel != 0) {
		// 证件照
		int idX = (cx - 72 - 256 - 20) / 2;
		int idY = titleY + 70;
		LoadBmpFromFile(idFile, idImg);
		picIdCard.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(idX, idY, idX + 72, idY + 89),this, 1610);
		picIdCard.SetBitmap(idImg);
		picIdCard.ShowWindow(true);
		
		// 人像照
		int avatarX = idX;
		int avatarY = idY + 103;
		LoadBmpFromFile(avatarFile, avatarImg);
		picAvatar.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(avatarX, avatarY, avatarX+72, avatarY + 89),this, 1611);
		picAvatar.SetBitmap(avatarImg);
		picAvatar.ShowWindow(true);
		
		// 视频照
		int videoX = idX + 97;
		int videoY = idY;
		picVideo.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(videoX, videoY, videoX+256, videoY + 192),this, 1612);
		picVideo.ShowWindow(true);

		// 分数
		int scoreX = (cx - 214) / 2;
		int scoreY = idY + 192 + 20;
		lblSimilarity.Create("相似度", WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(scoreX, scoreY, scoreX + 100, scoreY + 50), this, 1631);
		lblSimilarity.ShowWindow(true);

		CString text;
		text.Format("%d", similarity);
		lblScore.Create(text, WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(scoreX + 105, scoreY - 10, scoreX + 155, scoreY + 50), this, 1632);
		lblScore.ShowWindow(true);

		lblPercent.Create("%", WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(scoreX + 170, scoreY, scoreX + 190, scoreY + 50), this, 1633);
		lblPercent.ShowWindow(true);
	} else {
		scoreY = titleY = 50;
	}

	int btnY = scoreY + 90;
	if (scoreLevel == 0) btnY = cy / 2 - 50;
	if (scoreLevel == 1 || scoreLevel == 4 || scoreLevel == 0) {
		// 知道了  按钮
		int knowX= (cx - 156) / 2;
		btnKnow.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(knowX, btnY, knowX + 156, btnY + 38),this, 1620);
		if (scoreLevel == 1) {
			btnKnow.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BTNBLUEKNOW)));
		} else if (scoreLevel == 4 || scoreLevel == 0) {
			btnKnow.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BTNREDKNOW)));
		}
		btnKnow.ShowWindow(true);
	} else {
		int btnX = (cx-320) / 2;
		// 通过
		btnConfirm.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY, CRect(btnX, btnY, btnX + 155, btnY+37), this, 1601);
		btnConfirm.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BTNPASS)));
		btnConfirm.ShowWindow(true);

		// 否决
		btnRefuse.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY, CRect(btnX, btnY, btnX + 155, btnY + 37)/*CRect(btnX + 165, btnY, btnX + 320, btnY+37)*/, this, 1602);
		btnRefuse.SetBitmap(::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BTNREFUSE)));
		btnRefuse.ShowWindow(true);
	}


	CString tip;
	if (scoreLevel == 2) {
		tip = "相似度未达标, 请仔细审核后, 手动点击 \"通过\" 或 \"否决\" 按钮, 15秒后将自动关闭";
	} else if (scoreLevel == 1) {
		tip = "相似度达标, 审核已通过, 7秒后将自动关闭";
	} else if (scoreLevel == 4) {
		tip = "相似度过低, 该用户证件照片不符, 7秒后将自动关闭";
	} else if (scoreLevel == 0) {
		tip = "检测超时, 请重试, 7秒后将自动关闭";
	} 
	int tipY = btnY + 65;
	lblTip.Create(tip, WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(0, tipY, cx, tipY+40), this, 1603);
	lblTip.ShowWindow(true);

	if (scoreLevel == 1 || scoreLevel == 4 || scoreLevel == 0) {
		sleepSeconds = 7000;
		response = scoreLevel;
		AfxBeginThread(ExitThreadProc, this);
	} else if (scoreLevel == 2) {
		sleepSeconds = 15000;
		response = 0;
		AfxBeginThread(ExitThreadProc, this);
	}

	//btnIcon.ShowWindow(true);
	return TRUE;
}


HBRUSH ResultDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	int nID = pWnd->GetDlgCtrlID();
	if (nID == 1603) {
		pDC->SetTextColor(RGB(255,0,0));
	}

	return hbr;
}


BOOL ResultDlg::PreTranslateMessage(MSG* pMsg) {
	if(pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
			case VK_RETURN:
				if (scoreLevel != 2) {
					response = scoreLevel;
					OnCancel();
				} else if (scoreLevel == 2) {
					response = 2;
					OnCancel();
				}
		}
	}
    return CDialog::PreTranslateMessage(pMsg);
}

void ResultDlg::Init(int level,int score, CString id, CString avatar, CString video, int fullScreen) {
	scoreLevel = level;
	idFile = id;
	avatarFile = avatar;
	videoFile = video;
	similarity = score;
	screen = fullScreen;
	exited = FALSE;
}

int ResultDlg::Response() {
	return response;
}

void ResultDlg::OnStnClickedConfirm() {
	response = 2;
	OnOK();
}

void ResultDlg::OnStnClickedRefuse() {
	response = 0;
	OnCancel();
}

void ResultDlg::OnStnClickedKnow() {
	if (scoreLevel == 0) {
		response = scoreLevel;
		OnCancel();
	} else {
		if (exited) return;
		exited = TRUE;
		response = scoreLevel;
		OnCancel();
	}
}

void ResultDlg::OnPaint() {
	CPaintDC dc(this); // 用于绘制的设备上下文

	CRect winRect;
	GetClientRect(&winRect);
	CDC dcMem;
	dcMem.CreateCompatibleDC(&dc);
	CBitmap bmpBackground;
	bmpBackground.LoadBitmap(IDB_RESULTBACK);
	BITMAP bitmap;
	bmpBackground.GetBitmap(&bitmap);
	CBitmap* pbmpOld = dcMem.SelectObject(&bmpBackground); 
	dc.StretchBlt(0,0,winRect.Width(),winRect.Height(),&dcMem,0,0,bitmap.bmWidth,bitmap.bmHeight,SRCCOPY);


	CFont normal;
	normal.CreatePointFont(100,_T("微软雅黑"),NULL);
	DrawLabel(&lblTip, &normal, RGB(255,255,255), DT_CENTER);
	
	if (scoreLevel != 0) {
		DrawVideo();
		

		CFont simFont;
		simFont.CreatePointFont(200,_T("微软雅黑"),NULL);
		CFont numFont;
		numFont.CreatePointFont(350,_T("微软雅黑"),NULL);
		DrawLabel(&lblSimilarity, &simFont, RGB(255,255,255), DT_CENTER);
		DrawLabel(&lblScore, &numFont, RGB(255,0,0), DT_CENTER);
		DrawLabel(&lblPercent, &simFont, RGB(255,255,255), DT_CENTER);
	}

	CDialog::OnPaint();
}

void ResultDlg::DrawVideo() {
	Mat im;
	im = imread(videoFile.GetBuffer());
	int nWidth=im.cols;
	int nHeight = im.rows;
	long lLength = nWidth * nHeight * 3;
	BYTE* pCamBuf = new BYTE[lLength];
	memcpy(pCamBuf,im.data,nWidth*nHeight*3);

	BITMAPINFOHEADER bih;
	ContructBih(nWidth,nHeight,bih);
	LONG lLineBytes;
	lLineBytes = WIDTHBYTES(nWidth *24);
	BYTE* videoByte=new BYTE[lLineBytes*nHeight];
	for(int i=0;i<nHeight;i++)
	{
		for(int j=0;j<nWidth;j++)
		{
			videoByte[i*lLineBytes+j*3+0]=pCamBuf[(nHeight-1-i)*nWidth*3+j*3+0];
			videoByte[i*lLineBytes+j*3+1]=pCamBuf[(nHeight-1-i)*nWidth*3+j*3+1];
			videoByte[i*lLineBytes+j*3+2]=pCamBuf[(nHeight-1-i)*nWidth*3+j*3+2];
		}
	}
	DrawBmpBuf(bih,videoByte,picVideo.m_hWnd,TRUE,FALSE);
	delete []videoByte;
	delete []pCamBuf;
}


void ResultDlg::DrawMatch() {
	CRect rect;
	CString text;
	CFont font;
	CFont numFont;
	text.Format("%d", similarity);
	font.CreatePointFont(200,_T("微软雅黑"),NULL);
	COLORREF white = RGB(255,255,255);
	COLORREF red = RGB(255,0,0);

	lblScore.GetClientRect(&rect);
	CPaintDC dcLabel(&lblScore);
	dcLabel.SelectObject(font);
	dcLabel.SetTextColor(white);
	dcLabel.SetBkMode(TRANSPARENT);

	dcLabel.DrawText("相似度",&rect,DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	rect.left+= 105;
	dcLabel.SetTextColor(red);
	dcLabel.DrawText(text,&rect,DT_LEFT | DT_VCENTER | DT_SINGLELINE);
}

void ResultDlg::DrawLabel(CStatic* label, CFont* font, COLORREF color, int prop) {
	CRect rect;
	CString text;

	label->GetWindowTextA(text);
	label->GetClientRect(&rect);
	CPaintDC dcLabel(label);
	dcLabel.SelectObject(font);
	dcLabel.SetTextColor(color);
	dcLabel.SetBkMode(TRANSPARENT);
	dcLabel.DrawText(text,&rect, prop);
}


UINT ResultDlg::ExitThreadProc(LPVOID lpVoid) {

	ResultDlg* pDlg=(ResultDlg*)lpVoid;
	pDlg->Exit();
	return 1;
}

void ResultDlg::Exit() {
	Sleep(sleepSeconds);
	if (exited) return;
	exited = TRUE;
	PostMessage(WM_CLOSE, NULL, NULL);
}