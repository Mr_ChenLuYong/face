#pragma once


// IdlessDlg 对话框

class IdlessDlg : public CDialog
{
	DECLARE_DYNAMIC(IdlessDlg)

public:
	IdlessDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~IdlessDlg();

// 对话框数据
	enum { IDD = IDD_IDLESSDLG };

protected:
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	
	HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	// 拖动窗口
	afx_msg LRESULT OnNcHitTest(CPoint point);
	// 消息处理
	BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()


	
	afx_msg void OnStnClickedClose();
	afx_msg void OnStnClickedConfirm();

private:
	int cx, cy;
	CDialog* mainDlg;
	

	HBITMAP hClose;
	HBITMAP hConfirm;

	
	CStatic btnClose;
	CStatic btnConfirm;
	CStatic lblTip;

	CFont fntElement;
	CEdit edtElement;

	// 背景图片
	CBitmap cbmpBackgound;
	// 背景刷
	CBrush brsBackground;



public:
	void setDialog(CDialog* dlg);

	void Show(bool ifShow);
};
