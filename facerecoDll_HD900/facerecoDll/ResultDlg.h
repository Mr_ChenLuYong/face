#pragma once

#include "Resource.h"

// ResultDlg 对话框

class ResultDlg : public CDialog
{
	DECLARE_DYNAMIC(ResultDlg)

public:
	ResultDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~ResultDlg();

// 对话框数据
	enum { IDD = IDD_RESULTDLG };

protected:

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	
	// 审核通过
	afx_msg void OnStnClickedConfirm();
	// 审核否决
	afx_msg void OnStnClickedRefuse();
	// 知道了
	afx_msg void OnStnClickedKnow();

// 变量
private:
	// 是否全屏
	int screen;
	// 识别结果
	int scoreLevel;
	// 返回结果
	int response;
	// 字体
	CFont operFont;
	// 标题字体
	CFont titleFont;
	BOOL exited;

	int sleepSeconds;


	// 相似度
	int similarity;

	// 证件照
	CString idFile;
	// 视频截图
	CString videoFile;
	// 头像
	CString avatarFile;

	CBitmap idImg;
	CBitmap avatarImg;
	CBitmap videoImg;
// 控件
private:

	// 中间提示图标
	CStatic btnIcon;
	// 审核通过
	CStatic btnConfirm;
	// 审核失败
	CStatic btnRefuse;
	// 知道了 按钮
	CStatic btnKnow;


	// Logo
	CStatic picLogo;
	// 标题
	CStatic picTitle;
	// 视频图像
	CStatic picVideo;
	// 证件照
	CStatic picIdCard;
	// 人脸图片
	CStatic picAvatar;

	// 提示语句
	CStatic lblTip;
	// 识别比值
	CStatic lblScore;
	// 相似度
	CStatic lblSimilarity;
	// 百分号
	CStatic lblPercent;


private:
	void DrawVideo();
	void DrawMatch();
	// 绘制标签
	void DrawLabel(CStatic* label, CFont* font, COLORREF color, int prop);

	
	// 退出
	void Exit();
	// 自动退出线程
	static UINT ExitThreadProc(LPVOID lpVoid);

public:
	

	void Init(BOOL byHand, int score, CString id, CString avatar, CString video, int fullScreen);
	int Response();

};
