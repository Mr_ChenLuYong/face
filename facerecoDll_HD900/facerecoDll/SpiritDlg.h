#pragma once


// SpiritDlg 对话框

class SpiritDlg : public CDialog
{
	DECLARE_DYNAMIC(SpiritDlg)

public:
	SpiritDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~SpiritDlg();

// 对话框数据
	enum { IDD = IDD_SPIRITDLG };

protected:
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	// 拖动窗口
	afx_msg LRESULT OnNcHitTest(CPoint point);
	// 消息处理
	BOOL PreTranslateMessage(MSG* pMsg);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

private:
	CDialog* mainDlg;
	CStatic picBackground;
	int initType;
	int cx, cy;
	bool idless;


	HBITMAP hRun;
	HBITMAP hScan;
	HBITMAP hLeave;

	HBITMAP hRunIdless;
	HBITMAP hScanIdless;
	HBITMAP hLeaveIdless;


	HBITMAP hIdless;

	CStatic btnIdless;


	afx_msg void OnStnClickedBackground();
	afx_msg void OnStnClickedIdless();
	afx_msg LRESULT OnNotifyIcon(WPARAM wParam,LPARAM IParam);

public:
	void activeIdless();
	void setState(int state);
	void setDialog(CDialog* dlg, int type);
};
