// StateDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "facerecoDll.h"
#include "StateDlg.h"


// StateDlg 对话框

IMPLEMENT_DYNAMIC(StateDlg, CDialog)

StateDlg::StateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(StateDlg::IDD, pParent)
{

}

StateDlg::~StateDlg()
{
}

void StateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(StateDlg, CDialog)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// StateDlg 消息处理程序


BOOL StateDlg::OnInitDialog() {
	CDialog::OnInitDialog();
	
	cx = GetSystemMetrics(SM_CXFULLSCREEN);   
	cy = GetSystemMetrics(SM_CYFULLSCREEN);


	// 窗口置顶
	SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);


	hSuccess = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWSUCCESS));
	hFailer = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWFAILER));

	hSuccessIdless = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWSUCCESSIDLESS));
	hFailerIdless = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWFAILERIDLESS));

	int width = 220;
	int height = 30;
	if (idless) {
		width = 270;
	}

	MoveWindow(cx-width - 20, cy-height-50, width, height);

	// 创建背景图片
	picBackground.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(0, 0, width, height),this, 1801);
	picBackground.ShowWindow(true);

	if (idless) {
		picBackground.SetBitmap(hSuccessIdless);
	} else {
		picBackground.SetBitmap(hSuccess);
	}

	lblMsg.Create(_T("年年年年年年年年"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(35, 7, 265, 24), this, 1010);
	lblMsg.ShowWindow(true);

	return 1;
}

HBRUSH StateDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID()==1010) {
		CFont font;
        font.CreatePointFont(100,"微软雅黑");
        pDC->SelectObject(&font);//设置字体
		pDC->SetBkMode(TRANSPARENT);

		return (HBRUSH)GetStockObject(NULL_BRUSH);
	}
  
    return hbr;
}

void StateDlg::setState(char *msg, bool state) {
	int width = 220;
	int height = 30;
	if (idless) {
		width = 270;
	}

	MoveWindow(cx-width - 20, cy-height-50, width, height);
	picBackground.MoveWindow(CRect(0, 0, width, height), true);

	if (idless) {
		if (state == true) {
			picBackground.SetBitmap(hSuccessIdless);
		} else {
			picBackground.SetBitmap(hFailerIdless);
		}
	} else {
		if (state == true) {
			picBackground.SetBitmap(hSuccess);
		} else {
			picBackground.SetBitmap(hFailer);
		}	
	}

	lblMsg.SetWindowTextA(msg);
}



void StateDlg::activeIdless() {
	idless = true;
}