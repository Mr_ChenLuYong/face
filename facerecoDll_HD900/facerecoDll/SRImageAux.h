#ifndef __FACERECO_SRIMAGEAUX_H__
#define __FACERECO_SRIMAGEAUX_H__

#include <opencv\cv.h>
#include <opencv\highgui.h>
#include <opencv2\opencv.hpp>

namespace eshanren {

int pasteTransparencyImage(cv::Mat& _dest, const cv::Mat& _transparency,
	 float _transparence = 1, COLORREF _transparentColor = RGB(255, 255, 255));

}



#endif // !__FACERECO_SRIMAGEAUX_H__