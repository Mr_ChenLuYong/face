#pragma once


// StateDlg 对话框

class StateDlg : public CDialog
{
	DECLARE_DYNAMIC(StateDlg)

public:
	StateDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~StateDlg();

// 对话框数据
	enum { IDD = IDD_STATEDLG };


protected:
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()


private:
	int cx, cy;
	CStatic lblMsg;
	CStatic picBackground;

	HBITMAP hSuccess;
	HBITMAP hFailer;

	HBITMAP hSuccessIdless;
	HBITMAP hFailerIdless;

	int idless;

public:
	void activeIdless();
	void setState(char *msg, bool state);
};
