// facerecoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "resource.h"
#include "hoseDlg.h"
#include "facerecoDlg.h"
#include "ReadFilter.h"
#include "DMonitor.h"

#include <SRFacerecoCv.h>
#pragma comment(lib, "SRFacerecoCv.lib")


#include "./lib/include/sdtapi.h"
#pragma comment(lib,"./lib/lib/sdtapi.lib")
//#include "SRTest.h"

#include <mmsystem.h>
#pragma comment(lib,"winmm.lib")


#include <DbgHelp.h>
#pragma comment (lib, "DbgHelp.lib")
// 为了程序的简洁和集中关注关心的东西，此函数忽略错误检查,请注意后期优化
LONG WINAPI ApplicationCrashHandler(struct _EXCEPTION_POINTERS* ExceptionInfo)
{
    /*
      ***保存数据代码***
    */

    // 创建Dump文件  
    //  
    HANDLE hDumpFile = CreateFile("senken.dmp", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);  
  
    // Dump信息  
    //  
    MINIDUMP_EXCEPTION_INFORMATION dumpInfo;  
    dumpInfo.ExceptionPointers = ExceptionInfo;  
    dumpInfo.ThreadId = GetCurrentThreadId();  
    dumpInfo.ClientPointers = TRUE;  
  
    // 写入Dump文件内容  
    //  
    MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hDumpFile, MiniDumpNormal, &dumpInfo, NULL, NULL);  
  
    CloseHandle(hDumpFile); 

	return EXCEPTION_EXECUTE_HANDLER;
}

// 锁
CRITICAL_SECTION g_CriticalSection;
class MyLock
{
public:	
	MyLock(){EnterCriticalSection(&g_CriticalSection);};
	~MyLock(){LeaveCriticalSection(&g_CriticalSection);};
};

LRESULT CALLBACK KeybdProc(int nCode, WPARAM wParam, LPARAM lParam) {

	if (nCode >= 0) {

		switch (wParam) {
			case WM_KEYDOWN:
				{
					PKBDLLHOOKSTRUCT pVirKey = (PKBDLLHOOKSTRUCT)lParam;
					CString msg;
					if (pVirKey->vkCode == VK_F4) {
						pFacerecoDlg->StartDetect();
						return 1;
					}
					break;
				}
		}
	}

	//将消息向下一个钩子传递
     return CallNextHookEx(NULL, nCode, wParam, lParam);
}


// facerecoDlg 对话框

CfacerecoDlg::CfacerecoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CfacerecoDlg::IDD, pParent)
{
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	
    //注册异常捕获函数
    SetUnhandledExceptionFilter(
     (LPTOP_LEVEL_EXCEPTION_FILTER)ApplicationCrashHandler); 

	// 窗口置顶
	//SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
}

void CfacerecoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

#define WM_NC (WM_USER+1001)
#define WM_UPDATE (WM_USER+1011)

BEGIN_MESSAGE_MAP(CfacerecoDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_NC,&CfacerecoDlg::OnNotifyIcon)
	ON_MESSAGE(WM_UPDATE,&CfacerecoDlg::OnUpdate)
	//}}AFX_MSG_MAP

	ON_WM_NCHITTEST()
	ON_STN_CLICKED(1013, &CfacerecoDlg::OnStnClickedSubmit)
	ON_STN_CLICKED(1111, &CfacerecoDlg::OnStnClickedMinimize)
	ON_STN_CLICKED(1019, &CfacerecoDlg::OnStnClickedMan)
	ON_STN_CLICKED(1020, &CfacerecoDlg::OnStnClickedGirl)
	ON_STN_CLICKED(1025, &CfacerecoDlg::OnStnClickedConfirm)
	ON_STN_CLICKED(1026, &CfacerecoDlg::OnStnClickedRefuse)
	ON_STN_CLICKED(1027, &CfacerecoDlg::OnStnClickedVersion)
	ON_STN_CLICKED(1029, &CfacerecoDlg::OnStnClickedClose)

	ON_CBN_SELCHANGE(13001, &CfacerecoDlg::OnCbnSelchangeCombol)

END_MESSAGE_MAP()

// facerecoDlg 消息处理程序

BOOL CfacerecoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetActiveWindow();
	// 窗口置顶
	this->SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);
	
	
	cx = GetSystemMetrics(SM_CXFULLSCREEN);   
	cy = GetSystemMetrics(SM_CYFULLSCREEN);
	
	InitPublic();


	SetWindowText("人像识别程序");

	// TODO: 在此添加额外的初始化代码
	HANDLE hMutex=::CreateMutex(NULL,true,"facereco.dll");
	if (hMutex == NULL) {
		CloseHandle(hMutex);
		Exit();
		return true;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		CloseHandle(hMutex);
		Exit();
		return true;
	}

	// 获取项目所在路径
	char pModuleName[256];
	::GetModuleFileName(NULL,pModuleName,256);
	CString strTemp;
	strTemp=pModuleName; 
	int pos=strTemp.ReverseFind('\\');
	appPath=strTemp.Left(pos);

	CTime temp;
	temp = CTime::GetCurrentTime();
	CString timeFormat = temp.Format("\\face_%Y%m%d.log");

	// 创建暂存图片目录
	::CreateDirectory(appPath+"\\storage",NULL);

	config = appPath + "\\config.ini";
	readFilterPath = appPath + "\\ReadFilter.sys";
	
	storagePath = appPath + "\\storage";
	fullFile = storagePath+"\\full.jpg";
	avatarFile = storagePath+"\\avatar.jpg";
	idFile = storagePath+"\\photo.bmp";
	handFaceFile = storagePath + "\\handface.jpg";
	storeConfig = storagePath + "\\config.ini";
	logFile = storagePath + timeFormat;
	settingFile = storagePath + "\\setting.ini";
	hosterInfoFile = storagePath + "\\hoster.dat";
	idDataFile = storagePath + "\\id.dat";
	WriteLog("----------------------------------------", true);

	//  build storage config
	fstream file;
	file.open(storeConfig, ios::in);
	if (!file) {
		file.close();
		
		ofstream outfile(storeConfig);
		if (outfile) {
			outfile << "[config]" << endl;
			outfile << "token=" << endl;
		}
		outfile.close();
	} else {
		file.close();
	}

	exeWltToBmp.Format("%s\\RfdGetBmp.exe \"%s\"", appPath.GetBuffer(), "c:");

	// 加载配置
	LoadConfig();

	// 2017年10月20日 动车站版修改
	//sourceUrl.Format("%s", "http://facereco.eshanren.com/api/upload");
	// @else: 动车站局域网
	CString temp_ip;
	CString temp_port;
#ifdef _DEBUG
	GetPrivateProfileString("NET", "ip", "61.153.14.116", temp_ip.GetBuffer(30), 30, config);
	GetPrivateProfileString("NET", "port", "8089", temp_port.GetBuffer(10), 10, config);
#else
	GetPrivateProfileString("NET", "ip", "41.215.100.46", temp_ip.GetBuffer(30), 30, config);
	GetPrivateProfileString("NET", "port", "8089", temp_port.GetBuffer(10), 10, config);
#endif
	sourceUrl.Format("http://%s:%s/api/upload", temp_ip,temp_port);
	temp_ip.ReleaseBuffer();
	temp_port.ReleaseBuffer();
	// endif

	WriteLog("Init variable success...");

	int handWindowWidth = 0;
	int handWindowHeight = 0;
	if (initType == 1 || initType == 2) {
		handWindowWidth = 324;
	}

	int posX = 0;
	int posY = 0;
	windowRect_ = CSize(1024,768);

	if (cx > windowRect_.cx) {
		posX = (cx - windowRect_.cx) / 2;
	}
	if (cy > windowRect_.cy) {
	    posY = (cy - windowRect_.cy) / 2;
	}

	//if (cx != 1920 && cy != 1080 )
		MoveWindow(posX,posY, windowRect_.cx, windowRect_.cy);
	//else 
		//ShowWindow(SW_SHOWMAXIMIZED);

	notifyIcon.uID = IDI_ICON1;
	notifyIcon.cbSize=sizeof(NOTIFYICONDATA);
    notifyIcon.hIcon=AfxGetApp()->LoadIcon(IDI_ICON1);
    notifyIcon.hWnd=m_hWnd;
    lstrcpy(notifyIcon.szTip,_T("Facereco"));
    notifyIcon.uCallbackMessage=WM_NC;
    notifyIcon.uFlags=NIF_ICON | NIF_MESSAGE | NIF_TIP;
    Shell_NotifyIcon(NIM_ADD,&notifyIcon);

	
	apiDll = LoadLibrary("facerecoAPI.dll");
	if (apiDll == NULL) {
		Message("缺少组件facerecoAPI.dll");
		Exit();
		return false;
	}

	aboutDll = LoadLibrary("about.dll");
	if (aboutDll == NULL) {
		Message("缺少组件about.dll");
		Exit();
		return false;
	}

	if (initType == 2) {
		visitorDll = LoadLibrary("visitor.dll");
		if (visitorDll == NULL) {
			Message("缺少组件visitor.dll");
			Exit();
			return false;
		}

		apiThirdUpload = (APIThirdUpload)GetProcAddress(apiDll, "thirdUpload");

		apiVisitor = (APIVisitor)GetProcAddress(visitorDll, "visit");

	}

	// 获取接口 md5 方法
	apiMd5 = (APIMD5)GetProcAddress(apiDll, "md5");
	//// 获取接口 threshold 方法
	//apiThreshold = (APIThreshold)GetProcAddress(apiDll, "threshold");
	// 获取接口 hotelHand 方法
	apiHotelHand = (APIHotelHand)GetProcAddress(apiDll, "hotelHand");
	// 获取接口 netbarHand 方法
	apiNetBarHand = (APINetBarHand)GetProcAddress(apiDll, "netBarHand");
	// 获取接口 uploadInfo 方法
	apiUploadInfo = (APIUploadInfo)GetProcAddress(apiDll, "uploadInfo");
	// 获取接口 checkVersion 方法
	apiCheckVersion = (APICheckVersion)GetProcAddress(apiDll, "checkVersion");
	// 获取接口 gasDoSimilarity 方法
	apiGasDoSimilarity = (APIGasDoSimilarity)GetProcAddress(apiDll, "gasDoSimilarity");
	// 获取接口 getSimilarityVal 方法
	apiGetSimilarityVal = (APIGetSimilarityVal)GetProcAddress(apiDll, "getSimilarityVal");
	// 获取接口 doResultSure 方法
	apiDoResultSure = (APIDoResultSure)GetProcAddress(apiDll, "doResultSure");
	// 获取接口 activeUpload 方法
	apiActiveUpload = (APIActiveUpload)GetProcAddress(apiDll, "activeUpload");
	// 获取接口 activeHeartbeat 方法
	apiActiveHeartbeat = (APIActiveHeartbeat)GetProcAddress(apiDll, "activeHeartbeat");
	// 获取接口 activeHeartbeat 方法
	apiActiveCheckVersion = (APIActiveCheckVersion)GetProcAddress(apiDll, "activeCheckVersion");
	// 获取接口 activeDownloadFile 方法
	apiActiveDownloadFile = (APIActiveDownloadFile)GetProcAddress(apiDll, "activeDownloadFile");
	// 获取接口 activeThreshold 方法
	apiActiveThreshold = (APIActiveThreshold)GetProcAddress(apiDll, "activeThreshold");
	// 获取接口 activeSource 方法
	apiActiveSource = (APIActiveSource)GetProcAddress(apiDll, "activeSource");
	// 获取接口 activeFunction 方法
	apiActiveFunction = (APIActiveFunction)GetProcAddress(apiDll, "activeCheckFunction");
	

	// 获取接口 thirdHosterInfo 方法
	apiThirdHosterInfo = (APIThirdHosterInfo)GetProcAddress(apiDll, "thirdHosterInfo");




	// 获取无证上传接口
	apiActiveUploadIdless = (APIActiveUploadIdless)GetProcAddress(apiDll, "activeUploadIdless");
	// 获取无证信息
	apiActiveQueryIdless = (APIActiveQueryIdless)GetProcAddress(apiDll, "activeQueryIdless");


	// 动车站版本 2017年10月20日
	// 获取接口 verifyTrainInfo 方法
	apiVerifyTrainInfo = (APIVerifyTrainInfo)GetProcAddress(apiDll, "verifyTrainInfo");


	CString base_url,verify_url;
	base_url.Format("http://%s:%s/api/",temp_ip, temp_port);
	verifyUrl_.Format("%splaceverif",base_url);

#ifdef _DEBUG
	GetPrivateProfileString("NET","place","11111", placeCode_.GetBuffer(33), 33,config);
	GetPrivateProfileString("NET","verify","586365", placeCode_.GetBuffer(33), 33,config);
#else
	GetPrivateProfileString("NET", "place", "", placeCode_.GetBuffer(30), 30, config);
	GetPrivateProfileString("NET", "verify", "", verifyCode_.GetBuffer(10), 10, config);
#endif
	

	APISetBaseApiUrl temp_setapiurl_func = (APISetBaseApiUrl)GetProcAddress(apiDll, "setBaseApiUrl");
	if (temp_setapiurl_func) {
		temp_setapiurl_func(base_url);
	}
		
	
	// end 动车站版本
	
		




	// 获取 aboutUs 方法
	apiAboutUs = (APIAboutUs)GetProcAddress(aboutDll, "aboutUs");


	WriteLog("Init component success...");


	hVideo = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_VIDEO));
	hIdCard = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDCARD));
	hClose = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWX));
	hPass = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BANDNEWPASS));
	hRefuse = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWREFUSE));
	hIconPass = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWICONPASS));
	hIconRefuse = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWICONREFUSE));
	hSubmit = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_SUBMIT));
	hManCheck = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MANCHECK));
	hGirlCheck = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GIRLCHECK));
	hManUncheck = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MANUNCHECK));
	hGirlUncheck = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GIRLUNCHECK));
	hValid = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWVALID));
	hInvalid = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWINVALID));
	hMinimize_ = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MINIMIZE));

	spiritDlg = new SpiritDlg();
	spiritDlg->setDialog(this, initType);
	spiritDlg->Create(IDD_SPIRITDLG, this);
	spiritDlg->ShowWindow(false);

	stateDlg = new StateDlg();
	stateDlg->Create(IDD_STATEDLG,this);
	stateDlg->ShowWindow(false);



	idlessDlg = new IdlessDlg();
	idlessDlg->setDialog(this);
	idlessDlg->Create(IDD_IDLESSDLG, this);
	idlessDlg->ShowWindow(false);

	int labelX = 0;
	int labelY = 30;
	// 创建控件
	// 视频显示框
	//picVideo.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(labelX,labelY,windowRect_.cx - labelX,windowRect_.cy - 20),this, 1000);
	picVideo.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(0,0,windowRect_.cx,windowRect_.cy),this, 1000);
	picVideo.SetBitmap(hVideo);
	picVideo.ShowWindow(true);

	// 证件人像框
	labelX = 830;	// 795
	labelY = 100;
	picIDCard.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(windowRect_.cx - 218 - 20,labelY,windowRect_.cx - 20,labelY+270),this, 1001);
	picIDCard.SetBitmap(hIdCard);
	picIDCard.ShowWindow(false);
	//picIDCard.SetWindowPos(0,
	

	labelX = 830;	// 795
	labelY = 400;
	picSnapshoot_.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(windowRect_.cx - 218 - 20,labelY,windowRect_.cx - 20,labelY+270),this, 1002);
	picSnapshoot_.SetBitmap(hIdCard);
	picSnapshoot_.ShowWindow(false);


	labelX = 120;	// 795
	labelY = 10;
	lblCammerTip.Create(_T("摄像头编号:"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 100,labelY+28), this, 1115);
	lblCammerTip.ShowWindow(false);

	labelX=925 - 85;
	labelY= 7;
	cboCapture = (CComboBox*)GetDlgItem(IDC_CBOCAPTURE);
	cboCapture->MoveWindow(CRect(labelX, labelY, labelX + 80, labelY + 20), true);
	cboCapture->AddString("1");
	cboCapture->AddString("2");
	cboCapture->AddString("3");
	cboCapture->AddString("4");
	cboCapture->AddString("5");
	cboCapture->SetCurSel(pCapParam.lIndex);
	cboCapture->ShowWindow(true);

	labelX=925;
	labelY=0;
	btnMinimize.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(labelX,labelY,labelX+49,labelY+36),this, 1111);
	btnMinimize.SetBitmap(hMinimize_);
	btnMinimize.ShowWindow(false);

	labelX=925 + 49;
	btnClose.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(labelX,labelY,labelX+49,labelY+36),this, 1029);
	btnClose.SetBitmap(hClose);
	btnClose.ShowWindow(false);


	CString temp_title,temp_title_type;
	GetPrivateProfileString("APP", "title","人证核验",temp_title.GetBuffer(50),50,settingFile);
	GetPrivateProfileString("APP", "title_type","动车站版",temp_title_type.GetBuffer(50),50,settingFile);
	labelX = 125;
	labelY = 20;
	// 标题标签
	labTitle_.Create(temp_title, WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+40), this, 1305);
	labTitle_.ShowWindow(false);

	labelX = 140;
	labelY = 60;
	labTitleType_.Create(temp_title_type, WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+40), this, 1306);
	labTitleType_.ShowWindow(false);
 
	labelX = 800;
	labelY = 690;
	labCopyRight1_.Create("浙江立地信息科技有限公司", WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+40), this, 1307);
	labCopyRight1_.ShowWindow(false);
 
	labelX = 800;
	labelY = 720;
	labCopyRight2_.Create("温州山人网络科技有限公司", WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+40), this, 1308);
	labCopyRight2_.ShowWindow(false);


	labelX = 55;
	labelY = 50;
	// 状态标签
	lblState.Create(_T("正在初始化, 请勿进行任何操作..."), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 550,labelY+40), this, 1005);
	lblState.ShowWindow(false);

    // 姓名标签
	labelX = 52;
	labelY = 120;
	lblName.Create(_T("姓名"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+15), this, 1005);
	lblName.ShowWindow(false);

	// 性别标签
	labelY+=30;
	lblGender.Create(_T("性别"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+15), this, 1009);
	lblGender.ShowWindow(false);

	// 生日标签
	labelY+=30;
	lblBirthday.Create(_T("出生日期"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+15), this, 1008);
	lblBirthday.ShowWindow(false);

	// 地址标签
	labelY+=30;
	lblAddress.Create(_T("地址"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+45), this, 1007);
	lblAddress.ShowWindow(false);
	
	// 身份证标签
	labelY+=50;
	lblIDCode.Create(_T("身份证号"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+15), this, 1006);
	lblIDCode.ShowWindow(false);

	labelX=102;
	labelY=483;
	// 签证机关
	lblOrgn.Create(_T("签证机关"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+15), this, 1060);
	lblOrgn.ShowWindow(false);

	// 有效期
	labelY+=25;
	lblExpire.Create(_T("有效期"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(labelX,labelY,labelX + 200,labelY+15), this, 1061);
	lblExpire.ShowWindow(false);

	// 证件日期状态
	picIDState.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(298,506,343,524),this, 1066);
	picIDState.ShowWindow(false);

	// 人像比对结果
	labelX=730;
	labelY=515;
	lblScore.Create(_T("0%%"), WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(labelX,labelY,labelX+80,labelY+25), this, 1023);
	lblScore.ShowWindow(false);
	//lblScore.SetFont(&font); 

	lblPercent.Create(_T("%"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(1460,700,1530,750), this, 1123);
	lblPercent.ShowWindow(false);

	// 相似度标签
	lblMatch.Create(_T("相似度"), WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(475,673,553,703), this, 1024);
	lblMatch.ShowWindow(false);

	//// 状态提示
	//lblTip.Create(_T("相似度"), WS_CHILD|WS_VISIBLE|SS_CENTER, CRect(473,623,558,683), this, 1031);
	//lblTip.ShowWindow(true);

	// 人工审核通过
	labelX=101;
	labelY=608;
	btnConfirm.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(labelX,labelY,labelX+254,labelY+84),this, 1025);
	btnConfirm.SetBitmap(hPass);
	btnConfirm.ShowWindow(false);

	// 人工审核否决
	labelX=684;
	btnRefuse.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(labelX,labelY,labelX+254,labelY+84),this, 1026);
	btnRefuse.SetBitmap(hRefuse);
	btnRefuse.ShowWindow(false);

	// 通过图标
	labelX = 546;
	labelY = 527;
	picIconPass.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(labelX,labelY,labelX+117,labelY+40),this, 1064);
	picIconPass.SetBitmap(hIconPass);
	picIconPass.ShowWindow(false);

	// 否决图标
	labelX = 873;
	labelY = 527;
	picIconRefuse.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE,CRect(labelX,labelY,labelX+117,labelY+40),this, 1065);
	picIconRefuse.SetBitmap(hIconRefuse);
	picIconRefuse.ShowWindow(false);

	Clear();

	CString typeName;
	if (initType == 1) 
		typeName = "动车版";
	else if (initType == 2)
		typeName = "访客版";
	lblType.Create(typeName, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(860 - handWindowWidth, 537 - handWindowHeight, 942 - handWindowWidth, 557 - handWindowHeight),this, 1028);
	lblType.ShowWindow(false);

	if (initType == 1 || initType == 2) {
		menuTray.LoadMenu(IDR_MENUNOTIFY);
	}
	//if (fullScreen == 0) {
	//	menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_UNCHECKED);
	//} else {
	//	menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_CHECKED);
	//}

	WriteLog("Init control success...");


	cbmpBackgound.LoadBitmap(IDB_BRANDNEWBG);
	brsBackground.CreatePatternBrush(&cbmpBackgound);  
	cbmpBackgound.GetBitmap(&bmpBackground);

	// 初始化锁
	InitializeCriticalSection(&g_CriticalSection);

	// 初始化数据
	cmdExit = false;
	genderMan = true;
	validByHand = false;
	videoDetect = false;
	doGetImage = false;
	ifGetImage = false;
	doReading = true;
	doListening = true;
	doReceiving = false;
	doComparing = false;
	doDetecting = true;
	pauseVideo = false;
	doFilting = true;
	doHeartbeat = true;
	pauzeReading = false;
	pauzeDetecting = true;
	initFilter = false;
	initing = true;
	pauzeFilting = true;
	isTimeout = false;
	ifIdless = false;
	isIdlessCompare = false;
	faceWidthConst = 102;
	faceHeightConst = 128;
	cardReaderPort = 1001;
	maxScore = 0;
	matchCount = 0;
	maxMatchCountConst = 2;
	scoreThreshold = 55;
	scoreThresholdHigh = 65;
	tokenExpireTime = 0;
	featureSize = EF_Size();
	idFeature = new BYTE[featureSize];
	//appkey = "4adfd670ef2cbc72b9a3aceacb73c1c8";
	//appsecret = "5fefb260863ad70bfcf142bb84c4c8f6";

	// 提示语音
	remindOnOff_ = 0;
	remindSpeed_ = 9;

	AfxBeginThread(InitThreadProc, this);

	// 初始化我的资源
	initClyRes();

#if 0 // 版本验证
	CheckVersion();
#endif
	UpdateWindow();
	
	return true;  // 除非将焦点设置到控件，否则返回 true
}


// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CfacerecoDlg::OnPaint()
{
	if (IsIconic())
	{
	}
	else
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		//CRect winRect;
		//GetClientRect(&winRect);
		//CDC dcMem;
		//dcMem.CreateCompatibleDC(&dc);

		//CBitmap* pbmpOld = dcMem.SelectObject(&cbmpBackgound); 
		//dc.StretchBlt(0,0,winRect.Width(),winRect.Height(),&dcMem,0,0,bmpBackground.bmWidth,bmpBackground.bmHeight,SRCCOPY);

		if (initing) {
			CFont fotTip;
			fotTip.CreatePointFont(150,_T("微软雅黑"),NULL);
			DrawLabel(&lblState, &fotTip, DT_LEFT, RGB(255,0,0));
		} else {
			CFont fotTip;
			fotTip.CreatePointFont(150,_T("微软雅黑"),NULL);
			DrawLabel(&lblState, &fotTip, DT_LEFT, RGB(255,0,0));
		}

		
		CFont normal;
		normal.CreatePointFont(80,_T("微软雅黑"),NULL);
		//DrawLabel(&lblName, &normal, DT_LEFT);
		DrawLabel(&lblGender, &normal, DT_LEFT);
		DrawLabel(&lblBirthday, &normal, DT_LEFT);
		DrawLabel(&lblIDCode, &normal, DT_LEFT);
		DrawLabel(&lblOrgn, &normal, DT_LEFT);
		DrawLabel(&lblExpire, &normal, DT_LEFT);
		
		DrawAddress();
		DrawMatch();


		DrawLabel(&lblCammerTip, &normal, DT_RIGHT, RGB(255,255,255));

		CFont title_font;
		title_font.CreatePointFont(225,_T("微软雅黑"),NULL);

		DrawLabel(&labTitle_, &title_font, DT_LEFT, RGB(255,255,255));
		CFont title_type_font;
		title_type_font.CreatePointFont(150,_T("微软雅黑"),NULL);
		DrawLabel(&labTitleType_, &title_type_font, DT_LEFT, RGB(255,255,255));

		CFont title_type_font1;
		title_type_font1.CreatePointFont(120,_T("微软雅黑"),NULL);
		DrawLabel(&labCopyRight1_, &title_type_font1, DT_LEFT, RGB(255,255,255));
		DrawLabel(&labCopyRight2_, &title_type_font1, DT_LEFT, RGB(255,255,255));

		

		//DrawVersion();

		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CfacerecoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

LRESULT CfacerecoDlg::OnNcHitTest(CPoint point) {
	//@动车站1024*600屏幕全屏
	int cx = GetSystemMetrics(SM_CXSCREEN); 
	int cy = GetSystemMetrics(SM_CYSCREEN);
	if (cx == 1024 && cy == 600)
		ShowWindow(SW_SHOWMAXIMIZED);
	// end 171024
	return HTCAPTION;
}

BOOL CfacerecoDlg::PreTranslateMessage(MSG* pMsg) {
	if(pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
			case VK_RETURN:    // 屏蔽回车
				return true;
			case VK_ESCAPE:
				return true;
		}
	}
	else if (pMsg->message == WM_COMMAND)
	{
		return true;
	} else if (pMsg->message == WM_SYSCOMMAND) {
		::ShowWindow(this->m_hWnd, SW_SHOW);
		pauzeReading = false;
	}
   
    return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH CfacerecoDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	CFont normal;
	normal.CreatePointFont(80,_T("微软雅黑"),NULL);

	int ID=pWnd->GetDlgCtrlID();
	if (ID == 1005) {
		//pDC->SelectObject(normal);
		pDC->SetBkMode(TRANSPARENT); //设置标签背景透明
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	} else if (ID == 13001) {
		pDC->SetBkColor(RGB(255,255,255));
		pDC->SetTextColor(RGB(0,0,0));
		return hbr;
	} else if (ID == 1023) {
        //pDC->SetBkMode(TRANSPARENT);
		CFont font;  
		font.CreateFont(30,0,0,0,400,0,0,0,  
			ANSI_CHARSET,OUT_DEFAULT_PRECIS,  
			CLIP_DEFAULT_PRECIS,  
			DEFAULT_QUALITY,  
			DEFAULT_PITCH|FF_DONTCARE,  
			"微软雅黑");
		pDC->SelectObject(font);
        pDC->SetTextColor(RGB(250,250,250));
        pDC->SetBkColor(RGB(0,0,0));
		return (HBRUSH) ::GetStockObject(NULL_BRUSH);;
	}
	if (ID == 0) {
		return (HBRUSH)brsBackground;  
	}
  
	return hbr;
}

LRESULT CfacerecoDlg::OnNotifyIcon(WPARAM wParam,LPARAM IParam) {
	if (wParam == IDI_ICON1) {
		UINT uMsg = (UINT)IParam;


		switch(uMsg) {
			case WM_RBUTTONUP:
			{
				CPoint point;
				int id;
				GetCursorPos(&point);

				id = menuTray.GetSubMenu(0)->TrackPopupMenu(TPM_RETURNCMD | TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this);
				if (id == ID_UPDATE) {
					apiAboutUs(appkey.GetBuffer(), appsecret.GetBuffer(), identity.GetBuffer(), false);
				/*	char newVersion[20];
					char urls[800];
					int iRet;
					char newVer[20];
					CString msg;
					INT_PTR nRet;

					iRet = apiActiveCheckVersion(appkey, appsecret, version.GetBuffer(), newVer);
					if (iRet == 2) {
						msg.Format("当前版本%s, 最新版本%s, 是否更新?(更新完毕后将自动重启)", version, newVer);
						nRet = ::MessageBox(this->m_hWnd, msg, "提示", MB_OKCANCEL);
						if (nRet == IDOK) {
							if (doComparing) {
								Message("正在识别比对中, 请稍后再试");
								return 1;
							}

							Update(newVer);
						}
					} else if (iRet == 1) {
						msg.Format("当前版本%s, 已是最新版", version);
						Message(msg.GetBuffer());
					} else if (iRet == 0) {
						msg.Format("检查更新失败, 请确保网络已连接");
						Message(msg.GetBuffer());
					}*/

				} else if (id == ID_RESET) {
					if (!doComparing) {
						Exit();
					}
				} else if (id == ID_EXIT) {
					if (!doComparing) {
						SendProtect("cmd:exit");
						Sleep(1000);
						Exit();
					}
					else
						cmdExit = true;
				} else if (id == ID_FULLSCREEN) {
					if (fullScreen == 1) {
						fullScreen = 0;
						WritePrivateProfileString("config","fullscreen", "0", storeConfig);
						menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_UNCHECKED);
					} else {
						fullScreen = 1;
						WritePrivateProfileString("config","fullscreen", "1", storeConfig);
						menuTray.GetSubMenu(0)->CheckMenuItem(0,MF_BYCOMMAND |MF_BYPOSITION| MF_CHECKED);
					}
				} else if (id == ID_Settle) {
					if (initing || doComparing) break;

					pCapture->DeviceConfig(0);
				}
				// [MOD]: 20171016 增加请正视摄像头提示语音
				else if (id == ID_REMIND_ON_OFF) {
					if (remindOnOff_) {
						remindOnOff_ = 0;
						
						CWnd* wnd = GetDlgItem(id);
						if (wnd)
							wnd->SetWindowText("关");
					}
					else {
						remindOnOff_ = 1;
						CWnd* wnd = GetDlgItem(id);
						if (wnd)
							wnd->SetWindowText("开");
					}
				}
				else if (id == ID_REMIND_UP) {
					if (remindSpeed_ != 0)
						--remindSpeed_;
				}
				else if (id == ID_REMIND_DOWN) {
					++remindSpeed_;
				}
				// [END]: 20171016
				else if (ID_SETTING == id) {

					static HINSTANCE temp_hsetting = LoadLibrary("facesetting.dll");
					if (temp_hsetting == NULL) {
						temp_hsetting = LoadLibrary("facesetting.dll");
						if (!temp_hsetting) {
							Message("缺少系统设置的相关组件");
							return 0;
						}
					}

					typedef int(*showRegisterFunc) (void);

					showRegisterFunc  fucntion = (showRegisterFunc)GetProcAddress(temp_hsetting, "SRShowSetting");

					// 展示设置界面
					fucntion();					   					

				}
				break;
			}
	
			case WM_LBUTTONDBLCLK:
				pauzeReading = false;
				SetForegroundWindow();
				ShowWindow(SW_SHOWNORMAL);
				break;
			default:
				break;
		}
	}
	return 1;
}


LRESULT CfacerecoDlg::OnUpdate(WPARAM wParam,LPARAM IParam) {
	ShowVisit();
	AfxBeginThread(UploadPassThreadProc, this);
	validPass();

	return 1;
}

UINT CfacerecoDlg::VideoThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doListening) 
	{
		if (!pDlg->pauseVideo)
			pDlg->DoListening(lpVoid);
		Sleep(40);
	}
	::CoUninitialize();

	return 1;
}

UINT CfacerecoDlg::VideoDiscernThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg = (CfacerecoDlg*)lpVoid;

	while (pDlg->doListening)
	{
		if (!pDlg->pauseVideo && pDlg->videoDetect)
			pDlg->function(lpVoid);
		Sleep(100);
	}
	::CoUninitialize();

	return 1;
}

UINT CfacerecoDlg::SocketThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doReceiving) {
		if (pDlg->videoDetect) {
			Sleep(100);
			continue;
		}

		pDlg->DoReceiving();
		Sleep(30);
	}

	::CoUninitialize();

	return 1;
}


UINT CfacerecoDlg::ReaderThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	// [MOD]: 20171016 增加请正视摄像头提示语音
	unsigned int& r_remindOnOff = pDlg->remindOnOff_;
	unsigned int& r_remindSpeed = pDlg->remindSpeed_;
	unsigned int video_show_delay = 0;
	CString& r_strTopTip = pDlg->strTopTip_;
	// [END]: 20171016
	while (pDlg->doReading) {
		if (pDlg->videoDetect) {
			Sleep(500);
			video_show_delay = 0;
			continue;
		}

		CloseComm();
		int ret = InitComm(1001);
		if (ret != 1) {
			pDlg->Message("无法开启读卡器, 请确保设备已连接.");
			continue ;
		}

		
		if (!pDlg->pauzeReading) {
			r_strTopTip = "请刷身份证...";
			pDlg->DoReading();
		}
			
		Sleep(200);

		if (video_show_delay++ != 0 && !(video_show_delay % 15)) {
			DeleteFile(pDlg->idFile);
			DeleteFile(pDlg->fullFile);
			DeleteFile(pDlg->idDataFile);
			DeleteFile(pDlg->avatarFile);
			DeleteFile(pDlg->handFaceFile);
		}

		// [MOD]: 20171016 增加请正视摄像头提示语音
		if (0 == r_remindOnOff)
			continue;
		r_remindOnOff += 1;
		if (!(r_remindOnOff % r_remindSpeed))
			PlaySound(TEXT("res/remind.wav"), NULL, SND_FILENAME | SND_ASYNC);
		// [END]: 20171016
	}
	::CoUninitialize();

	return 1;
}

UINT CfacerecoDlg::DetectThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	while (pDlg->doDetecting) {
		if (pDlg->videoDetect) {
			Sleep(100);
			continue;
		}

		if (!pDlg->pauzeDetecting)
			pDlg->DoDetecting();
		Sleep(30); 
	}
	::CoUninitialize();

	return 1;
}


UINT CfacerecoDlg::FilterThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	// [MOD]: 20171016 增加请正视摄像头提示语音
	unsigned int& r_remindOnOff = pDlg->remindOnOff_;
	unsigned int& r_remindSpeed = pDlg->remindSpeed_;
	// [END]: 20171016

	while (pDlg->doFilting) {
		if (pDlg->pauzeFilting) {
			Sleep(1000);
			continue;
		}

		pDlg->DoFilting();
		Sleep(200);

		// [MOD]: 20171016 增加请正视摄像头提示语音
		if (0 == r_remindOnOff)
			continue;
		r_remindOnOff += 1;
		if (!(r_remindOnOff % r_remindSpeed))
			PlaySound(TEXT("res/remind.wav"), NULL, SND_FILENAME | SND_ASYNC);
		// [END]: 20171016

	}

	::CoUninitialize();
	return 1;
}

UINT CfacerecoDlg::InitThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;

	pDlg->DoIniting();

	::CoUninitialize();
	return 1;

}

UINT CfacerecoDlg::HeartbeatThreadProc(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;
	bool bRet;

	while (pDlg->doHeartbeat) {
		bRet = pDlg->DoHeartbeat();
		if (!bRet) {
			spiritDlg->setState(3);
		}
		Sleep(1800000); 
	}

	::CoUninitialize();
	return 1;
}

UINT CfacerecoDlg::UploadPassThreadProc(LPVOID lpVoid) {
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;
	pDlg->UploadInfo(1);
	return 1;
}

UINT CfacerecoDlg::UploadRejectThreadProc(LPVOID lpVoid) {
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;
	pDlg->UploadInfo(0);
	return 1;
}

UINT CfacerecoDlg::UploadHandPassThreadProc(LPVOID lpVoid) {
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;
	pDlg->UploadInfo(2);
	return 1;
}

UINT CfacerecoDlg::UploadHandRejectThreadProc(LPVOID lpVoid) {
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;
	pDlg->UploadInfo(3);
	return 1;
}

void CfacerecoDlg::OnStnClickedSubmit() {
	if (ifGetImage) {
		SaveCBitmapToBmpFile(&bmpStoreFaceByHand, handFaceFile);
	}

	edtName.GetWindowTextA(byHandName);
	edtIDCode.GetWindowTextA(byHandCode);
	edtAddress.GetWindowTextA(byHandLocation);
	edtYear.GetWindowTextA(byHandYear);
	edtMonth.GetWindowTextA(byHandMonth);
	edtDay.GetWindowTextA(byHandDay);

	if (byHandName == "") {
		Message("请先填写正确用户名");
		return;
	}

	if (byHandCode == "") {
		Message("请先填写正确身份证号");
		return;
	}


	CString sex;
	if (genderMan)
		sex = "1";
	else
		sex = "0";

	CString birth_day;
	if (byHandYear.GetLength() > 0 && byHandMonth.GetLength() > 0 && byHandDay.GetLength() > 0)
		birth_day.Format("%s-%s-%s", byHandYear, byHandMonth, byHandDay);
	CString video_img_md5;
	CString imgFile = handFaceFile;
	if (ifGetImage) {
		video_img_md5 = Md5file(imgFile.GetBuffer());
	} else {
		imgFile = "";
	}


	CString location;

	bool res = false;
	//if (initType == 2) {
	//	res = apiHotelHand(appkey.GetBuffer(), appsecret.GetBuffer(), imgFile.GetBuffer(), byHandName.GetBuffer(), byHandCode.GetBuffer(),video_img_md5.GetBuffer(), sex.GetBuffer(), "3",
	//		internet_bar_id.GetBuffer(), birth_day.GetBuffer(), location.GetBuffer());
	//} else if (initType == 4) {
	//	res = apiNetBarHand(appkey.GetBuffer(), appsecret.GetBuffer(), imgFile.GetBuffer(), byHandName.GetBuffer(), byHandCode.GetBuffer(),video_img_md5.GetBuffer(), sex.GetBuffer(), "3",
	//		internet_bar_id.GetBuffer(), birth_day.GetBuffer(), location.GetBuffer());
	//}

	if (res) {
		Message("上传成功");
		edtYear.SetWindowTextA("");
		edtMonth.SetWindowTextA("");
		edtDay.SetWindowTextA("");
		edtName.SetWindowTextA("");
		edtIDCode.SetWindowTextA("");
		edtAddress.SetWindowTextA("");

	} else {
		Message("上传失败, 请填写正确信息");
	}
}

void CfacerecoDlg::OnStnClickedMinimize() {
	if (doComparing) return;

	ShowWindow(SW_HIDE);

	pauseVideo = true;
	HideMain();
}

void CfacerecoDlg::OnStnClickedMan() {
	if (genderMan) return;

	btnMan.SetBitmap(hManCheck);
	btnGirl.SetBitmap(hGirlUncheck);
	genderMan = true;
}

void CfacerecoDlg::OnStnClickedGirl() {
	if (!genderMan) return;

	btnMan.SetBitmap(hManUncheck);
	btnGirl.SetBitmap(hGirlCheck);
	genderMan = false;
}

void CfacerecoDlg::OnStnClickedConfirm() {
	if (!validByHand) return;

	ShowVisit();

	AfxBeginThread(UploadHandPassThreadProc, this);
	validPass();

	//// 发送消息
	//SendSocket("res:2");

	//doComparing = false;
	validByHand = false;
}

void CfacerecoDlg::OnStnClickedRefuse() {
	if (!validByHand) return;

	AfxBeginThread(UploadHandRejectThreadProc, this);

	CString msg;
	msg.Format("%s 认证否决 识别度 %d%%", name, (int)maxScore);
	validRefuse(msg.GetBuffer());


	//// 发送消息
	//SendSocket("res:0");
	//
	//if (cmdExit) {
	//	Exit();
	//}

	//validByHand = false;
	doComparing = false;
}

void CfacerecoDlg::OnStnClickedVersion() {
	// 关闭自动更新
	return;
	char newVersion[20];
	char urls[800];

	CString curVersion;
	curVersion.Format("v%s", version);
	int res = apiCheckVersion(appkey.GetBuffer(), appsecret.GetBuffer(), curVersion.GetBuffer(), internet_bar_id.GetBuffer(), newVersion, urls);

	if (res == -1) {
		Message("更新失败, 请确保网络已连接");
	} else if (res == 0) {
		Message("当前版本已为最新版");
	} else if (res == 1) {
		CString nVersion(newVersion);
		CString url(urls);
		
		bool res = UpdateVersion(nVersion, url);
		if (res) {
			Message("版本已更新, 重启后生效. 即将关闭程序, 请手动重启!!");
			if (!doComparing)
				Exit();
			else
				cmdExit = true;
		}
	}
}

void CfacerecoDlg::OnStnClickedClose() {
	ShowWindow(SW_HIDE);

	if (!doComparing) {
		SendProtect("cmd:exit");
		Sleep(1000);
		Exit();
	}
	else
		cmdExit = true;
}


void CfacerecoDlg::OnCbnSelchangeCombol() {
	if (initing) {
		int index = cboCapture->GetCurSel();
		pCapParam.lIndex = index;
		CString id;
		id.Format("%d", index);
		WritePrivateProfileString("camera","id", id, config);

		DoIniting();
		return ;
	}

	if (!initing && !doComparing) {
		int index = cboCapture->GetCurSel();
		pCapParam.lIndex = index;

		CString msg;
		msg.Format("Switch captur to no.%d", (index+1));
		WriteLog(msg.GetBuffer());

		pauzeReading = true;
		pauseVideo = true;
		pauzeFilting = true;
		Sleep(1000);

		if(pCapture) {
			pCapture->Stop();
			delete pCapture;
			pCapture=NULL;

			WriteLog("Stop capture success...");
		}

		// 初始化摄像头
		updateState("正在驱动摄像头,请稍等...");
		pCapture = MCapture::Create(&pCapParam);
		if (pCapture == NULL) {
			updateState("未识别摄像头，请确保设备已连接，并在右上角选择对应设备");
		
			WriteLog("Create Capture failed...");
			Message("未识别摄像头设备, 请确保设备已连接.");
			return;
		}
		WriteLog("Create Capture success...");

		BOOL bOK=pCapture->InitCapture();
		if (!bOK) {
			updateState("未识别摄像头，请确保设备已连接，并在右上角选择对应设备");
		
			WriteLog("Init Capture failed...");
			Message("未识别摄像头设备, 请确保设备已连接.");
			return;
		}
		WriteLog("Init Capture success...");
		
		CString id;
		id.Format("%d", index);
		WritePrivateProfileString("camera","id", id, config);

		updateState("请刷身份证...");


		pCapture->Play();
		pauzeReading = false;
		pauseVideo = false;
		pauzeFilting = false;
	}
}


void CfacerecoDlg::DoListening(LPVOID lpVoid) {
	int nWidth = pCapParam.szCap.cx;
	int nHeight = pCapParam.szCap.cy;
	long lLength = nWidth * nHeight * 3;

	BYTE* pCamBuf=new BYTE[lLength];
	BITMAPINFOHEADER bih;
	ContructBih(nWidth,nHeight,bih);
	BOOL bGet=pCapture->GetFrame(pCamBuf, lLength);


	if (bGet) {
		static unsigned int i = 0;
		
		if ((i++ % 24) == 0) {
			MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, false,24);
			WaitForSingleObject( mtxDetect_, INFINITE );  
			int ret = DetectFace(pCamBuf, nWidth, nHeight, NULL, true, true, false, false);
			ReleaseMutex( mtxDetect_ );
			MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, false,24);
		}


		if (!pauseVideo) {
#ifndef CHENPEIJIE_DONGCHEZHAN
			// 对视频流叠加图片
			cv::Mat video_mat;
			do {
				try {
					
					// 获取视频快照
//					WriteLog("<< Begin to draw the video stream. >>");
					CBitmap videoTemp;
					GetCBitmap(&bih, pCamBuf, videoTemp);
					SaveCBitmapToBmpFile(&videoTemp,"temp.bmp");
					video_mat = cv::imread("temp.bmp");
					//remove("temp.bmp");
					if (video_mat.empty())
						break;

					//WriteLog("Change the video stream size to fit the client.");
					RECT video_rect;
					picVideo.GetWindowRect(&video_rect);
					cv::resize(video_mat,video_mat,cv::Size(video_rect.right-video_rect.left,video_rect.bottom - video_rect.top));
					
					// [0] 初始化其他区域
					//WriteLog("Draw title bar.");
					// title
					// 初始化title区
					cv::Mat& mat_title_view = video_mat(cv::Rect(0,0,video_mat.size().width,60));
					// 向该区域填充黑底透明图
					SRFacerecoCv::pasteTransparencyImage(video_mat, cv::Mat(mat_title_view.rows,mat_title_view.cols,0,cv::Scalar(0,0,0)),0.3);
					cv::Mat logo = cv::imread("res/logo.bmp");
					if (!logo.empty()) {
						cv::Mat& logo_area = video_mat(cv::Rect(cv::Point(30,10),logo.size()));
						SRFacerecoCv::pasteTransparencyImage(logo_area,logo,0.99,RGB(252,252,252));
						//SRFacerecoCv::pasteText(video_mat, "|", 40, cv::Point(45 + logo.size().width, 95),cv::Scalar(252,252,252));
						//SRFacerecoCv::pasteText(video_mat, "山人科技", 30, cv::Point(70 + logo.size().width, 75),cv::Scalar(252,252,252));
						//SRFacerecoCv::pasteText(video_mat, "E S H A N R E N", 15, cv::Point(60 + logo.size().width, 95),cv::Scalar(252,252,252));
						//SRFacerecoCv::pasteText(
					}
					

					// 时间
					cv::Mat& title_mat = video_mat(cv::Rect(0,0,video_mat.size().width,50));
					CTime tm = CTime::GetCurrentTime();//获取系统日期
					CString str=tm.Format("%H:%M");
					SRFacerecoCv::pasteText(title_mat, str.GetBuffer(),30, cv::Point(title_mat.size().width - 100, 40), cv::Scalar(255,255,255));
					str = tm.Format("%m.%d");
					SRFacerecoCv::pasteText(title_mat, str.GetBuffer(),25, cv::Point(title_mat.size().width - 180, 40), cv::Scalar(255,255,255));


					// 读取证件照片
					//WriteLog("Look for photos.");
					cv::Mat id_face = cv::imread(idFile.GetBuffer());
					if (!id_face.empty()) {

						// 获取监控头像快照
						//WriteLog("Get avatar image file.");
						cv::Mat active_file = cv::imread(avatarFile.GetBuffer());
						
						//WriteLog("Draw result rect.");
						// [1] 初始化比对结果区
						cv::Mat& mat_ident_view = video_mat(cv::Rect(video_mat.size().width - 240 - 30,video_mat.size().height / 2 - 190,240,380));
						// 向该区域填充黑底透明图
						SRFacerecoCv::pasteTransparencyImage(mat_ident_view, cv::Mat(mat_ident_view.rows,mat_ident_view.cols,0,cv::Scalar(0,0,0)),0.5);
						

						// [2] 初始化各个选区// 
						cv::Mat& title_area = mat_ident_view(cv::Rect(0, 0, mat_ident_view.cols,50));
						cv::Mat& face_area = mat_ident_view(cv::Rect(0, title_area.rows ,mat_ident_view.cols, id_face.rows));
						int sum_title_face = title_area.rows + 10 + face_area.rows;
						cv::Mat& info_area = mat_ident_view(cv::Rect(0, sum_title_face, mat_ident_view.cols, mat_ident_view.rows - sum_title_face));


						// [3] 填写标题  
						//WriteLog("Draw result area title.");
						SRFacerecoCv::pasteText(title_area, "比对结果", 20, cv::Point(title_area.size().width/2 - 45, 30), cv::Scalar(255,255,255));


						// [4] 填写头像选区			
						//// 证件快照
						//WriteLog("Draw id card photo.");
						cv::Mat& id_face_mat = face_area(cv::Rect(10,0,id_face.cols,id_face.rows));
						SRFacerecoCv::pasteTransparencyImage(id_face_mat,id_face);

						// 视频快照
						//WriteLog("Draw avatar photo.");
						if (!active_file.empty()) {
							cv::Mat& video_face_mat = face_area(cv::Rect(face_area.cols - active_file.cols - 10,0,active_file.cols,active_file.rows));
							SRFacerecoCv::pasteTransparencyImage(video_face_mat,active_file,1);
						}



						// [5] 填写人物信息选区
						//WriteLog("Draw person information.");
						// 姓名
						CString temp_name = "姓名： " + this->name;
						SRFacerecoCv::pasteText(info_area, temp_name.GetBuffer(), 15,cv::Point(10, 40), cv::Scalar(255,255,255));
						// 性别
						CString temp_sex = "性别： " + this->gender;
						SRFacerecoCv::pasteText(info_area, temp_sex.GetBuffer(), 15,cv::Point(10, 80), cv::Scalar(255,255,255));
						// 身份证号
						CString temp_id_code = "身份证： " + this->idcode;
						SRFacerecoCv::pasteText(info_area, temp_id_code.GetBuffer(), 15, cv::Point(10, 120), cv::Scalar(255,255,255));
						// 认证结果
						CvScalar color;
						color.val[2] = GetRValue(clResultTip_);
						color.val[1] = GetGValue(clResultTip_);
						color.val[0] = GetBValue(clResultTip_);
						// 分数
						if (!strResultTip_.IsEmpty()) {
							WriteLog("Draw result quality score.");
							CString result;
							result.Format("%d%%", (int)maxScore);
							SRFacerecoCv::pasteText(info_area, result.GetBuffer(), 35, cv::Point(100, 160), color);
							// 结果
							cv::Point temp_point = cv::Point((video_mat.size().width >> 1) - 160, (video_mat.size().height >> 1) + (video_mat.size().height >> 2));
							SRFacerecoCv::pasteText(video_mat, strResultTip_.GetBuffer(), 40, temp_point, color);
						}
					

					}
						
					// [6] 设置视频参数
					BITMAPINFOHEADER bih1;
					ContructBih(video_mat.size().width,video_mat.size().height,bih1);

					// 反转视频帧
					MirrorDIB((LPSTR) video_mat.data, video_mat.size().width,video_mat.size().height, false,24);

					if (pCamBuf!=NULL) {
						delete[] pCamBuf;
						pCamBuf = NULL;
					}

					DrawBmpBuf2(bih1, video_mat.data, picVideo, TRUE, (LPCTSTR)"", clTopTip_, (LPCTSTR)"", clResultTip_);
					
					//WriteLog("<< Complete the video stream drawing. >>");
					return ;
				}
				catch(std::exception ex) {
					std::string error = "Make video frame error:";
					error += ex.what();
					WriteLog((char*)error.c_str());
					break;
				}
				catch(...) {
					WriteLog("Make video frame error.");
					break;
				}
			} while(false);
#endif
			// 设置文字状态
			DrawBmpBuf2(bih, pCamBuf, picVideo, TRUE, (LPCTSTR)"", clTopTip_, (LPCTSTR)"", clResultTip_);
		}
	}
	if (pCamBuf!=NULL)
		delete[] pCamBuf;
}

void CfacerecoDlg::function(LPVOID lpVoid) {
	
	int nWidth = pCapParam.szCap.cx;
	int nHeight = pCapParam.szCap.cy;
	long lLength = nWidth*nHeight * 3;

	BYTE* pCamBuf = new BYTE[lLength];
	BITMAPINFOHEADER bih;
	ContructBih(nWidth, nHeight, bih);

	while (videoDetect) {
		memset(pCamBuf, 0, lLength);
		BOOL bGet = pCapture->GetFrame(pCamBuf, lLength);

		if (bGet) {
			
			CTime temp;
			temp = CTime::GetCurrentTime();

			CBitmap videoTemp;
			int timeGap = temp.GetTime() - scanFaceTime.GetTime();
			if (timeGap < 10) {
				SetActiveWindow();
				double matchScore = 0;
				BYTE* pFeature = new BYTE[featureSize];

				// 缓存视频图像
				videoTemp.DeleteObject();
				GetCBitmap(&bih, pCamBuf, videoTemp);
				SaveCBitmapToBmpFile(&videoTemp, fullFile);

				MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, false, 24);
			WaitForSingleObject( mtxDetect_, INFINITE );  
				int ret = DetectFace(pCamBuf, nWidth, nHeight, pFeature, false, false, true, true);
			ReleaseMutex( mtxDetect_ );
				MirrorDIB((LPSTR)pCamBuf, nWidth, nHeight, false, 24);
				if (ret == 1) {
					matchCount++;
					matchScore = EF_Compare(pFeature, idFeature) * 100 + 20.0f;
					if (matchScore >= 100) matchScore = 95 + (rand() % 5);

					if (matchScore > maxScore) {
						// 更新最大值
						maxScore = matchScore;
						// 更新缓存视频图像
						HGDIOBJ obj = bmpStoreVideo.Detach();
						DeleteObject(obj);
						bmpStoreVideo.Attach(CopyBitmap(videoTemp));
						// 更新缓存人脸图像
						obj = bmpStoreFace.Detach();
						DeleteObject(obj);
						bmpStoreFace.Attach(CopyBitmap(bmpFace));

						//picSnapshoot_.SetBitmap(bmpStoreFace);

						// 更新缓存画框人脸照
						videoTemp.DeleteObject();
						GetCBitmap(&bih, pCamBuf, videoTemp);
						obj = bmpStoreBoxedFace.Detach();
						DeleteObject(obj);
						bmpStoreBoxedFace.Attach(CopyBitmap(videoTemp));

						// 缓存视频人脸特征值
						videoFeatureHex = BYTEToHex(pFeature, 2560);
					}

					if (matchCount >= maxMatchCountConst) {
						// 停止监测人脸
						videoDetect = false;

						CString txtScore;
						txtScore.Format("%d%%", (int)maxScore);
						lblScore.SetWindowTextA(txtScore);
						lblPercent.SetWindowTextA("%");
						lblMatch.SetWindowTextA("相似度");
						//picVideo.SetBitmap(bmpStoreBoxedFace);


						// 更新窗口
						//Invalidate(true); 
						//UpdateWindow();

						pauseVideo = true;

						// 保存视频文件
						SaveCBitmapToBmpFile(&bmpStoreVideo, fullFile);
						// 保存人脸文件
						SaveCBitmapToBmpFile(&bmpStoreFace, avatarFile);


						picSnapshoot_.SetBitmap(bmpStoreFace);

						if (maxScore >= scoreThresholdHigh) {
							WriteLog("Compare success...");

							CfacerecoDlg* pDlg = (CfacerecoDlg*)lpVoid;
							pDlg->PostMessage(WM_UPDATE, 0, 0);
						}
						else {
							WriteLog("Compare failed...");

							btnRefuse.ShowWindow(false);
							CString msg;
							msg.Format("认证失败", name, (int)maxScore);

							AfxBeginThread(UploadRejectThreadProc, this);
							validRefuse(msg.GetBuffer());
						}
					}
				}
				videoTemp.DeleteObject();
				delete []pFeature;
			}
			else {
				WriteLog("Compare timeout...");

				videoDetect = false;
				CString msg;
				btnRefuse.ShowWindow(false);
				updateState("未识别人脸...   ");

				msg.Format("未识别人脸");

				// 更新缓存视频图像
				HGDIOBJ obj = bmpStoreVideo.Detach();
				DeleteObject(obj);
				bmpStoreVideo.Attach(CopyBitmap(videoTemp));
				// 保存视频文件
				SaveCBitmapToBmpFile(&bmpStoreVideo, fullFile);

				isTimeout = true;
				AfxBeginThread(UploadRejectThreadProc, this);
				validRefuse(msg.GetBuffer());
			}
		}

	}

	
	
	//if (bGet) {
	//	if (videoDetect) {
	//	}
	//	if (!pauseVideo) {
	//		//DrawBmpBuf(bih,pCamBuf,picVideo.m_hWnd,true,true);
	//		// 设置文字状态
	//		DrawBmpBuf2(bih, pCamBuf, picVideo, TRUE, (LPCTSTR)strTopTip_, clTopTip_, (LPCTSTR)strResultTip_, clResultTip_);
	//		//CBitmap* bmp = CBitmap::FromHandle(hIdCard);
	//		//DrawBmp(picVideo,bmp,0,0);

	//		//picIDCard.SetWindowPos(0,539,132,201,261,SWP_NOACTIVATE); //设置广告窗口浮动于右下角
	//	}
	//}
	delete[] pCamBuf;
}

void CfacerecoDlg::DoReceiving() {
	SOCKADDR_IN saClient;
	char cRecvBuff[400];
	int nSize,nbSize;
	nSize = sizeof (SOCKADDR_IN);

	if((nbSize=recvfrom(receiverSocket,cRecvBuff,400,0,(SOCKADDR FAR *) &saClient,&nSize))==SOCKET_ERROR) {
		return;
	}
	cRecvBuff[nbSize] = '\0';
	CString msg;
	msg = cRecvBuff;

	CStringArray ary;
	SplitCString(cRecvBuff, ary, ';');
	if (ary.GetSize() > 1) {
		if (ary.ElementAt(0) == "cmd" && ary.ElementAt(1) == "exit") {
			SendProtect("cmd:exit");
			Sleep(1000);
			cmdExit = true;
		}
		if (!doComparing) {
			if (ary.ElementAt(0) == "idinfo") {
				if (ary.GetSize() == 12) {
					if(CopyFile(ary.ElementAt(1),idFile, false)) {
						SetActiveWindow();
						SetFocus();
						ShowWindow(SW_SHOW);
						SetFocus();
						SetActiveWindow();

						name=ary.ElementAt(2);
						gender=ary.ElementAt(3);
						nation=ary.ElementAt(4);
						bornday=ary.ElementAt(5);
						location=ary.ElementAt(6);
						idcode=ary.ElementAt(7);
						polistation=ary.ElementAt(8);
						startday=ary.ElementAt(9);
						enday=ary.ElementAt(10);
						internet_bar_id = ary.ElementAt(11);

						//DetectIDFace(true);
					}
				} else {
					SendSocket("res:0");
				}
			} else if (ary.ElementAt(0) == "cmd") {
				if (ary.ElementAt(1) == "hide") {
					Clear();
					ShowWindow(SW_HIDE);
				} else if (ary.ElementAt(1) == "show") {
					SetActiveWindow();
					ShowWindow(SW_SHOW);
					SetActiveWindow();
				} else if (ary.ElementAt(1) == "clear") {
					Clear();
				} else if (ary.ElementAt(1) == "exit") {
					Exit();
				}
			}
		} else {
			// 发送消息
			SendSocket("Bus");
		}
	}
}


void CfacerecoDlg::DoReading() {
	if (!doComparing) {
		DetectIDFace(false);
	}
}

void CfacerecoDlg::DoDetecting() {
#ifdef _SHANREN_TEST_H_
	SRTest test;
#endif
	if (!doComparing) {

		CTime temp;
		temp = CTime::GetCurrentTime();
		int timeGap = temp.GetTime() - detectingTime.GetTime();
		if (timeGap > 10) {
			pauzeDetecting = true;
			doComparing = false;
			spiritDlg->setState(1);
			
			return;
		}

		CFileFind find;
		CTime lastTime = NULL;
		CTime tempTime;
		CString fileName;
		CString filePath = "";
		BOOL bf = find.FindFile(detectPath);
		while (bf) {
			bf = find.FindNextFile();
			if(!find.IsDots()) {
				find.GetLastWriteTime(tempTime);
				if (lastTime == NULL) {
					lastTime = tempTime;
					fileName = find.GetFileName();
					filePath = find.GetFilePath();
				} else if (tempTime > lastTime) {
					lastTime = tempTime;
					fileName = find.GetFileName();
					filePath = find.GetFilePath();
				}
			}
		}


		if (filePath != "") {
			if (CopyFile(find.GetFilePath(),idFile, false)) {
				idcode = fileName.Left(fileName.GetLength() - 6);
				::ShowWindow(this->m_hWnd, SW_SHOW);
				pauzeReading = false;
				pauzeDetecting = true;

				DetectIDFace(true);
			}
			remove(filePath);
		}
	}
#ifdef _SHANREN_TEST_H_
	std::stringstream log_info_stream;
	log_info_stream << test.getElapsed();

	std::string log_info;
	log_info_stream >> log_info;
	log_info += "times, [Function:DoDetecting] elapsed time.";

	WriteLog((char*)log_info.c_str());
#endif
}

void CfacerecoDlg::DoFilting() {
#ifdef _SHANREN_TEST_H_
	SRTest test;
#endif
	if (!doComparing) {
		BOOL bRet;
		CHAR tname[50];
		CHAR tgender[20];
		CHAR tnation[20];
		CHAR tbornday[20];
		CHAR taddress[80];
		CHAR tidcode[20];
		CHAR torgn[50];
		CHAR tstartday[20];
		CHAR tendday[20];
		CHAR tpath[256];
		memset(tname,0,50);
		memset(tgender,0,20);
		memset(tnation,0,20);
		memset(tbornday,0,20);
		memset(taddress,0,80);
		memset(tidcode,0,20);
		memset(torgn,0,50);
		memset(tstartday,0,20);
		memset(tendday,0,20);

		//bRet=rfdCheckHandle();
		//if (!bRet) {
		//	WriteLog("Check driver handler failed...");
		//	Message("读卡器链接失败, 请确保设备已连接");
		//	
		//	unloadDriver("ReadFilter");
		//	bool bRet=loadDriver("ReadFilter", readFilterPath.GetBuffer());
		//	if (!bRet) {
		//		WriteLog("Load filter failed...");
		//		Message("加载驱动失败, 请确保设备已连接.");
		//		Exit();
		//		return;
		//	}
		//	Sleep(1000);
		//}

		bRet=rfdReadInfo(tname,tgender,tnation,tbornday,taddress,tidcode,torgn,tstartday,tendday,storagePath.GetBuffer(),1288, readSleep);
#ifdef _SHANREN_TEST_H_
		std::stringstream log_info_stream;
		log_info_stream << test.getElapsed();

		std::string log_info;
		log_info_stream >> log_info;
		log_info += "times, [Function:rfdReadInfo] elapsed time.";

		WriteLog((char*)log_info.c_str());
#endif
		if (bRet) {
			pauzeFilting = true;
			WriteLog("Read idcard success...");

			// copy wlt file to c:
			CString wltFile;
			wltFile.Format("%s\\photo.wlt", storagePath);
			DeleteFile("c:\\photo.wlt");
			bRet = MoveFile(wltFile.GetBuffer(), "c:\\photo.wlt");
			if (!bRet) {
				WriteLog("Move wlt failed...");
				pauzeFilting = false;
				return;
			}
			// get bmp
			std::string temp_exeWltToBmp(exeWltToBmp.GetBuffer());
			STARTUPINFO si;
			memset(&si, 0, sizeof(STARTUPINFO));//初始化si在内存块中的值（详见memset函数）
			si.cb = sizeof(STARTUPINFO);
			si.dwFlags = STARTF_USESHOWWINDOW;
			si.wShowWindow = SW_HIDE;
			PROCESS_INFORMATION pi;//必备参数设置结束
			CreateProcess(NULL, (char*)temp_exeWltToBmp.c_str(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
			// 等待程序退出
			DWORD exit_code = STILL_ACTIVE;


			clock_t temp_start_clock = clock();
			do {				
				GetExitCodeProcess(pi.hProcess, &exit_code);
				// 超时等待1秒钟
				if (temp_start_clock - clock() > 1000)
					break;
			} while (exit_code == STILL_ACTIVE);

			// copy bmp to storage
			DeleteFile(idFile.GetBuffer());
			bRet = MoveFile("c:\\photo.bmp", idFile.GetBuffer());
			if (!bRet)  {
				WriteLog("Move bmp failed...");
				pauzeFilting = false;
				return;
			}
			WriteLog("Generate photo success...");

			SetActiveWindow();
			SetFocus();
			ShowWindow(SW_SHOW);
			SetFocus();
			SetActiveWindow();

			name.Format("%s", tname);
			gender.Format("%s", tgender);
			nation.Format("%s", tnation);
			bornday.Format("%s", tbornday);
			location.Format("%s", taddress);
			idcode.Format("%s", tidcode);
			polistation.Format("%s", torgn);
			startday.Format("%s", tstartday);
			enday.Format("%s", tendday);
			internet_bar_id="0";

			name.Trim();
			gender.Trim();
			nation.Trim();
			bornday.Trim();
			location.Trim();
			idcode.Trim();
			polistation.Trim();
			startday.Trim();
			enday.Trim();
#ifdef _SHANREN_TEST_H_
			std::stringstream log_info_stream1;
			log_info_stream1 << test.getElapsed();

			std::string log_info;
			log_info_stream1 >> log_info;
			log_info += "times, [Function:DoFilting] elapsed time.";

			WriteLog((char*)log_info.c_str());
#endif
			DetectIDFace(true);

		}
	}
}

void CfacerecoDlg::DoIniting() {
	// spirit hide
	spiritDlg->ShowWindow(false);

	// 初始化摄像头
	updateState("正在驱动摄像头,请稍等...");
	pCapture = MCapture::Create(&pCapParam);
	if (pCapture == NULL) {
		updateState("未识别摄像头，请确保设备已连接，并在右上角选择对应设备");
		
		WriteLog("Create Capture failed...");
		Message("未识别摄像头设备, 请确保设备已连接");
		return;
	}
	WriteLog("Create Capture success...");

	BOOL bOK=pCapture->InitCapture();
	if (!bOK) {
		updateState("未识别摄像头，请确保设备已连接，并在右上角选择对应设备");

		WriteLog("Init Capture failed...");
		Message("未识别摄像头设备, 请确保设备已连接");
		return;
	}
	WriteLog("Init Capture success...");
	
	try {
		pCapture->Stop();
		Sleep(200);
		pCapture->Play();
		WriteLog("Play capture success...");
	} catch (...) {
		WriteLog("Play capture failed...");
		Message("启动摄像头失败, 请确保设备已连接");
		return;
	}

	// 初始化识别程序

	/*updateState("正在初始化识别程序,请稍等...");*/
	THFI_Param param;
	param.nMinFaceSize=40;
	param.nRollAngle=45;
	param.bOnlyDetect=true;
	int ret = THFI_Create(2,&param);
	if (ret != 2) {
		WriteLog("Init face component failed...");
		Message("人脸识别程序加载失败, 请重试.");
		Exit();
		return;
	}
	WriteLog("Init face component success...");

	EF_Init(2);
	WriteLog("EF_Init success...");

	// 加载驱动
	if (1 == initType || 2 == initType) {


#if 0  // 老版本读卡器
		updateState("正在加载驱动,请稍等...");
		bRet=loadDriver("ReadFilter", readFilterPath.GetBuffer());
		if (!bRet) {
			WriteLog("Load filter failed...");
			Message("加载驱动失败, 请确保设备已连接.");
			Exit();
			return;
		}
		unloadDriver("ReadFilter");
		bRet=loadDriver("ReadFilter", readFilterPath.GetBuffer());
		if (!bRet) {
			WriteLog("Load filter failed...");
			Message("加载驱动失败, 请确保设备已连接.");
			Exit();
			return;
		}
		WriteLog("Load filter success...");

		iRet=rfdInit();
		if (iRet == 32) {
			Message("系统检测到其他程序正在使用读卡器,请关闭后启动本系统");
			Exit();
			return;
		} else if (iRet == 0) {
			Message("获取驱动失败, 请确保设备已连接");
			Exit();
			return;
		}

		// 检验读卡器
		bRet=rfdCheck();
		if (!bRet) {
			WriteLog("Check filter failed...");
			Message("测试驱动失败, 请确保设备已连接");
			Exit();
			return;
		}
		WriteLog("Check filter success...");

		AfxBeginThread(FilterThreadProc, this);
		WriteLog("Run filter success...");

//#ifdef __SHANREN_IDENTIDCARD_H__ // 自定义的读卡器通用接口
#else

		int ret = InitComm(1001);
		if (ret != 1) {
			Message("无法开启读卡器, 请确保设备已连接.");
			Exit();
			return ;
		}
		WriteLog("init comm  success");

		AfxBeginThread(ReaderThreadProc, this);
#endif
		initFilter = true;
	}



/*	iRet = AiFaceInit();
	std::ostringstream  io1;
	io1 << "Init AiFace :" << iRet;

	WriteLog((char*)io1.str().c_str());

	if (iRet == -1) {
		WriteLog("Init face component failed...");
		Message("人像识别未授权, 请重试.");
		Exit();
		return;
	}
	if (iRet == -2) {
		WriteLog("Init face algorithm failed...");
		Message("人像识别算法加载失败, 请重试.");
		Exit();
		return;
	}
	std::ostringstream  io;
	io << "Init AiFace success..." << iRet;
	
	WriteLog((char*)io.str().c_str());
*/
	
	//bool buildSocket = false;
	//// 创建接收守护进程消息Socket
	//buildSocket = BuildReceiverSocket(receiverProtect, receiverProtectPort, 5);
	//if(!buildSocket)
	//{
	//	Message("无法创建通信1.\n");
	//	Exit();
	//	return ;
	//}

	//// 创建发送守护进程消息Socket
	//buildSocket = BuildSenderSocket(senderProtect, senderProtectPort, saUdpServProtect);
	//if(!buildSocket)
	//{
	//	Message("无法创建通信2.\n");
	//	Exit();
	//	return ;
	//}

	//// 打开守护进程
	//WinExec(appPath + "\\faceprotect.dll", SW_SHOW);
	//Sleep(3000);

	SOCKADDR_IN saClient;
	char cRecvBuff[200];
	int nSize,nbSize;
	nSize = sizeof (SOCKADDR_IN);

	// 启用绘制摄像头线程
	AfxBeginThread(VideoThreadProc, this);
	WriteLog("Run video success...");

	AfxBeginThread(VideoDiscernThreadProc, this);
	WriteLog("Run video discern success...");

	// 启动 Heartbeat;
	AfxBeginThread(HeartbeatThreadProc, this);
	WriteLog("Run heartbeat success...");

	initing = false;
	pauzeFilting = false;
	pauzeReading = false;

	//Invalidate(true);
//	UpdateWindow();
	AfxBeginThread(thread_VerifyTrainInfo, this);
#if 0

	int bret = apiVerifyTrainInfo(appkey, appsecret, verifyUrl_, placeCode_.GetBuffer(), verifyCode_.GetBuffer());
	if (bret) {
		WriteLog("verify train place and verify code succeed.");
	}
	else {
		WriteLog("verify train place and verify code failed.");
	}

	BOOL bRet = DoHeartbeat();
	int iRet;
	if (!bRet) {
		Exit();
	}
#if 1 // 版本自动更新
	CheckSource();
	CheckFunction();
#endif 

	// 请求阈值
	QueryThreshold();
	WriteLog("Query score range success...");
#endif
	//ShowWindow(SW_HIDE);
	updateState("请刷身份证...");
}

bool CfacerecoDlg::DoHeartbeat() {
	bool bRet;
	char result[200];

	try {
		bRet = apiActiveHeartbeat(appkey.GetBuffer(), appsecret.GetBuffer(), token.GetBuffer(), result);
		if (bRet) {
			CString notation;
			notation.Format("兔啃:%s",appsecret);
			string utf8Str;
			MultiToUTF8(notation.GetBuffer(), utf8Str);

			char tmp[40];
			apiMd5((unsigned char *)utf8Str.c_str(),utf8Str.length(), tmp);

			if (token == "abcdefghijklmnopqrstuvwxyz123456" || strcmp(tmp, result) == 0) {
				token.Format("%s", result);
				WritePrivateProfileString("config","token", token, settingFile);
				WriteLog("Check token success...");
				return true;
			}
		}
	} catch (...) {
	
	}

	//WriteLog("Check token failed...");

	//SendProtect("cmd:exit");
	//Message("状态失效, 请确认网络已连接或联系管理员");
	//Exit();
	return true;
}

void CfacerecoDlg::LoadConfig() {
	// 摄像头初始值
	pCapParam.szCap.cx=640;
	pCapParam.szCap.cy=480;
	pCapParam.eType=CAP_WDM;
	pCapParam.lIndex=0;

	// 初始化类型
	initType = GetPrivateProfileInt("identities","initype",0,config);
	//// appkey
	//GetPrivateProfileString("identities","appkey","0", appkey.GetBuffer(33), 33,config);

	//// appsecret
	//GetPrivateProfileString("identities","appsecret","0", appsecret.GetBuffer(33), 33,config);

	// 读卡休眠时间
	readSleep =  GetPrivateProfileInt("config","readSleep",200,config);
	// 摄像头编号
	pCapParam.lIndex = GetPrivateProfileInt("camera","id",0,config);
	// 发送端口
	senderPort = GetPrivateProfileInt("socket","sport",7002,config);
	// 接收端口
	receiverPort = GetPrivateProfileInt("socket","lport",7001,config);
	// 发送保护进程端口
	senderProtectPort = GetPrivateProfileInt("socket","spport",37777,config);
	// 接收守护进程端口
	receiverProtectPort = GetPrivateProfileInt("socket","lpport",36666,config);	
	// 读卡器端口
	cardReaderPort=GetPrivateProfileInt("card","port",1001,config);
	// 版本号
	GetPrivateProfileString("config","curver","v1.0.0", version.GetBuffer(15), 15,storeConfig);

	// 场所编号
	GetPrivateProfileString("config","placeCode","10001", placeCode.GetBuffer(50), 50,settingFile);

	//// 读取场地编号
	//GetPrivateProfileString("config","placeno","0", internet_bar_id.GetBuffer(15), 15,storeConfig);

	//// 是否全屏
	//fullScreen=GetPrivateProfileInt("config","fullscreen",0,storeConfig);
	//// 读取监测路径
	//GetPrivateProfileString("config", "detectpath", "", detectPath.GetBuffer(300), 300, storeConfig);
	//detectPath.Format("%s\\*_H.jpg", detectPath.GetBuffer());

	GetPrivateProfileString("config","token","", token.GetBuffer(33), 33,settingFile);
}

void CfacerecoDlg::QueryThreshold() {
	char threshold[4];
	char thresholdHigh[4];
	//apiThreshold(appkey.GetBuffer(), appsecret.GetBuffer(), threshold, thresholdHigh);
	apiActiveThreshold(appkey.GetBuffer(), appsecret.GetBuffer(), token.GetBuffer(), threshold, thresholdHigh);

	scoreThreshold = atof(threshold);
	scoreThresholdHigh = atof(thresholdHigh);

	if (scoreThreshold == 0)
		scoreThreshold = 55;
	if (scoreThresholdHigh == 0)
		scoreThresholdHigh = 65;
}

void CfacerecoDlg::UploadInfo(int result) {
	if(!FileExists(idFile.GetBuffer())) return;
	if(!FileExists(fullFile.GetBuffer())) return;

	if(!isTimeout) {
		if(!FileExists(avatarFile.GetBuffer())) return;
	}
#if 0
	if (!isIdlessCompare) {
		if(!FileExists(idDataFile.GetBuffer())) return;
	}
#else
	ofstream file;

	file.open("storage\\id.dat");
	//file.open(idDataFile.GetBuffer(),std::ios_base::out);
	if (file.is_open()) {
		file<<"sdtapi hd, not id data source.";
		file.close();	
	}
	else {
		WriteLog("id.dat write erroe");
	}
	
#endif

	CTime temp;
	temp = CTime::GetCurrentTime();
	long time = temp.GetTime();

	CString imgPath;
	imgPath.Format("%s\\%ld", storagePath, time);
	// 创建临时目录
	::CreateDirectory(imgPath,NULL);
	// 拷贝待上传文件到缓存文件夹
	CString tmpIdFile, tmpAvatarFile, tmpFullFile, tmpIdDataFile;
	tmpFullFile = imgPath+"\\full.jpg";
	tmpAvatarFile = imgPath+"\\avatar.jpg";
	tmpIdFile = imgPath+"\\photo.bmp";
	tmpIdDataFile = imgPath+"\\id.dat";

	BOOL bRet;
	bRet = CopyFile(idFile, tmpIdFile,true);
	if (!bRet) {
//WriteLog("bRet = MoveFile(idFile, tmpIdFile);");
		RemoveDirectory(imgPath);
		return;
	}

	if (!isTimeout) {
		bRet = CopyFile(avatarFile, tmpAvatarFile,true);
		if (!bRet) {
//WriteLog("bRet = MoveFile(avatarFile, tmpAvatarFile);");
			DeleteFile(tmpIdFile);
			RemoveDirectory(imgPath);
			return;
		}
	}

	bRet = CopyFile(fullFile, tmpFullFile,true);
	if (!bRet) {
		DeleteFile(tmpIdFile);
		if (!isTimeout) {
			DeleteFile(tmpAvatarFile);
		}
//WriteLog("bRet = MoveFile(fullFile, tmpFullFile);");

		RemoveDirectory(imgPath);
		return;
	}

	if (!isIdlessCompare) {
		bRet = MoveFile(idDataFile, tmpIdDataFile);
		if (!bRet) {
			DeleteFile(tmpIdFile);
			DeleteFile(tmpFullFile);
			if (!isTimeout) {
				DeleteFile(tmpAvatarFile);
			}

			RemoveDirectory(imgPath);
			return;
		}
	}


	CString place;
	if (initType == 1 || initType == 2) {
		place = "网吧";
	}
	CString id_card_info;
	id_card_info.Format("%s-%s-%s-%s-【%s】%s-%s-%s-%s-%s", name, gender, nation, bornday, place, location,idcode, polistation, startday, enday);

	CString vedio_img_md5;
	CString id_card_img_md5= Md5file(tmpIdFile.GetBuffer());
	CString full_img_md5= Md5file(tmpFullFile.GetBuffer());

	if (!isTimeout) {
		vedio_img_md5 = Md5file(tmpAvatarFile.GetBuffer());
	}

	CString res;
	res.Format("%d", result);
	similarity.Format("%d", (int)maxScore);

	//apiUploadInfo(appkey.GetBuffer(), appsecret.GetBuffer(), idFile.GetBuffer(), avatarFile.GetBuffer(),
	//	fullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
	//	internet_bar_id.GetBuffer(), res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), initType);

	//apiActiveUpload(appkey.GetBuffer(), appsecret.GetBuffer(), sourceUrl.GetBuffer(), tmpIdFile.GetBuffer(), tmpAvatarFile.GetBuffer(),
	//	tmpFullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
	//	res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), placeCode.GetBuffer());

	CString lThreshold, hThreshold;
	lThreshold.Format("%d", (int)scoreThreshold);
	hThreshold.Format("%d", (int)scoreThresholdHigh);
	do {
	if (!isIdlessCompare) {
		byte idData[1288];
		ifstream istream;
		istream.open(tmpIdDataFile, std::ios::binary);
		istream.read((char *)idData, 1288);
		istream.close();

		CString strIdData;
		strIdData = BYTEToHex(idData, 1288);

		// @动车站版
		apiActiveUpload(appkey.GetBuffer(), appsecret.GetBuffer(), sourceUrl.GetBuffer(), identity.GetBuffer(), tmpIdFile.GetBuffer(), tmpAvatarFile.GetBuffer(),
			tmpFullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
			res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), strIdData.GetBuffer(),lThreshold.GetBuffer(),hThreshold.GetBuffer(), placeCode.GetBuffer(), false);
		break;
		// @动车站版 else delete return  
		
		if (!isTimeout) {
			if (sourceType == 1 || sourceType == 3) {
				// full to maintain
				apiActiveUpload(appkey.GetBuffer(), appsecret.GetBuffer(), sourceUrl.GetBuffer(), identity.GetBuffer(), tmpIdFile.GetBuffer(), tmpAvatarFile.GetBuffer(),
					tmpFullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
					res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), strIdData.GetBuffer(),lThreshold.GetBuffer(),hThreshold.GetBuffer(), placeCode.GetBuffer(), false);
			} else if (sourceType == 4) {
				// only text to maitnain
				apiActiveUpload(appkey.GetBuffer(), appsecret.GetBuffer(), sourceUrl.GetBuffer(), identity.GetBuffer(), NULL, NULL,
					NULL, full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
					res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), strIdData.GetBuffer(),lThreshold.GetBuffer(), hThreshold.GetBuffer(),placeCode.GetBuffer(), false);
			}
		}

		if (sourceType == 2 || sourceType == 3 || sourceType == 4) {
			// send to visitor hoster
			if (initType ==  2 && !isTimeout) {
				apiThirdUpload(appkey.GetBuffer(), appsecret.GetBuffer(), clientSourceUrl.GetBuffer(), tmpIdFile.GetBuffer(), tmpAvatarFile.GetBuffer(),
					tmpFullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
					res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), placeCode.GetBuffer(), hoster_id.GetBuffer(),phone_num.GetBuffer(), reason.GetBuffer());
			} else {
				CString uploadUrl;
				if (ifIdless) {
					uploadUrl = idlessUrl;
				} else {
					uploadUrl = clientSourceUrl;
				}

				if (isTimeout) {
					// full to client
					apiActiveUpload(appkey.GetBuffer(), appsecret.GetBuffer(), uploadUrl.GetBuffer(), identity.GetBuffer(), tmpIdFile.GetBuffer(), NULL,
						tmpFullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
						res.GetBuffer(), similarity.GetBuffer(), NULL, NULL, strIdData.GetBuffer(),lThreshold.GetBuffer(),hThreshold.GetBuffer(), placeCode.GetBuffer(), true);
				} else {
					
					// full to client
					apiActiveUpload(appkey.GetBuffer(), appsecret.GetBuffer(), uploadUrl.GetBuffer(), identity.GetBuffer(), tmpIdFile.GetBuffer(), tmpAvatarFile.GetBuffer(),
						tmpFullFile.GetBuffer(), full_img_md5.GetBuffer(), id_card_img_md5.GetBuffer(), id_card_info.GetBuffer(), idFeatureHex.GetBuffer(),
						res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), vedio_img_md5.GetBuffer(), strIdData.GetBuffer(),lThreshold.GetBuffer(),hThreshold.GetBuffer(), placeCode.GetBuffer(), false);
				}

			}
		}
	} else {
		if (isTimeout) {
			apiActiveUploadIdless(appkey.GetBuffer(), appsecret.GetBuffer(), idlessUrl.GetBuffer(),identity.GetBuffer(), tmpIdFile.GetBuffer(), NULL,
				tmpFullFile.GetBuffer(), res.GetBuffer(), similarity.GetBuffer(), NULL, idcode.GetBuffer(),lThreshold.GetBuffer(), hThreshold.GetBuffer(),placeCode.GetBuffer(), true);
		} else {
			apiActiveUploadIdless(appkey.GetBuffer(), appsecret.GetBuffer(), idlessUrl.GetBuffer(),identity.GetBuffer(), tmpIdFile.GetBuffer(), tmpAvatarFile.GetBuffer(),
				tmpFullFile.GetBuffer(), res.GetBuffer(), similarity.GetBuffer(), videoFeatureHex.GetBuffer(), idcode.GetBuffer(),lThreshold.GetBuffer(),hThreshold.GetBuffer(), placeCode.GetBuffer(), false);
		}
	}
	} while(false);

	hoster_id = "0";
	phone_num = "";
	reason = "";

	DeleteFile(tmpIdFile);
	DeleteFile(tmpAvatarFile);
	DeleteFile(tmpFullFile);
	if (!isIdlessCompare) {
		DeleteFile(tmpIdDataFile);
	}
	RemoveDirectory(imgPath);

	isTimeout = false;
 
	if (cmdExit) {
		Exit();
	}
}


bool CfacerecoDlg::UpdateVersion(CString& nVersion, CString& urls) {
	nVersion = nVersion.Mid(1, nVersion.GetLength() - 1);
	// 更新版本号
	char configFile[256];
	strcpy(configFile,appPath);
	strcat(configFile,"\\storage\\config.ini");
	WritePrivateProfileString("config","newvers", nVersion, configFile);

	// 创建新版本文件夹
	CString filePath;
	filePath.Format("%s\\%s", appPath, nVersion);
	::CreateDirectory(filePath,NULL);

	// 下载新版本组件
	CString fileName;
	CStringArray ary;
	SplitCString(urls, ary, ';');

	for (int i = 0; i < ary.GetSize(); i++) {
		CStringArray fileInfo;
		SplitCString(ary.ElementAt(i), fileInfo, '-');
		fileName.Format("%s\\%s.dll", filePath, fileInfo.ElementAt(0).GetBuffer());

		int res = DownloadFile(fileInfo.ElementAt(1), fileName);
		if (res != 0) {
			Message("版本更新失败, 请稍后再试");
			return false;
		}
	}

	return true;
}

void CfacerecoDlg::DrawMatch() {
	CFont normal;
	normal.CreatePointFont(400,_T("微软雅黑"),NULL);

	CFont font;  
	font.CreateFont(30,0,0,0,400,0,0,0,  
		ANSI_CHARSET,OUT_DEFAULT_PRECIS,  
		CLIP_DEFAULT_PRECIS,  
		DEFAULT_QUALITY,  
		DEFAULT_PITCH|FF_DONTCARE,  
		"微软雅黑");

	if (maxScore < scoreThreshold) {
		DrawLabel(&lblScore, &font, DT_CENTER, RGB(255,0,0));
	} else {
		DrawLabel(&lblScore, &font, DT_CENTER, RGB(255,255,255));
	}

	
	CFont fpercent;
	fpercent.CreatePointFont(200,_T("微软雅黑"),NULL);
	DrawLabel(&lblPercent, &fpercent, DT_LEFT, RGB(255,255,255));
	

	CFont fmatch;
	fmatch.CreatePointFont(150,_T("微软雅黑"),NULL);
	DrawLabel(&lblMatch, &fmatch, DT_CENTER, RGB(255,255,255));
}


void CfacerecoDlg::DrawAddress() {
	CFont normal;
	normal.CreatePointFont(80,_T("微软雅黑"),NULL);

	CRect rect;
	CString text;
	lblAddress.GetWindowTextA(text);
	lblAddress.GetClientRect(&rect);

	CPaintDC dcLabel(&lblAddress);
	dcLabel.SelectObject(normal);	
	dcLabel.SetTextColor(RGB(0,0,0));
	dcLabel.SetBkMode(TRANSPARENT);


	WCHAR wdata[50];
	MultiToUnicode(text.GetBuffer(),wdata);
	CString line1,line2,line3;
	int wlength=lstrlenW(wdata);
	if (wlength > 17) {
		WCHAR wline1[17];
		for (int i=0; i<17; i++) {
			wline1[i] = wdata[i];
		}
		char cline1[50], cline2[50], cline3[50];
		UnicodeToMulti(wline1,17,cline1);
		line1.Format("%s", cline1);

		if (wlength > 31) {
			WCHAR wline2[14];
			for (int i=17; i<31; i++) {
				wline2[i-17] = wdata[i];
			}
			UnicodeToMulti(wline2,14,cline2);
			line2.Format("%s", cline2);

			WCHAR wline3[19];
			for (int i=31; i<wlength; i++) {
				wline3[i-31] = wdata[i];
			}
			UnicodeToMulti(wline3,wlength-31,cline3);
			line3.Format("%s", cline3);
		} else {
			WCHAR wline2[14];
			for (int i=17; i<wlength; i++) {
				wline2[i-17] = wdata[i];
			}
			UnicodeToMulti(wline2,wlength-17,cline2);
			line2.Format("%s", cline2);
		}
	}
	
	if (line1.GetLength() > 0) {
		dcLabel.DrawText(line1,&rect, DT_LEFT);
	}
	if (line2.GetLength() > 0) {
		rect.left+=32;
		rect.top+=15;
		dcLabel.DrawText(line2,&rect, DT_LEFT);
	}
	if (line3.GetLength() > 0) {
		rect.top+=15;
		dcLabel.DrawText(line3,&rect, DT_LEFT);
	}
}


void CfacerecoDlg::DrawVersion() {
	CFont font;
	CRect rect;
	CString text;
	COLORREF white = RGB(255,255,255);
	text.Format("Ver %s", version);
	font.CreatePointFont(80,_T("微软雅黑"),NULL);

	btnVersion.GetClientRect(&rect);
	CPaintDC dcLabel(&btnVersion);
	dcLabel.SelectObject(font);
	dcLabel.SetTextColor(white);
	dcLabel.SetBkMode(TRANSPARENT);
	dcLabel.DrawText(text,&rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
}

void CfacerecoDlg::DrawLabel(CStatic* label, CFont* font, int prop) {
	DrawLabel(label, font, prop,  RGB(0,0,0));
}


void CfacerecoDlg::DrawLabel(CStatic* label, CFont* font, int prop, COLORREF clr) {
	CRect rect;
	CString text;

	label->GetWindowTextA(text);
	label->GetClientRect(&rect);
	CPaintDC dcLabel(label);
	dcLabel.SelectObject(font);
	dcLabel.SetTextColor(clr);
	dcLabel.SetBkMode(TRANSPARENT);
	dcLabel.DrawText(text,&rect, prop);
}
void CfacerecoDlg::DetectIDFace(bool fromFile) {
#ifdef _SHANREN_TEST_H_
	SRTest test;
#endif
	Mat im;
	if (fromFile) {
		doComparing = true;
		SendProtect("status:comparing");
		pauzeReading = false;
		printf("start detect\n");
	} else {
		int ret = Authenticate();
		if (ret != 1)  {
			return;
		}
			
		doComparing = true;
		SendProtect("status:comparing");
		pauzeReading = false;

		doGetImage = false;
		ifGetImage = false;

		char tname[31];
		char tgender[3];
		char tfolk[10];
		char tbirthday[9];
		char tcode[19];
		char taddress[300];
		char tagency[100];
		char texpireStart[9];
		char texpireEnd[9];

		try {
			int res = ReadBaseInfosPhoto(tname,tgender,tfolk,tbirthday,tcode,taddress,tagency,texpireStart,texpireEnd,storagePath.GetBuffer());
			if (res != 1) {
				doComparing = false;
				return;
			}

			DeleteFile(fullFile);
			DeleteFile(idDataFile);
			DeleteFile(avatarFile);
			DeleteFile(handFaceFile);

			Clear();

			TrimAry(tname, 31);
			TrimAry(tgender, 3);
			TrimAry(tfolk, 10);
			TrimAry(tbirthday, 9);
			TrimAry(tcode, 19);
			TrimAry(taddress, 300);
			TrimAry(tagency, 100);
			TrimAry(texpireStart, 9);
			TrimAry(texpireEnd, 9);

		} catch (...) {
			doComparing = false;
			return;
		}

		name=tname;
		gender=tgender;
		nation=tfolk;
		bornday=tbirthday;
		location=taddress;
		idcode=tcode;
		polistation=tagency;
		startday=texpireStart;
		enday=texpireEnd;
	}

	try {
		im = imread(idFile.GetBuffer());
		cv::Mat dst =  Mat(cv::Size(faceWidthConst, faceHeightConst), CV_32S);;
		cv::resize(im, dst, cv::Size(faceWidthConst, faceHeightConst));
		cv::imwrite(idFile.GetBuffer(),dst);
		im = imread(idFile.GetBuffer());
	
	} catch(...) {
		doComparing = false;
		return;
	}
	if (im.empty()) {
		doComparing = false;
		WriteLog("read photo.bmp failed.");
		return;
	}
		
	
	int nWidth = im.cols;
	int nHeight = im.rows;

	long lLength = faceWidthConst * faceHeightConst * 3;
	BITMAPINFOHEADER bih;
	ContructBih(faceWidthConst,faceHeightConst,bih);
	BYTE* pCamBuf = new BYTE[lLength];
	memcpy(pCamBuf,im.data, nWidth*nHeight*3);

			WaitForSingleObject( mtxDetect_, INFINITE );  
	int ret = DetectFace(pCamBuf, nWidth, nHeight, idFeature, false, true, true, false);
	
			ReleaseMutex( mtxDetect_ );
	if (ret == 1) {
		picIDCard.SetBitmap((HBITMAP)LoadImage(NULL,idFile,IMAGE_BITMAP,0,0,LR_LOADFROMFILE));

/*
	CString name;
	CString gender;
	CString nation;
	CString bornday;
	CString location;
	CString idcode;
	CString polistation;
	CString startday;
	CString enday;
	CString similarity;
*/

		CString strName;
		strName.Format("姓名：%s", name);
		CString strGender;
		strGender.Format("性别：%s   民族：%s", gender, nation);
		CString strBornday;
		strBornday.Format("出生：%s年%s月%s日", bornday.Left(4),bornday.Mid(4,2),bornday.Mid(6,2));
		CString strAddress;
		strAddress.Format("住址：%s", location);
		CString strIdcode;
		strIdcode.Format("公民身份证号：%s", idcode);
		CString strOrgn;
		strOrgn.Format("签发机关：%s", polistation);
		CString strExpire;

		if (enday.Left(1) != "1" && enday.Left(1) != "2") {
			strExpire.Format("有效期限：%s.%s.%s-%s",startday.Left(4),startday.Mid(4,2),startday.Mid(6,2),enday);
			picIDState.SetBitmap(hValid);

		} else {
			strExpire.Format("有效期限：%s.%s.%s-%s.%s.%s",startday.Left(4),startday.Mid(4,2),startday.Mid(6,2),enday.Left(4),enday.Mid(4,2),enday.Mid(6,2));
			CTime endtime(atoi(enday.Left(4)),atoi(enday.Mid(4,2)),atoi(enday.Mid(6,2)),0,0,0);
			CTime nowtime = CTime::GetCurrentTime();
			if (nowtime > endtime) {
				// 失效
				picIDState.SetBitmap(hInvalid);
				PlaySound(TEXT("res/invalid.wav"), NULL, SND_FILENAME | SND_ASYNC);

			} else {
				picIDState.SetBitmap(hValid);
			}
		}

		picIDCard.ShowWindow(false);
		lblName.SetWindowTextA(strName);
		lblGender.SetWindowTextA(strGender);
		lblBirthday.SetWindowTextA(strBornday);
		lblAddress.SetWindowTextA(strAddress);
		lblIDCode.SetWindowTextA(strIdcode);
		lblOrgn.SetWindowTextA(strOrgn);
		lblExpire.SetWindowTextA(strExpire);

		picIDState.ShowWindow(false);
		
		//picVideo.SetBitmap(hVideo);

		updateState("请正视摄像头...   ");
		PlaySound(TEXT("res/remind.wav"), NULL, SND_FILENAME | SND_ASYNC);
		lblMatch.SetWindowTextA("识别中");
		spiritDlg->setState(2);

		//Invalidate(true);
		//UpdateWindow();

		// 缓存证件特征值
		idFeatureHex = BYTEToHex(idFeature, 2560);

		// 重置参数
		maxScore = 0;
		matchCount = 0;
		// 开始监测人脸
		videoDetect = true;
		pauseVideo = false;
		scanFaceTime = CTime::GetCurrentTime();
		//ShowMain();
		
		WriteLog("Start capture...");
		Active();

	} else {
		WriteLog("Scan face failed...");
		doComparing = false;
		pauzeFilting = false;
		HideMain();
	}
	if (pCamBuf)
		delete []pCamBuf;

#ifdef _SHANREN_TEST_H_
	std::stringstream log_info_stream;
	log_info_stream << test.getElapsed();

	std::string log_info;
	log_info_stream >> log_info;
	log_info += "times, [Function:DetectIDFace] elapsed time.";

	WriteLog((char*)log_info.c_str());
#endif
}

int CfacerecoDlg::DetectFace(BYTE* pBuf, int width, int height, BYTE* pFeature, bool detectOnly, bool drawFaceBox, bool genFeature, bool checkAngle) {
#ifdef _SHANREN_TEST_H_
	SRTest test;
#endif
	// 开始人脸检测
	THFI_FacePos facePos[1];
	facePos[0].dwReserved = (DWORD) new BYTE[512];
	int face = THFI_DetectFace(0, pBuf, 24, width, height, facePos, 1);

	FaceAngle angle = facePos->fAngle;
	if (!checkAngle || face > 0 && abs(angle.yaw) <= 20 && abs(angle.pitch) <= 20) {
		if (genFeature) {
			memset(pFeature,0,featureSize);
			int ret = EF_Extract(0, pBuf, width, height, 3, (DWORD)&facePos[0], pFeature);
			if (!ret) {
				return -1;
			}
		} 

		if (!detectOnly) {
			// 获取人脸部分图片
			RECT rcFace=facePos[0].rcFace;
			int faceWidth = rcFace.right - rcFace.left;
			int faceHeight = rcFace.bottom - rcFace.top;
			BYTE* pFaceBuf = new BYTE[faceWidth*faceHeight*3];
			::GetPartImage(pBuf,width,height,pFaceBuf,rcFace.left,rcFace.top,rcFace.right,rcFace.bottom);

			// 垂直人脸镜像
			LONG lineBytes;
			lineBytes = WIDTHBYTES(faceWidth *24);
			BYTE* pVertical = new BYTE[lineBytes*faceHeight];
			for(int i=0;i<faceHeight;i++)
			{
				for(int j=0;j<faceWidth;j++)
				{
					pVertical[i*lineBytes+j*3+0]=pFaceBuf[(faceHeight-1-i)*faceWidth*3+j*3+0];
					pVertical[i*lineBytes+j*3+1]=pFaceBuf[(faceHeight-1-i)*faceWidth*3+j*3+1];
					pVertical[i*lineBytes+j*3+2]=pFaceBuf[(faceHeight-1-i)*faceWidth*3+j*3+2];
				}
			}
			delete []pFaceBuf;
			
			// 缩放至 102 x 126 大小人脸
			BITMAPINFOHEADER bih;
			::ContructBih(faceWidth,faceHeight,bih);
			BYTE* pFace=new BYTE[WIDTHBYTES(faceWidthConst *24)*faceHeightConst];
			::ResampleBmp(bih,pVertical,faceWidthConst,faceHeightConst,pFace);
			delete []pVertical;

			// 缓存人脸头像
			bmpFace.DeleteObject();
			GetCBitmap(&bih, pFace, bmpFace);

			delete []pFace;

		}

		if (drawFaceBox) {
			DrawFaceRects(pBuf,width,height,&facePos[0].rcFace,1,RGB(0,255,0),4);
		}
	
		return 1;

#ifdef _SHANREN_TEST_H_
		std::stringstream log_info_stream;
		log_info_stream << test.getElapsed();

		std::string log_info;
		log_info_stream >> log_info;
		log_info += "times, [Function:DetectIDFace] elapsed time.";

		WriteLog((char*)log_info.c_str());
#endif
	}

	delete [](BYTE*)facePos[0].dwReserved;

	return 0;
}

void CfacerecoDlg::validPass() {
	PlaySound(TEXT("res/pass.wav"), NULL, SND_FILENAME | SND_ASYNC);
	CString msg;
	msg.Format("%s认证通过识别度%d%%", name, (int)maxScore);
	stateDlg->setState(msg.GetBuffer(), true);
	stateDlg->ShowWindow(false);
	strResultTip_ = "认证通过";
	clResultTip_ = RGB(95,255,120);

	btnRefuse.ShowWindow(false);
	btnConfirm.ShowWindow(false);
	picIconPass.ShowWindow(false);
	
	//Invalidate(true); 
	//UpdateWindow();

	if (isIdlessCompare) {
		rfdSuccessD(idMetadata);
		isIdlessCompare = false;
	} else {
		rfdSuccess();
	}
	//UploadInfo(type);
	// 识别匹配结束
	SendProtect("status:compared");
	Sleep(200);

	HideMain();
	doComparing = false;
	pauzeFilting = false;
	pauzeReading = false;
}

void CfacerecoDlg::validRefuse(char *msg) {
	PlaySound(TEXT("res/refuse.wav"), NULL, SND_FILENAME | SND_ASYNC);
	stateDlg->ShowWindow(SW_HIDE);
	stateDlg->setState(msg, false);
	stateDlg->ShowWindow(false);
	strResultTip_ = msg;
	clResultTip_=RGB(255,87,16);

	btnRefuse.ShowWindow(false);
	btnConfirm.ShowWindow(false);
	picIconRefuse.ShowWindow(false);
	//Invalidate(true);
//	UpdateWindow();


	if (isIdlessCompare) {
		isIdlessCompare = false;
	}
	rfdSuccess();
	// 识别匹配结束
	SendProtect("status:compared");
	Sleep(200);

	HideMain();
	doComparing = false;
	pauzeFilting = false;
	pauzeReading = false;
}

void CfacerecoDlg::updateState(char* msg) {
	lblState.SetWindowTextA(msg);
	strTopTip_ = msg;
//	Invalidate(true); 
//	UpdateWindow();
}

// 弹出消息框
void CfacerecoDlg::Message(char* msg) {
	::MessageBox(this->m_hWnd, msg, "提示", MB_ICONWARNING);
}

void CfacerecoDlg::Clear() {
	maxScore = 0;

	//if (initType == 1 || initType == 2) {
		picIDCard.SetBitmap(hIdCard);
		picSnapshoot_.SetBitmap(hIdCard);

		//lblCardMatch.SetWindowTextA("相似度");

		//picIDCard.ShowWindow(false);

		strResultTip_ = "";

		lblName.SetWindowTextA("");
		lblGender.SetWindowTextA("");
		lblBirthday.SetWindowTextA("");
		lblAddress.SetWindowTextA("");
		lblIDCode.SetWindowTextA("");
		lblOrgn.SetWindowTextA("");
		lblExpire.SetWindowTextA("");
 
		lblScore.SetWindowTextA("        ");
		lblPercent.SetWindowTextA("");
		lblMatch.SetWindowTextA("");

		btnConfirm.ShowWindow(false);
		btnRefuse.ShowWindow(false);

		picIconPass.ShowWindow(false);
		picIconRefuse.ShowWindow(false);
		picIDState.ShowWindow(false);
	//}

//	Invalidate(true);
//	UpdateWindow();
}

void CfacerecoDlg::SendSocket(char* msg) {
}


// 发送消息
void CfacerecoDlg::SendProtect(char* msg)  {
	}
 
void CfacerecoDlg::HideMain() {
	// 停止录像
	//pauzeReading = true;
	//pauseVideo = true;
	//ShowWindow(SW_HIDE);
	pauseVideo = false;

	
	//picVideo.SetBitmap(hVideo);
	spiritDlg->setState(1);

	//Clear();
}


void CfacerecoDlg::Exit() {

	doDetecting = false;

	videoDetect = false;

	doListening = false;

	doReceiving = false;

	doReading = false;

	doFilting = false;

	doHeartbeat = false;
	Sleep(2000);
	WriteLog("Stop process...");

	WritePrivateProfileString("config","token", token, settingFile);

	UnhookWindowsHookEx(m_hHook);
	m_hHook = NULL;

	bmpFace.DeleteObject();
	bmpStoreFace.DeleteObject();
	bmpStoreVideo.DeleteObject();
	bmpStoreFaceByHand.DeleteObject();
	bmpStoreBoxedFace.DeleteObject();

	DeleteObject(hVideo);
	DeleteObject(hIdCard);
	DeleteObject(hClose);
	DeleteObject(hPass);
	DeleteObject(hRefuse);
	DeleteObject(hIconPass);
	DeleteObject(hIconRefuse);
	DeleteObject(hSubmit);
	DeleteObject(hManCheck);
	DeleteObject(hGirlCheck);
	DeleteObject(hManUncheck);
	DeleteObject(hGirlUncheck);
	DeleteObject(hInvalid);


	remove(fullFile);
	remove(avatarFile);
	remove(idFile);
	remove(handFaceFile);
	remove(storagePath + "\\1.jpg");
	remove(storagePath + "\\2.jpg");
	remove(storagePath + "\\photo.bmp");
	WriteLog("Remove infos success...");

	if(pCapture)
	{
		pCapture->Stop();
		delete pCapture;
		pCapture=NULL;

		WriteLog("Stop capture success...");
	}

	//closesocket(receiverProtect);
	//closesocket(senderProtect);

	if ((initType == 1 || initType == 2) && initFilter) {
		rfdFailed();
		rfdRelease();

		unloadDriver("ReadFilter");
		WriteLog("Stop filter success...");
	}

	WSACleanup();

	delete []idFeature;
	idFeature = NULL;

	if (apiDll != NULL) {
		FreeLibrary(apiDll);
		WriteLog("Free component success...");
	}

	if (initType == 2) {
		if (visitorDll != NULL) {
			FreeLibrary(visitorDll);
		}
	}
	CloseComm();
	WriteLog("Close comm success");

	THFI_Release();
	EF_Release();
	WriteLog("Release video success");

	DeleteCriticalSection(&g_CriticalSection);
	Shell_NotifyIcon(NIM_DELETE,&notifyIcon);

	ReleasePublic();
	OnCancel();
}


void CfacerecoDlg::Update(char* newVer) {
	return;
	bool bRet;

	char urls[10240];
	bRet = apiActiveDownloadFile(appkey, appsecret, version, urls);
	if (bRet) {
		CStringArray ary;
		SplitCString(urls, ary, ';');
		CString fileName;
		for (int i = 0; i < ary.GetSize(); i++) {
			CString item = ary.ElementAt(i);

			int index;
			CString dnFile, cpFile;
			CString downFile;
			CString downPath;

			dnFile = item;
			dnFile.Replace("/", "\\");
			dnFile = dnFile.Right(dnFile.GetLength() - 9);
			downFile.Format("%s%s",storagePath, dnFile);

			index = downFile.ReverseFind('\\');
			downPath = downFile.Left(index);
			::CreateDirectory(downPath,NULL);

			CString url;
			url.Format("http://facereco.eshanren.com%s", item);
			try {
				int res = DownloadFile(url, downFile);
				if (res != 0) {
					Message("更新失败, 请确保网络已连接");
					break;
				}
			} catch (...) {
				Message("更新失败, 请确保网络已连接");
				break;
			}
		}
	}

	WritePrivateProfileString("config","newver", newVer, storeConfig);
	if (!doComparing)
		Exit();
}

void CfacerecoDlg::CheckSource() {
	char url[256];
	char check[256];
	sourceType = apiActiveSource(appkey.GetBuffer(), appsecret.GetBuffer(), token.GetBuffer(), url, check);
	if (sourceType != 0) {
		CString notation;
		notation.Format("数源:%s%s", token, url);
		string utf8Str;
		MultiToUTF8(notation.GetBuffer(), utf8Str);

		char tmp[40];
		apiMd5((unsigned char *)utf8Str.c_str(),utf8Str.length(), tmp);

		if (strcmp(tmp, check) == 0) {
			CString theUrl(url);
			if (theUrl.Left(44).Compare("http://face.hongyaer.com/admin.php/Admin/Api") == 0)  {
				clientSourceUrl.Format("%s/UploadVisitInfo.html?code=%s", url, placeCode.GetBuffer());
				clientHostUrl.Format("%s/GetAllVisitedInfo.html", url);

				if (initType == 2) {
					fstream file;
					file.open(hosterInfoFile, ios::in);
					if (!file) {
						file.close();

						apiThirdHosterInfo(clientHostUrl, hosterInfoFile.GetBuffer());
					} else {
						file.close();
					}
				}
			} else {
				int i = theUrl.Mid(7).Find("/") + 7;
				clientSourceUrl.Format("%s/post/%s", url, placeCode.GetBuffer());
				idlessUrl.Format("%s/api/faceno/post/%s",theUrl.Left(i), placeCode.GetBuffer());
			}
		}
	}
}

void CfacerecoDlg::CheckVersion()  {
	int iRet;
	char newVer[30];
	CString msg;
	iRet = apiActiveCheckVersion(appkey, appsecret, version.GetBuffer(), identity.GetBuffer(), newVer);
	if (iRet == 1) {
		if (version.Compare(newVer) != 0) {
			apiAboutUs(appkey.GetBuffer(), appsecret.GetBuffer(), identity.GetBuffer(), true);
			//msg.Format("%s, %s", version, newVer);
			//Message(msg.GetBuffer());
		}
	}
}

void CfacerecoDlg::CheckFunction() {
	bool bRet;
	char idls[256];
	char check[256];
	bRet = apiActiveFunction(appkey.GetBuffer(), appsecret.GetBuffer(), token.GetBuffer(), idls, check);
	if (bRet) {
		CString notation;
		notation.Format("功能:%s%s", token, idls);
		string utf8Str;
		MultiToUTF8(notation.GetBuffer(), utf8Str);

		char tmp[40];
		apiMd5((unsigned char *)utf8Str.c_str(),utf8Str.length(), tmp);
		if (strcmp(tmp, check) == 0) {
			if (strlen(idls) > 0) {
				ifIdless = true;
				idlessQueryUrl.Format("%s/api/compare/get", idls);
				spiritDlg->activeIdless();
				stateDlg->activeIdless();
			}
		}
	}
}

void CfacerecoDlg::ShowVisit() {
	if (initType == 2) {
		char theId[50];
		char thePhone[50];
		char theReason[250];

		ShowWindow(SW_HIDE);
		int num = apiVisitor(thePhone, theReason, theId);
		
		hoster_id.Format("%s", theId);
		reason.Format("%s", theReason);
		phone_num.Format("%s", thePhone);
	}
}

void CfacerecoDlg::Active() {
	//updateState("请刷身份证...");
	SetActiveWindow();
	ShowWindow(SW_SHOW);
	SetActiveWindow();
	pauzeReading = false;
	pauseVideo = false;
}

void CfacerecoDlg::StartDetect() {

	//if (!pauzeDetecting || doComparing) return;

	//CFileFind find;
	//CTime lastTime = NULL;
	//CTime tempTime;
	//CString fileName;
	//CString filePath = "";
	//bool bf = find.FindFile(detectPath);
	//while (bf) {
	//	bf = find.FindNextFile();
	//	if(!find.IsDots()) {
	//		remove(find.GetFilePath());
	//	}
	//}

	//pauzeDetecting = false;
	//detectingTime = CTime::GetCurrentTime();
	//maxScore = 0;

	spiritDlg->setState(2);
}



void CfacerecoDlg::SetSecret(char *theKey, char *theSecret, char *theIdentity) {
	appkey = theKey;
	appsecret = theSecret;
	identity = theIdentity;
}

void CfacerecoDlg::ActiveIdless() {
	if (initing || doComparing) return;

	// clear id feature
	memset(idFeature, 0, featureSize);

	doComparing = true;
	pauzeFilting = true;
	idlessDlg->Show(true);
}

int CfacerecoDlg::QueryIdlessInfo(char* data) {

	CHAR tname[50];
	CHAR tgender[20];
	CHAR tnation[20];
	CHAR tbornday[20];
	CHAR taddress[80];
	CHAR tidcode[20];
	CHAR torgn[50];
	CHAR tstartday[20];
	CHAR tendday[20];
	CHAR tidData[2576];
	CHAR tfaceFile[10240];
	CHAR tfaceFeature[5200];
	memset(tname,0,50);
	memset(tgender,0,20);
	memset(tnation,0,20);
	memset(tbornday,0,20);
	memset(taddress,0,80);
	memset(tidcode,0,20);
	memset(torgn,0,50);
	memset(tstartday,0,20);
	memset(tendday,0,20);
	memset(tidData,0,2576);
	memset(tfaceFile,0,10240);
	memset(tfaceFeature,0,5200);



	bool bRet;
	CString url;
	url.Format("%s/%s", idlessQueryUrl, data);
	bRet = apiActiveQueryIdless(appkey.GetBuffer(), appsecret.GetBuffer(), url.GetBuffer(), identity.GetBuffer(), tname, tgender, tnation, tidcode, taddress, tstartday, tendday, tbornday, torgn, tidData, tfaceFeature, tfaceFile);
	if (bRet) {
		tidData[2576] = '\0';
		name.Format("%s", tname);
		gender.Format("%s", tgender);
		nation.Format("%s", tnation);
		bornday.Format("%s", tbornday);
		location.Format("%s", taddress);
		idcode.Format("%s", tidcode);
		polistation.Format("%s", torgn);
		startday.Format("%s", tstartday);
		enday.Format("%s", tendday);
		internet_bar_id="0";

		name.Trim();
		gender.Trim();
		nation.Trim();
		bornday.Trim();
		location.Trim();
		idcode.Trim();
		polistation.Trim();
		startday.Trim();
		enday.Trim();

		HexToBYTE(tidData, (BYTE*)idMetadata);

		memset(tname,0,50);
		memset(tnation,0,20);	
		memset(taddress,0,80);
		memset(torgn,0,50);
		
		UTF8ToMB(name.GetBuffer(), tname, name.GetLength());
		name.Format("%s", tname);

	
		UTF8ToMB(nation.GetBuffer(), tnation, nation.GetLength());
		nation.Format("%s", tnation);
	
		UTF8ToMB(location.GetBuffer(), taddress, location.GetLength());
		location.Format("%s", taddress);
	
		UTF8ToMB(polistation.GetBuffer(), torgn, polistation.GetLength());
		polistation.Format("%s", torgn);
	

		//name = UTF8Convert(name,CP_UTF8,CP_ACP);

		CString strName;
		strName.Format("姓名：%s", name);
		CString strGender;
		strGender.Format("性别：%s   民族：%s", gender, nation);
		CString strBornday;
		strBornday.Format("出生：%s年%s月%s日", bornday.Left(4),bornday.Mid(5,2),bornday.Mid(8,2));
		CString strAddress;
		strAddress.Format("住址：%s", location);
		CString strIdcode;
		strIdcode.Format("公民身份证号：%s", idcode);
		CString strOrgn;
		strOrgn.Format("签发机关：%s", polistation);
		CString strExpire;

		if (enday.Left(1) != "1" && enday.Left(1) != "2") {
			strExpire.Format("有效期限：%s.%s.%s-%s",startday.Left(4),startday.Mid(5,2),startday.Mid(8,2),"长期");
			picIDState.SetBitmap(hValid);

		} else {
			strExpire.Format("有效期限：%s.%s.%s-%s.%s.%s",startday.Left(4),startday.Mid(5,2),startday.Mid(8,2),enday.Left(4),enday.Mid(5,2),enday.Mid(8,2));
			CTime endtime(atoi(enday.Left(4)),atoi(enday.Mid(5,2)),atoi(enday.Mid(8,2)),0,0,0);
			CTime nowtime = CTime::GetCurrentTime();
			if (nowtime > endtime) {
				picIDState.SetBitmap(hInvalid);
			} else {
				picIDState.SetBitmap(hValid);
			}
		}

		picIDCard.ShowWindow(false);
		lblName.SetWindowTextA(strName);
		lblGender.SetWindowTextA(strGender);
		lblBirthday.SetWindowTextA(strBornday);
		lblAddress.SetWindowTextA(strAddress);
		lblIDCode.SetWindowTextA(strIdcode);
		lblOrgn.SetWindowTextA(strOrgn);
		lblExpire.SetWindowTextA(strExpire);

		idFeatureHex.Format("%s", tfaceFeature);
		HexToBYTE(idFeatureHex, idFeature);

		CString fileDataHex;
		fileDataHex.Format("%s", tfaceFile);

		int dataLength = fileDataHex.GetLength() / 2;
		BYTE* fileData = new BYTE[dataLength];
		HexToBYTE(fileDataHex, fileData);

		ofstream ostream(idFile, std::ios::binary);
		ostream.write((const char *)fileData, dataLength);
		ostream.flush();
		ostream.close();

		delete []fileData;

		LoadBmpFromFile(idFile, bmpIDAvatar);
		picIDCard.SetBitmap(bmpIDAvatar);
		picIDCard.ShowWindow(false);

		return 1;
	}

	return 0;
}

void CfacerecoDlg::LeaveIdless() {
	doComparing = false;
	pauzeFilting = false;
	idlessDlg->Show(false);
}

void CfacerecoDlg::StartIdless() {

	updateState("请正视摄像头...   ");
	lblMatch.SetWindowTextA("识别中");
	SetActiveWindow();
	ShowWindow(SW_SHOW);
	SetActiveWindow();
	spiritDlg->setState(2);

	// 重置参数
	maxScore = 0;
	matchCount = 0;
	// 确认为无证比对
	isIdlessCompare = true;
	// 开始监测人脸
	videoDetect = true;
	pauseVideo = false;
	scanFaceTime = CTime::GetCurrentTime();

	idlessDlg->Show(false);
}

UINT CfacerecoDlg::thread_VerifyTrainInfo(LPVOID lpVoid) {
	::CoInitialize(NULL);
	CfacerecoDlg* pDlg=(CfacerecoDlg*)lpVoid;
	pDlg->verifyTrainInfo();
	//
	//int bret = apiVerifyTrainInfo(pDlg->appkey, pDlg->appsecret, pDlg->verifyUrl_, pDlg->placeCode_.GetBuffer(), pDlg->verifyCode_.GetBuffer());
	//if (bret) {
	//	WriteLog("verify train place and verify code succeed.");
	//}
	//else {
	//	WriteLog("verify train place and verify code failed.");
	//}


	return 1;
}

void CfacerecoDlg::verifyTrainInfo(void) {
	
	int bret = apiVerifyTrainInfo(appkey, appsecret, verifyUrl_, placeCode_.GetBuffer(), verifyCode_.GetBuffer());
	if (bret) {
		WriteLog("verify train place and verify code succeed.");
	}
	else {
		WriteLog("verify train place and verify code failed.");
	}

	BOOL bRet = DoHeartbeat();
	int iRet;
	if (!bRet) {
		Exit();
	}
#if 1 // 版本自动更新
	CheckSource();
	CheckFunction();
#endif 

	::CoUninitialize();

}

void CfacerecoDlg::initClyRes(void) {

	// 视频中的顶部提示
	strTopTip_ = "请刷身份证...";
	clTopTip_ = RGB(0,255,136);

	// 视频中心验证结果提示
	strResultTip_ = "";
	clResultTip_ = RGB(255,87,0);

	// 创建 DetectFace 函数的锁
    mtxDetect_ = CreateMutex( NULL,  
        FALSE,   
        NULL );  


	return ;
}

//extern "C" __declspec(dllexport) CfacerecoDlg* ShowDialog() {
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	CfacerecoDlg* pDlg = new CfacerecoDlg;
//	pFacerecoDlg = pDlg;
//
//	return pDlg;
//}

extern "C"  __declspec(dllexport) void ShowDlg(char *appkey, char *appsecret, char *identity) {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CfacerecoDlg* pDlg = new CfacerecoDlg;
	pFacerecoDlg = pDlg;
	// ("4adfd670ef2cbc72b9a3aceacb73c1c8", "5fefb260863ad70bfcf142bb84c4c8f6");
	// ("9bf059d8c33c205265475074c0d442a7", "5951d00b241cf4d220eddebb0eea2b47");
	pFacerecoDlg->SetSecret("4adfd670ef2cbc72b9a3aceacb73c1c8", "5fefb260863ad70bfcf142bb84c4c8f6", identity);
	pFacerecoDlg->DoModal();
}
