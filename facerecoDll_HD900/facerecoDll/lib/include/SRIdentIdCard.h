// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SRIDENTIDCARD_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SRIDENTIDCARD_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifndef __SHANREN_IDENTIDCARD_H__
#define __SHANREN_IDENTIDCARD_H__

#ifdef SRIDENTIDCARD_EXPORTS
#define SRIDENTIDCARD_API __declspec(dllexport)
#else
#define SRIDENTIDCARD_API __declspec(dllimport)
#endif


#ifndef  _WIN32
#define SRIDCARD  __SRIDCARD
#else
#define SRIDCARD
#endif


#ifdef __cplusplus
extern "C"{
#endif

	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_SetMaxRFByte(int iPortID,unsigned char ucByte,int bIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_GetCOMBaud(int iComID,unsigned int *puiBaud);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_SetCOMBaud(int iComID,unsigned int  uiCurrBaud,unsigned int  uiSetBaud);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_OpenPort(int iPortID);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ClosePort(int iPortID);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_GetErrorString(int ErrorCode, char * ErrorString);

	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_GetSAMStatus(int iPortID,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ResetSAM(int iPortID,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_GetSAMID(int iPortID,unsigned char *pucSAMID,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_GetSAMIDToStr(int iPortID,char *pcSAMID,int iIfOpen);

	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_StartFindIDCard(int iPortID,unsigned char *pucIIN,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_SelectIDCard(int iPortID,unsigned char *pucSN,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ReadBaseMsg(int iPortID,unsigned char * pucCHMsg,unsigned int *	puiCHMsgLen,unsigned char * pucPHMsg,unsigned int  *puiPHMsgLen,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ReadFPMsg(int iPortID,unsigned char * pucFPMsg,unsigned int *puiFPMsgLen,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ReadIINSNDN(int iPortID,unsigned char * pucIINSNDN,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ReadNewAppMsg(int iPortID,unsigned char * pucAppMsg,unsigned int *puiAppMsgLen,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ReadMngInfo(int iPortID, unsigned char * pucManageMsg,int iIfOpen);

	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ReadBaseMsgToFile(int iPortID,char * pcCHMsgFileName,unsigned int *puiCHMsgFileLen,char * pcPHMsgFileName,unsigned int  *puiPHMsgFileLen,int iIfOpen);
	SRIDENTIDCARD_API	int SRIDCARD SR_SDT_ReadIINSNDNToASCII(int iPortID, unsigned char *pucRecvData,int iIfOpen);

	SRIDENTIDCARD_API int SRIDCARD  SR_InitComm( int iPort );
	SRIDENTIDCARD_API int SRIDCARD  SR_CloseComm(void);
	SRIDENTIDCARD_API int SRIDCARD  SR_Authenticate(void);

	SRIDENTIDCARD_API int SRIDCARD  SR_ReadBaseMsgWPhoto( unsigned char * pMsg, int * len,char * directory);
	SRIDENTIDCARD_API int SRIDCARD  SR_ReadBaseMsgPhoto( unsigned char * pMsg, int * len,char * directory);
	SRIDENTIDCARD_API int SRIDCARD  SR_ReadBaseInfosPhoto( char * Name, char * Gender, char * Folk,
		char *BirthDay, char * Code, char * Address,
		char *Agency, char * ExpireStart,char* ExpireEnd,char * directory);

	SRIDENTIDCARD_API int SRIDCARD  SR_ReadBaseMsgW( unsigned char * pMsg, int * len);
	SRIDENTIDCARD_API int SRIDCARD  SR_ReadBaseMsg( unsigned char * pMsg, int * len);
	SRIDENTIDCARD_API int SRIDCARD  SR_ReadBaseInfos( char * Name, char * Gender, char * Folk,
		char *BirthDay, char * Code, char * Address,
		char *Agency, char * ExpireStart,char* ExpireEnd);

	SRIDENTIDCARD_API int SRIDCARD  SR_ReadNewAppMsgW( unsigned char * pMsg, int * num );
	SRIDENTIDCARD_API int SRIDCARD  SR_ReadNewAppMsg( unsigned char * pMsg, int * len );
	SRIDENTIDCARD_API int SRIDCARD SR_ReadNewAppInfos( unsigned char * addr1, unsigned char * addr2,
		unsigned char * addr3, unsigned char * addr4,
		int * num );

	SRIDENTIDCARD_API int SRIDCARD SR_ReadIINSNDN( char * pMsg );

	SRIDENTIDCARD_API int SRIDCARD SR_GetSAMIDToStr(char *pcSAMID );

	SRIDENTIDCARD_API int SRIDCARD SR_Routon_StartFindIDCard( unsigned char *err );
	SRIDENTIDCARD_API int SRIDCARD SR_Routon_ReadBaseMsg( unsigned char * pucCHMsg,unsigned int *	puiCHMsgLen );
	SRIDENTIDCARD_API int SRIDCARD SR_Routon_BeepLED(bool BeepON,bool LEDON,unsigned int duration);
	SRIDENTIDCARD_API int SRIDCARD SR_Routon_SetNewVersion(bool f);
	SRIDENTIDCARD_API int SRIDCARD SR_Routon_IC_FindCard(void);
	SRIDENTIDCARD_API int SRIDCARD SR_Routon_IC_HL_ReadCardSN(char * SN);
	SRIDENTIDCARD_API int SRIDCARD SR_Routon_IC_HL_ReadCard (int SID,int BID,int KeyType,unsigned char * Key,unsigned char * data);
	SRIDENTIDCARD_API int SRIDCARD SR_Routon_IC_HL_WriteCard (int SID,int BID,int KeyType,unsigned char * Key,unsigned char * data);

	SRIDENTIDCARD_API int SRIDCARD SR_dc_init(int port,long baud);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_exit(int dev);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_request(int icdev,unsigned char _Mode,unsigned int *TagType);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_anticoll(int icdev,unsigned char _Bcnt,unsigned long *_Snr);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_select(int icdev,unsigned long _Snr,unsigned char *_Size);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_authentication_passaddr(int icdev, unsigned char _Mode, unsigned char Addr, unsigned char *passbuff);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_read(int icdev,unsigned char _Adr,unsigned char *_Data);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_write(int icdev,unsigned char _Adr,unsigned char *_Data); 
	SRIDENTIDCARD_API int SRIDCARD SR_dc_halt(int icdev);
	SRIDENTIDCARD_API int SRIDCARD SR_dc_BeepLED(int icdev,bool BeepON,bool LEDON,unsigned int duration);
	SRIDENTIDCARD_API int SRIDCARD SR_HID_BeepLED(bool BeepON,bool LEDON,unsigned int duration);

#ifdef __cplusplus
}
//#endif 
#endif

#endif // !__SHANREN_IDENTIDCARD_H__