#pragma once

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
using namespace cv;

#include <atlimage.h>

#include "facerecoDlg.h"
#include "ResultDlg.h"
#include "SpiritDlg.h"

#include "ijl15\\IjlLib.h"
#include "../include/ThFacerCap.h"

#include "../include/THFaceImage_i.h"
#pragma comment (lib, "../include/THFaceImage.lib")

#include "../include/THFeature_i.h"
#pragma comment(lib,"../include/THFeature.lib")

#include "../include/sdtapi.h"
#pragma comment (lib, "../include/Sdtapi.lib")

#define WIDTHBYTES(bits)    (((bits) + 31) / 32 * 4)

extern CfacerecoDlg* pFacerecoDlg;
extern ResultDlg resultDlg;
extern SpiritDlg* spiritDlg;

extern CString appPath;
extern CString storagePath;

extern long preLastTime;
extern CString preDetectingPath;

extern CString readFilterPath;

// 摄像头
extern	MCapture* pCapture;
// 摄像头基本属性
extern	SCapParam pCapParam;

// 绘制 bmp 画面
extern void DrawBmpBuf(BITMAPINFOHEADER& bih,BYTE* pDataBuf,HWND hShowWnd,BOOL bFitWnd,bool drawLine);
// 封装 BITMAPINFOHEADER
extern void ContructBih(int nWidth,int nHeight,BITMAPINFOHEADER& bih);
// 获取部分图片
extern void GetPartImage( BYTE* image, int imageWidth, int imageHeight, BYTE* part,  int left,int top, int right ,int bottom );
// 缩放图片
extern BOOL ResampleBmp(BITMAPINFOHEADER& bih, BYTE* inBuf,int newx,int newy,BYTE* outBuf);
// 绘制人像框
extern void DrawFaceRects(BYTE* pRgbBuf,int nBufWidth,int nBufHeight,RECT* pFaceRects,int nRectNum,COLORREF color,int nPenWidth);
// 图片镜像
extern BOOL MirrorDIB(LPSTR lpDIBBits, LONG lWidth, LONG lHeight, BOOL bDirection,int nImageBits);
// 缓存数据转化成 CBitmap
extern BOOL GetCBitmap(BITMAPINFOHEADER* pBih, BYTE* pDataBuf,CBitmap &bmp);
// 拆分字符串
extern void SplitCString(CString str, CStringArray& Arr, char ch);
// 保存缓存成bmp文件
extern BOOL SaveBufToBmpFile( BYTE* buf, LPBITMAPINFOHEADER lpBih, LPCSTR strFileName );
// 保存CBitmap成bmp文件
extern void SaveCBitmapToBmpFile(CBitmap* bmp, LPCSTR strFileName);
// 复制CBitmap位图
extern HBITMAP CopyBitmap(HBITMAP hSourceHbitmap);
// byte 转 16进制字符串
extern CString BYTEToHex(const BYTE* ary);
// 获取文件md5
extern CString Md5file(const char* filename);
// 多字节字符转utf8
extern bool MultiToUTF8(const std::string& multiText, std::string& utf8Text);
// 加载图片读取数据
extern void LoadBmpFromFile(LPCTSTR strFileName, CBitmap& bmp);
// 下载文件
extern int DownloadFile(const CString strUrl,const CString strSavePath);
// 移除空格
extern void TrimAry(char* ary, int len);
// 创建发送数据Socket
extern BOOL BuildSenderSocket(SOCKET& sdSocket, int sdPort, SOCKADDR_IN& sockaddr_in);
// 创建接收数据Socket
extern BOOL BuildReceiverSocket(SOCKET& rcSocket, int rcPort, int timeout);
// 判断文件是否存在
extern BOOL FileExists(char* fileName);




// API MD5 define
typedef void (*APIMD5)(unsigned char*,int, char*);
// API Threshold define
typedef void (*APIThreshold)(char*,char*,char*,char*);
// API CheckVersion defin
typedef int (*APICheckVersion)(char*, char*, char*, char*, char*, char*);
// API HotelHand define
typedef BOOL (*APIHotelHand)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*);
// API NetBarHand define
typedef BOOL (*APINetBarHand)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*);
// API UpdateInfo define
typedef void (*APIUploadInfo)(char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, char*, int);
// API gasDoSimilarity define
typedef void (*APIGasDoSimilarity)(const char*, const char*, const char*, const char*, const char*, const char*, const char*, const char*, char*);
// API getSimilarityVal define
typedef void (*APIGetSimilarityVal)(const char*, const char*, const char*, const char*, char*);
// API doResultSure define
typedef void (*APIDoResultSure)(const char*, const char*, const char*, const char*, const char*, char*);

// API DLL
extern HINSTANCE apiDll;
// API MD5 function
extern APIMD5 apiMd5;
// API Threshold function
extern APIThreshold apiThreshold;
// API UploadInfo function
extern APIUploadInfo apiUploadInfo;
// API HotelHand function
extern APIHotelHand apiHotelHand;
// API NetBarHand function
extern APINetBarHand apiNetBarHand;
// API CheckVersion function
extern APICheckVersion apiCheckVersion;
// API GasDoSimilarity function
extern APIGasDoSimilarity apiGasDoSimilarity;
// API GetSimilarity function
extern APIGetSimilarityVal apiGetSimilarityVal;
// API DoResultSure function
extern APIDoResultSure apiDoResultSure;

typedef BOOL (*APIRfdReadInfo)(CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , CHAR* , ULONG );

