#pragma once


// SRMessageBoxDlg 对话框

class SRMessageBoxDlg : public CDialog
{
	DECLARE_DYNAMIC(SRMessageBoxDlg)

public:
	SRMessageBoxDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~SRMessageBoxDlg();

// 对话框数据
	enum { IDD = IDD_SRMESSAGEBOXDLG };

protected:
	afx_msg void OnTimer(UINT_PTR nIDEvent);


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
