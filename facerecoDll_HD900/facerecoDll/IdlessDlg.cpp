// IdlessDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "facerecoDll.h"
#include "IdlessDlg.h"


// IdlessDlg 对话框

IMPLEMENT_DYNAMIC(IdlessDlg, CDialog)

IdlessDlg::IdlessDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IdlessDlg::IDD, pParent)
{

}

IdlessDlg::~IdlessDlg()
{
}

void IdlessDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(IdlessDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_WM_NCHITTEST()


	ON_STN_CLICKED(1380, &IdlessDlg::OnStnClickedClose)
	ON_STN_CLICKED(1381, &IdlessDlg::OnStnClickedConfirm)

END_MESSAGE_MAP()


LRESULT IdlessDlg::OnNcHitTest(CPoint point) {
	return HTCAPTION;
}

// IdlessDlg 消息处理程序
BOOL IdlessDlg::PreTranslateMessage(MSG* pMsg) {
	if(pMsg->message == WM_KEYDOWN)
	{
		switch(pMsg->wParam)
		{
			case VK_RETURN:    // 屏蔽回车
				OnStnClickedConfirm();
				return true;
			case VK_ESCAPE:
				return true;
		}
	}
   
    return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH IdlessDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) {
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	int ID=pWnd->GetDlgCtrlID();

	if (ID == 1381) {
		//pDC->SelectObject(fntElement);
		return hbr;
	} else if (ID == 1382) {
		pDC->SetTextColor(RGB(255,0,0));
		pDC->SetBkMode(TRANSPARENT); //设置标签背景透明
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	} else if (ID == 0) {
		return (HBRUSH)brsBackground;  
	}
  
	return hbr;
}

BOOL IdlessDlg::OnInitDialog() {
	CDialog::OnInitDialog();

	cx = GetSystemMetrics(SM_CXFULLSCREEN);   
	cy = GetSystemMetrics(SM_CYFULLSCREEN);
	
	// 窗口置顶
	SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

	int width = 750;
	int height = 350;
	
	MoveWindow((cx-width) / 2, (cy-height) / 2, width, height);
	
	cbmpBackgound.LoadBitmap(IDB_IDLESSBACKGROUND);
	brsBackground.CreatePatternBrush(&cbmpBackgound);  

	
	hClose = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDLESSCLOSE));
	hConfirm = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IDLESSCONFIRM));

	int posX = 700;
	int posY = 0;
	int ctlWidth = 50;
	int ctlHeight = 35;
	btnClose.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(posX,posY,posX+ctlWidth,posY+ctlHeight),this, 1380);
	btnClose.SetBitmap(hClose);
	btnClose.ShowWindow(true);

	posX = 292;
	posY = 265;
	ctlWidth = 165;
	ctlHeight = 57;
	btnConfirm.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(posX,posY,posX+ctlWidth,posY+ctlHeight),this, 1381);
	btnConfirm.SetBitmap(hConfirm);
	btnConfirm.ShowWindow(true);


	posX = 104;
	posY = 156;
	ctlWidth = 544;
	ctlHeight = 45;
	fntElement.CreatePointFont(220,_T("微软雅黑"),NULL);
	edtElement.Create(WS_CHILD|WS_VISIBLE|WS_TABSTOP, CRect(posX,posY,posX+ctlWidth,posY+ctlHeight), this, 1385);
	edtElement.SetFont(&fntElement);
	edtElement.ShowWindow(true);

	posX = 96;
	posY = 211;
	ctlWidth = 560;
	ctlHeight = 30;
	lblTip.Create(_T("请输入有效证件号或手机号"), WS_CHILD|WS_VISIBLE|SS_LEFT, CRect(posX,posY,posX+ctlWidth,posY+ctlHeight), this, 1382);
	lblTip.ShowWindow(false);

	return 1;
}


void IdlessDlg::OnStnClickedClose() {
	edtElement.SetWindowText("");
	((CfacerecoDlg*)mainDlg)->LeaveIdless();
}

void IdlessDlg::OnStnClickedConfirm() {
	CString text;
	edtElement.GetWindowText(text);
	lblTip.ShowWindow(false);

	if (text.GetLength() != 15 && text.GetLength() != 18 &&  text.GetLength() != 11) {
		lblTip.SetWindowText("请输入有效证件号或手机号");
		lblTip.ShowWindow(true);
		return;
	}

	bool bRet;
	bRet = ((CfacerecoDlg*)mainDlg)->QueryIdlessInfo(text.GetBuffer());
	if (bRet == true) {
		//edtElement.SetWindowText("");
		((CfacerecoDlg*)mainDlg)->StartIdless();
	} else {
		lblTip.SetWindowText("检索不到相关信息");
		lblTip.ShowWindow(true);
	}

}


void IdlessDlg::setDialog(CDialog* dlg) {
	mainDlg = dlg;
}

void IdlessDlg::Show(bool ifShow) {
	this->ShowWindow(ifShow);
	lblTip.ShowWindow(false);
}