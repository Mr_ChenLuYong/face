
#include "SRImageAux.h"



int eshanren::pasteTransparencyImage(cv::Mat& _dest, const cv::Mat& _transparency,
					    float _transparence, COLORREF _transparentColor) {

	if (_dest.cols < _transparency.cols 
		&& _dest.rows < transparency.rows) {
			return 1;
	}

	try {
		// 获得透明背景色
		const BYTE R = GetRValue(_transparentColor);
		const BYTE G = GetGValue(_transparentColor);
		const BYTE B = GetBValue(_transparentColor);
		// 贴图片
		for (int r = 0; r < _transparency.rows; ++r) {
			for (int c = 0; c < _transparency.cols; ++c) {

				cv::Vec3b tr_mat_val = _transparency.at<cv::Vec3b>(cv::Point(c, r));
				// 过滤透明色
				if (tr_mat_val[0] == R &&
					tr_mat_val[1] == G &&
					tr_mat_val[2] == B) {
					continue;
				}
				// 逐像素重构
				cv::Vec3b& dest_mat_val = _dest.at<cv::Vec3b>(cv::Point(c, r));
				dest_mat_val[0] = (uchar)(dest_mat_val[0] * (1 - _transparence) + tr_mat_val[0] * _transparence);
				dest_mat_val[1] = (uchar)(dest_mat_val[1] * (1 - _transparence) + tr_mat_val[1] * _transparence);
				dest_mat_val[2] = (uchar)(dest_mat_val[2] * (1 - _transparence) + tr_mat_val[2] * _transparence);
			}
		}
	}
	catch (std::exception ex) {
		std::cout << ex.what() << std::endl;
		return -1;
	}

	return 0;
}