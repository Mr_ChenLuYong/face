// SpiritDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "facerecoDll.h"
#include "facerecoDlg.h"
#include "SpiritDlg.h"


// SpiritDlg 对话框

IMPLEMENT_DYNAMIC(SpiritDlg, CDialog)

SpiritDlg::SpiritDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SpiritDlg::IDD, pParent)
{

}

SpiritDlg::~SpiritDlg()
{
}

void SpiritDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}



#define WM_NC (WM_USER+1001)

BEGIN_MESSAGE_MAP(SpiritDlg, CDialog)
	ON_WM_NCHITTEST()
	ON_MESSAGE(WM_NC,&SpiritDlg::OnNotifyIcon)
	ON_STN_DBLCLK(1801, &SpiritDlg::OnStnClickedBackground)
	ON_STN_CLICKED(1802, &SpiritDlg::OnStnClickedIdless)
END_MESSAGE_MAP()


// spiritDlg 消息处理程序

BOOL SpiritDlg::PreTranslateMessage(MSG* pMsg) {
    return CDialog::PreTranslateMessage(pMsg);
}

LRESULT SpiritDlg::OnNotifyIcon(WPARAM wParam,LPARAM IParam) {

	return 1;
}

BOOL SpiritDlg::OnInitDialog() {
	CDialog::OnInitDialog();
	
	cx = GetSystemMetrics(SM_CXFULLSCREEN);   
	cy = GetSystemMetrics(SM_CYFULLSCREEN);

	// 窗口置顶
	SetWindowPos(&wndTopMost,0,0,0,0, SWP_NOMOVE | SWP_NOSIZE);

	idless = false;

	int btnWidth = 0;
	int width = 220;
	if (idless == true) {
		btnWidth = 70;
		width = 200 + btnWidth;
	}

	int height = 50;


	
	hRun = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWRUN));
	hScan = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWSCAN));
	hLeave = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWLEAVE));
	hRunIdless = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWRUNIDLESS));
	hScanIdless = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWSCANIDLESS));
	hLeaveIdless = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BRANDNEWLEAVEIDLESS));

	hIdless = ::LoadBitmap(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_BTNIDLESS));

	// 设置窗口大小
	MoveWindow(cx-width - 20, cy-height, width, height);

	// 创建背景图片
	picBackground.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(btnWidth, 0, width, height),this, 1801);
	picBackground.SetBitmap(hRun);
 	picBackground.ShowWindow(true);

	btnIdless.Create(NULL, WS_CHILD|SS_BITMAP|SS_CENTERIMAGE|SS_NOTIFY,CRect(0,0,btnWidth,height),this, 1802);
	btnIdless.SetBitmap(hIdless);
	

	if (idless == true) {
		btnIdless.ShowWindow(idless);
	} else {
		btnIdless.ShowWindow(false);
	}
	return 1;
}


LRESULT SpiritDlg::OnNcHitTest(CPoint point) {
	return HTCAPTION;
}


void SpiritDlg::activeIdless() {
	idless = true;
	setState(1);
}


void SpiritDlg::setState(int state) {
	//if (initType != 5) return;

	int btnWidth = 0;
	int height = 50;
	int width = 220;
	if (idless == true) {
		btnWidth = 70;
		width = 200 + btnWidth;
	}

	if (idless == true) {
		if (state == 1) {
			MoveWindow(cx-width - 20, cy-height, width, height);
			picBackground.MoveWindow(CRect(0, 0, width, height), true);
			picBackground.SetBitmap(hRunIdless);
		} else if (state == 2) {
			picBackground.SetBitmap(hScanIdless);
		} else if (state == 3) {
			picBackground.SetBitmap(hLeaveIdless);
		}
	} else {
		if (state == 1) {
			MoveWindow(cx-width - 20, cy-height, width, height);
			picBackground.MoveWindow(CRect(btnWidth, 0, width, height), true);
			picBackground.SetBitmap(hRun);
		} else if (state == 2) {
			picBackground.SetBitmap(hScan);
		} else if (state == 3) {
			picBackground.SetBitmap(hLeave);
		}
	}


	if (idless == true) {
		btnIdless.MoveWindow(CRect(0, 0, btnWidth, height), true);
		btnIdless.ShowWindow(false);
	}
}

void SpiritDlg::setDialog(CDialog* dlg, int type) {
	mainDlg = dlg;
	initType = type;
}


void SpiritDlg::OnStnClickedBackground() {
	((CfacerecoDlg*)mainDlg)->Active();
	((CfacerecoDlg*)mainDlg)->StartDetect();
}

void SpiritDlg::OnStnClickedIdless() {
	((CfacerecoDlg*)mainDlg)->ActiveIdless();

}