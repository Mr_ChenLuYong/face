#pragma once

// Hook 按键过程
LRESULT CALLBACK KeybdProc(int nCode, WPARAM wParam, LPARAM lParam);

// facerecoDlg 对话框
class CfacerecoDlg : public CDialog {

// 构造
public:
	CfacerecoDlg(CWnd* pParent = NULL);	// 标准构造函数
	
	// 对话框数据
	enum { IDD = 13000 };

	NOTIFYICONDATA notifyIcon;
	afx_msg LRESULT OnNotifyIcon(WPARAM wParam,LPARAM IParam);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON m_hIcon;
	HHOOK m_hHook;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	// 拖动窗口
	afx_msg LRESULT OnNcHitTest(CPoint point);
	// 消息处理
	BOOL PreTranslateMessage(MSG* pMsg);

	// 获取人像
	afx_msg void OnStnClickedGetImage();
	// 人工录入提交
	afx_msg void OnStnClickedSubmit();
	// 最小化窗口
	afx_msg void OnStnClickedMinimize();
	// 性别男
	afx_msg void OnStnClickedMan();
	// 性别女
	afx_msg void OnStnClickedGirl();
	// 手动审核通过
	afx_msg void OnStnClickedConfirm();
	// 手动审核否决
	afx_msg void OnStnClickedRefuse();
	// 版本更新
	afx_msg void OnStnClickedVersion();
	// 关闭程序
	afx_msg void OnStnClickedClose();

// 窗口控件
private:
	CStatic btnMan;
	CStatic btnGirl;
	CStatic picVideo;
	CStatic picIDCard;
	CStatic picIDByHand;
	CStatic btnGetImage;
	CStatic btnMinimize;
	CStatic btnSubmit;
	CStatic btnConfirm;
	CStatic btnRefuse;
	CStatic btnVersion;
	CStatic btnClose;

	CStatic lblName;
	CStatic lblIDCode;
	CStatic lblAddress;
	CStatic lblBirthday;
	CStatic lblGender;
	CStatic lblYear;
	CStatic lblMonth;
	CStatic lblDay;

	CStatic lblCardName;
	CStatic lblCardCode;
	CStatic lblCardAddress;
	CStatic lblCardMatch;
	CStatic lblType;

	CEdit edtName;
	CEdit edtIDCode;
	CEdit edtAddress;
	CEdit edtYear;
	CEdit edtMonth;
	CEdit edtDay;


	CMenu menuTray;

// 线程
private:
	// 是否捕捉
	bool doListening;
	// 是否检测人脸
	bool videoDetect;
	// 捕捉方法
	void DoListening();
	// 捕捉摄像头线程
	static UINT VideoThreadProc(LPVOID lpVoid);

	// 是否接收
	BOOL doReceiving;
	// 接收方法
	void DoReceiving();
	// 监听socket 线程
	static UINT SocketThreadProc(LPVOID lpVoid);


	// 是否读取
	bool doReading;
	// 暂停读取
	bool pauzeReading;
	// 读取信息
	void DoReading();
	// 监听读卡器线程
	static UINT ReaderThreadProc(LPVOID lpVoid);

	// 是否检测
	bool doDetecting;
	// 暂停监测
	bool pauzeDetecting;
	// 检测图片
	void DoDetecting();
	// 检测图片线程
	static UINT DetectThreadProc(LPVOID lpVoid);


	// 是否过滤
	bool doFilting;
	// 读取过滤信息
	void DoFilting();
	// 读取过滤线程
	static UINT FilterThreadProc(LPVOID lpVoid);


	// 是否在初始化
	bool initing;
	// 初始化
	void DoIniting();
	// 初始化线程
	static UINT InitThreadProc(LPVOID lpVoid);

	

// 属性
private:
	// 版本号
	CString version;
	// 初始化类型
	int initType;
	// 是否全屏弹出框
	int fullScreen;

	// 读卡器端口
	int cardReaderPort;

	// 手动获取人像控制器
	bool doGetImage;
	// 性别男
	bool genderMan;
	// 可手动审核
	bool validByHand;

	// 是否手动获取人像
	bool ifGetImage;

	// 人脸图像
	CBitmap bmpFace;
	// 缓存人脸照
	CBitmap bmpStoreFace;
	// 缓存视频照
	CBitmap bmpStoreVideo;
	// 证件照
	CBitmap bmpIDAvatar;
	// 缓存手动获取人像;
	CBitmap bmpStoreFaceByHand;

	// 特征大小
	int featureSize;
	// 证件特征
	BYTE* idFeature;


	// 比对次数
	int matchCount;
	// 相似度
	double maxScore;
	// 分数最低阈值
	double scoreThreshold;
	// 分数最高阈值
	double scoreThresholdHigh;

	// 是否在识别
	bool doComparing;

	// 指令退出
	bool cmdExit;

	// 监测路径
	CString detectPath;

	// 发送数据socket
	SOCKET senderSocket;
	// 发送地址信息
	SOCKADDR_IN saUdpServ;
	// 接收数据socket
	SOCKET receiverSocket;
	// 发送端口
	int senderPort;
	// 接收端口
	int receiverPort;

	// 发送数据给守护进程Socket
	SOCKET senderProtect;
	// 发送数据给守护进程地址信息
	SOCKADDR_IN saUdpServProtect;
	// 接收守护进程数据Socket
	SOCKET receiverProtect;
	// 发送守护进程端口
	int senderProtectPort;
	// 接收守护进程端口
	int receiverProtectPort;

	CString storeConfig;
	CString config;

	// 视频检索人像时间
	CTime scanFaceTime;

	// 监测头像图片时间
	CTime detectingTime;

	bool initFilter;


// 身份数据
private:
	CString appkey;
	CString appsecret;

	CString fullFile;
	CString avatarFile;
	CString idFile;
	CString handFaceFile;

	CString name;
	CString gender;
	CString nation;
	CString bornday;
	CString location;
	CString idcode;
	CString polistation;
	CString startday;
	CString enday;
	CString similarity;

	CString internet_bar_id;

	// 证件人脸特征
	CString idFeatureHex;
	// 视频人脸特征
	CString videoFeatureHex;

	CString byHandName;
	CString byHandCode;
	CString byHandLocation;
	CString byHandYear;
	CString byHandMonth;
	CString byHandDay;


// 常量
private:
	int faceWidthConst;
	int faceHeightConst;
	int maxMatchCountConst;

// 方法
private:
	// 加载配置
	void LoadConfig();
	// 请求阈值
	void QueryThreshold();
	// 上传数据
	void UploadInfo(int result);
	// 更新版本
	BOOL UpdateVersion(CString& nVersion, CString& urls);
	// 绘制相似度
	void DrawMatch();
	// 绘制版本
	void DrawVersion();
	// 绘制标签
	void DrawLabel(CStatic* label, CFont* font, int prop);
	// 检测证件人脸
	void DetectIDFace(BOOL fromFile);
	// 检测人脸
	int DetectFace(BYTE* pBuf, int width, int height, BYTE* pFeature, BOOL detectOnly, BOOL drawFaceBox, BOOL genFeature);
	// 弹出消息框
	void Message(char* msg);
	// 清空显示数据
	void Clear();
	// 发送消息
	void SendSocket(char* msg);
	// 发送消息
	void SendProtect(char* msg);
	// 隐藏主窗口
	void HideMain();
	// 退出程序
	void Exit();

public:
	void Active();
	void StartDetect();
};