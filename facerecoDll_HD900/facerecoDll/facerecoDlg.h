#pragma once

// Hook 按键过程
LRESULT CALLBACK KeybdProc(int nCode, WPARAM wParam, LPARAM lParam);

// facerecoDlg 对话框
class CfacerecoDlg : public CDialog {

// 构造
public:
	CfacerecoDlg(CWnd* pParent = NULL);	// 标准构造函数
	
	// 对话框数据
	enum { IDD = 13000 };

	NOTIFYICONDATA notifyIcon;
	afx_msg LRESULT OnNotifyIcon(WPARAM wParam,LPARAM IParam);

	afx_msg LRESULT OnUpdate(WPARAM wParam,LPARAM IParam);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON m_hIcon;
	HHOOK m_hHook;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()

	// 拖动窗口
	afx_msg LRESULT OnNcHitTest(CPoint point);
	// 消息处理
	BOOL PreTranslateMessage(MSG* pMsg);

	// 人工录入提交
	afx_msg void OnStnClickedSubmit();
	// 最小化窗口
	afx_msg void OnStnClickedMinimize();
	// 性别男
	afx_msg void OnStnClickedMan();
	// 性别女
	afx_msg void OnStnClickedGirl();
	// 手动审核通过
	afx_msg void OnStnClickedConfirm();
	// 手动审核否决
	afx_msg void OnStnClickedRefuse();
	// 版本更新
	afx_msg void OnStnClickedVersion();
	// 关闭程序
	afx_msg void OnStnClickedClose();

	// 摄像头切换
	afx_msg void OnCbnSelchangeCombol();

// 窗口控件
private:
	CStatic btnMan;
	CStatic btnGirl;
	CStatic picVideo;
	CStatic picIDCard;
	CStatic picIDByHand;
	CStatic picIDState;
	CStatic btnGetImage;
	CStatic btnMinimize;
	CStatic btnSubmit;
	CStatic btnConfirm;
	CStatic btnRefuse;
	CStatic btnVersion;
	CStatic btnClose;
	CStatic picBackground;

	CStatic picIconPass;
	CStatic picIconRefuse;

	CStatic lblName;
	CStatic lblIDCode;
	CStatic lblAddress;
	CStatic lblBirthday;
	CStatic lblGender;
	CStatic lblYear;
	CStatic lblMonth;
	CStatic lblDay;
	CStatic lblOrgn;
	CStatic lblExpire;
	CStatic lblScore;
	CStatic lblMatch;
	CStatic lblState;
	CStatic lblPercent;
	CStatic lblCammerTip;

	CComboBox* cboCapture;


	CStatic lblCardName;
	CStatic lblCardCode;
	CStatic lblCardAddress;
	CStatic lblCardMatch;
	CStatic lblType;

	CEdit edtName;
	CEdit edtIDCode;
	CEdit edtAddress;
	CEdit edtYear;
	CEdit edtMonth;
	CEdit edtDay;


	CMenu menuTray;


// 初始化函数
private:
	void initClyRes(void);

	void function(LPVOID lpVoid);

private:
	// 主标题标签
	CStatic labTitle_;
	// 副标题标签
	CStatic labTitleType_;

	// 版权
	CStatic labCopyRight1_;
	CStatic labCopyRight2_;

	// 镜头拍摄的快照照片标签
	CStatic picSnapshoot_;


	// 该参数为0时，则 (请正视摄像头) 为关闭，大于0时，为开启
	unsigned int remindOnOff_;
	// 提醒语音的速度，按等级划分，最低为0级。
	unsigned int remindSpeed_;

	// 窗口大小
	CSize windowRect_;

	// 视频中的顶部提示
	CString strTopTip_;
	COLORREF clTopTip_;

	// 视频中心验证结果提示
	CString strResultTip_;
	COLORREF clResultTip_;




	// detect函数锁
	HANDLE mtxDetect_;


// 线程
private:
	// 是否捕捉
	bool doListening;
	// 是否检测人脸
	bool videoDetect;
	// 暂停视频
	bool pauseVideo;
	// 捕捉方法
	void DoListening(LPVOID lpVoid);
	// 捕捉摄像头线程
	static UINT VideoThreadProc(LPVOID lpVoid);

	static UINT VideoDiscernThreadProc(LPVOID lpVoid);

	// 是否接收
	bool doReceiving;
	// 接收方法
	void DoReceiving();
	// 监听socket 线程
	static UINT SocketThreadProc(LPVOID lpVoid);


	// 是否读取
	bool doReading;
	// 暂停读取
	bool pauzeReading;
	// 读取信息
	void DoReading();
	// 监听读卡器线程
	static UINT ReaderThreadProc(LPVOID lpVoid);

	// 是否检测
	bool doDetecting;
	// 暂停监测
	bool pauzeDetecting;
	// 检测图片
	void DoDetecting();
	// 检测图片线程
	static UINT DetectThreadProc(LPVOID lpVoid);


	// 是否过滤
	bool doFilting;
	// 暂停过滤
	bool pauzeFilting;
	// 读取过滤信息
	void DoFilting();
	// 读取过滤线程
	static UINT FilterThreadProc(LPVOID lpVoid);


	// 是否在初始化
	bool initing;
	// 初始化
	void DoIniting();
	// 初始化线程
	static UINT InitThreadProc(LPVOID lpVoid);

	// 是否心跳
	bool doHeartbeat;
	// 心跳 token 在线验证
	bool DoHeartbeat();
	// 心跳线程
	static UINT HeartbeatThreadProc(LPVOID lpVoid);

	
	// 上传通过
	static UINT UploadPassThreadProc(LPVOID lpVoid);
	// 上传否决
	static UINT UploadRejectThreadProc(LPVOID lpVoid);
	// 上传手动通过
	static UINT UploadHandPassThreadProc(LPVOID lpVoid);
	// 上传手动否决
	static UINT UploadHandRejectThreadProc(LPVOID lpVoid);

// 属性
private:

	int cx, cy;

	// 版本号
	CString version;
	// 初始化类型
	int initType;
	// 是否全屏弹出框
	int fullScreen;

	// 读卡器端口
	int cardReaderPort;

	// 手动获取人像控制器
	bool doGetImage;
	// 性别男
	bool genderMan;
	// 可手动审核
	bool validByHand;

	// 是否手动获取人像
	bool ifGetImage;

	// 数据源类型
	int sourceType;
	// 数据源url
	CString sourceUrl;
	// 客户数据源url
	CString clientSourceUrl;
	// 客户受访人url
	CString clientHostUrl;

	
	// 是否支持无证比对
	bool ifIdless;
	// 是否为无证比对
	bool isIdlessCompare;
	// 无证路由
	CString idlessUrl;
	// 无证请求路由
	CString idlessQueryUrl;

	bool isTimeout;

public:
	// 人脸图像
	CBitmap bmpFace;
	// 缓存人脸照
	CBitmap bmpStoreFace;
	// 缓存画框人脸照
	CBitmap bmpStoreBoxedFace;
	// 缓存视频照
	CBitmap bmpStoreVideo;
	// 证件照
	CBitmap bmpIDAvatar;
	// 缓存手动获取人像;
	CBitmap bmpStoreFaceByHand;



	HBITMAP hVideo;
	HBITMAP hIdCard;
	HBITMAP hMinimize_;
	HBITMAP hClose;
	HBITMAP hPass;
	HBITMAP hRefuse;
	HBITMAP hIconPass;
	HBITMAP hIconRefuse;
	HBITMAP hSubmit;
	HBITMAP hManCheck;
	HBITMAP hGirlCheck;
	HBITMAP hManUncheck;
	HBITMAP hGirlUncheck;
	HBITMAP hValid;
	HBITMAP hInvalid;


		
	// 背景图片
	CBitmap cbmpBackgound;
	// 背景图片
	BITMAP bmpBackground;
	// 背景刷
	CBrush brsBackground; 

	// 特征大小
	int featureSize;
	// 证件特征
	BYTE* idFeature;


	// 比对次数
	int matchCount;
	// 相似度
	double maxScore;
	// 分数最低阈值
	double scoreThreshold;
	// 分数最高阈值
	double scoreThresholdHigh;

	// 是否在识别
	bool doComparing;

	// 指令退出
	bool cmdExit;

	// 监测路径
	CString detectPath;

	// 发送数据socket
	SOCKET senderSocket;
	// 发送地址信息
	SOCKADDR_IN saUdpServ;
	// 接收数据socket
	SOCKET receiverSocket;
	// 发送端口
	int senderPort;
	// 接收端口
	int receiverPort;

	// 发送数据给守护进程Socket
	SOCKET senderProtect;
	// 发送数据给守护进程地址信息
	SOCKADDR_IN saUdpServProtect;
	// 接收守护进程数据Socket
	SOCKET receiverProtect;
	// 发送守护进程端口
	int senderProtectPort;
	// 接收守护进程端口
	int receiverProtectPort;

	CString storeConfig;
	CString config;

	// 视频检索人像时间
	CTime scanFaceTime;

	// 监测头像图片时间
	CTime detectingTime;

	bool initFilter;

// 身份数据
private:
	CString appkey;
	CString appsecret;
	CString identity;
	CString token;

	CString fullFile;
	CString avatarFile;
	CString idFile;
	CString handFaceFile;
	CString idDataFile;

	CString name;
	CString gender;
	CString nation;
	CString bornday;
	CString location;
	CString idcode;
	CString polistation;
	CString startday;
	CString enday;
	CString similarity;

	CHAR idMetadata[1288];

	CString internet_bar_id;

	CString phone_num;
	CString reason;
	CString hoster_id;

	// 证件人脸特征
	CString idFeatureHex;
	// 视频人脸特征
	CString videoFeatureHex;

	CString byHandName;
	CString byHandCode;
	CString byHandLocation;
	CString byHandYear;
	CString byHandMonth;
	CString byHandDay;


// 常量
private:
	int faceWidthConst;
	int faceHeightConst;
	int maxMatchCountConst;

// 方法
private:
	// 加载配置
	void LoadConfig();
	// 请求阈值
	void QueryThreshold();
	// 上传数据
	void UploadInfo(int result);
	// 更新版本
	bool UpdateVersion(CString& nVersion, CString& urls);
	// 绘制相似度
	void DrawMatch();
	// 绘制地址
	void DrawAddress();
	// 绘制版本
	void DrawVersion();
	// 绘制标签
	void DrawLabel(CStatic* label, CFont* font, int prop);
	// 绘制标签
	void DrawLabel(CStatic* label, CFont* font, int prop, COLORREF clr);
	// 检测证件人脸
	void DetectIDFace(bool fromFile);
	// 检测人脸
	int DetectFace(BYTE* pBuf, int width, int height, BYTE* pFeature, bool detectOnly, bool drawFaceBox, bool genFeature, bool checkAngle);
	// 审核通过
	void validPass();
	// 审核否决
	void validRefuse(char* msg);
	// 更新状态
	void updateState(char* msg);
	// 弹出消息框
	void Message(char* msg);
	// 清空显示数据
	void Clear();
	// 发送消息
	void SendSocket(char* msg);
	// 发送消息
	void SendProtect(char* msg);
	// 隐藏主窗口
	void HideMain();
	// 退出程序
	void Exit();
	// 下载更新文件
	void Update(char* newVer);
	// 检查源
	void CheckSource();
	// 检查版本
	void CheckVersion();
	// 检查功能
	void CheckFunction();

	void ShowVisit();

public:
	void Active();
	void StartDetect();
	void SetSecret(char *theKey, char *theSecret, char *theIdentity);

	void ActiveIdless();
	int QueryIdlessInfo(char* data);
	void LeaveIdless();
	void StartIdless();




private:


public:	

	// 为避免单线程卡顿，多线程验证网络接口状况
	static UINT thread_VerifyTrainInfo(LPVOID lpVoid);
	void verifyTrainInfo(void);

	// 场所编号
	CString placeCode_;
	// 场所验证码
	CString verifyCode_;
	// 验证url地址
	CString verifyUrl_;
};