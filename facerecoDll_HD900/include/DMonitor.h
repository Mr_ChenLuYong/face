#include <windows.h>
#include <winsvc.h>
#include <conio.h>
#include <stdio.h>


#define DLLOPTION _declspec(dllexport)

extern "C" {
	DLLOPTION BOOL loadDriver(const char* lpszDriverName, const char* lpszDriverPath);
	DLLOPTION BOOL unloadDriver(const char* szServiceName);
}