////////////////////////////////////////////////////////
//
// AiFace.h
//
// 人证核验SDK接口定义
//
////////////////////////////////////////////////////////

#ifndef __AIFACE_H__
#define __AIFACE_H__

#ifdef AIFACE_EXPORTS
  #define AIFACE_API __declspec(dllexport)
#else
  #define AIFACE_API __declspec(dllimport)
#endif

typedef struct FACE_DETECT_RESULT {
	RECT rcFace;
   	POINT ptLeftEye, ptRightEye, ptMouth, ptNose;
	int nAngleYaw, nAnglePitch, nAngleRoll, nQuality;
	char FaceData[512];
} FACE_DETECT_RESULT, *LPFACE_DETECT_RESULT;

#ifdef __cplusplus
  extern "C" {
#endif

// 获取SDK版本（可不调用）
// 返回：SDK版本号
// 备注：可不调用，任何时候均可调用
AIFACE_API int WINAPI AiFaceVer(void);

// SDK初始化
// 返回：成功返回0，算法初始化失败返回-1，未授权返回-2
// 备注：附获取SDK版本外的任何接口都必须在SDK初始化成功后才能调用
AIFACE_API int WINAPI AiFaceInit(void);

// SDK反初始化
// 备注：必须在初始化成功后调用，反初始化后不能再调用除获取SDK版本及SDK初始化外的任何接口
AIFACE_API void WINAPI AiFaceUninit(void);

// 检测人脸
// 输入参数：pRgb24 ---- RGB24格式的图象数据
//           nWidth ---- 图象数据宽度（象素单位）
//           nHeight ---- 图象数据高度（象素单位）
// 输出参数：pResultBuf ---- 检测到的人脸参数（人脸及眼睛等坐标位置及角度等，调用前必须分配有效的空间）
// 返回：返回1表示检测到人脸，0表示无人脸，-1表示检测失败
AIFACE_API int WINAPI AiFaceDetectFace(BYTE* pRgb24, int nWidth, int nHeight, LPFACE_DETECT_RESULT pFaceResult);

// 获取特征码大小
// 返回：特征码大小
AIFACE_API int WINAPI AiFaceFeatureSize(void);

// 提取特征码
// 输入参数：pRgb24 ---- RGB24格式的图象数据
//           nWidth ---- 图象数据宽度
//           nHeight ---- 图象数据高度
//           pResultBuf ---- 检测到的人脸参数（必须将检测人脸返回的结果原样传入）
// 输出参数：特征码（调用前必须分配有效的空间）
// 成功返回0，失败返回-1
AIFACE_API int WINAPI AiFaceFeatureGet(BYTE* pRgb24, int nWidth, int nHeight, LPFACE_DETECT_RESULT pFaceResult, BYTE* pFeature);

// 特征比对
// 输入参数：pFeature1 ---- 第1个人脸特征
//           pFeature2 ---- 第2个人脸特征
// 返回：返回两个人脸特征对应的人脸的相似度（0-100）
AIFACE_API unsigned char WINAPI AiFaceFeatureCompare(BYTE* pFeature1, BYTE* pFeature2);

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 以下为扩展接口，供应用程序二次加解密或其它情况使用
//

// 获取加密狗设备唯一ID
// 输入参数：无
// 输出参数：无
// 返回：加密狗设备唯一ID（8字节长度）
AIFACE_API UINT64 WINAPI AiGetHardwareID();

// 写用户数据到加密狗（数据长度最多64字节，最多写入100次）
// 输入参数：pData ---- 需要写入加密狗中的数据
//           nDataLen ---- 需要写入的数据长度（大于64则只写前64字节）
// 输出参数：无
// 返回：成功返回写入的数据长度，失败返回-1
// 备注：1. 写入加密狗中的数据已加密，同样的数据写入不同加密狗后也是不同的。只有插入同一加密狗调用读函数读出来才是正确的。
//       2. 不支持频繁写入，最多写入100次，写超过100次以后就只读了（再写入直接返回失败）
AIFACE_API int WINAPI AiWriteEprom(char* pData, int nDataLen);

// 从加密狗读用户数据（数据长度最多64字节，读次数无限制）
// 输入参数：nReadLen ---- 需要读取的数据长度（大于64也只能读到64字节）
// 输出参数：pDataBuf ---- 存放读到的数据，调用前必须分配足够的空间
// 返回：成功返回读到的数据长度，失败返回-1
// 备注：1. 写入加密狗中的数据已加密，同样的数据写入不同加密狗后也是不同的，只有插入同一加密狗调用本函数读出来才是正确的。
//       2. 读次数无限制
AIFACE_API int WINAPI AiReadEprom(char* pDataBuf, int nReadLen);


#ifdef __cplusplus
  }
#endif

#endif // __IDFACESDK_H__

